## SharedPrefrences的再封装

### 源码分析

原理 xml文件和io操作

- 缓存机制 进行同步操作

- 异步加载xml   标志位思想

- 值操作Editor

  commit 同步写入磁盘

  apply 异步写入磁盘

  有缓存机制,可以直接拿到数据

### 异常处理

- name key不能为null

- Sp clear操作只是标志位操作,需要apply 或commit后生效

- 容灾备份, commit/apply 操作时会备份bak,完成后删除bak文件 

- ANR   

- 存在序列化对象

  serialVersionUID 默认是由类,接口,变量名称等计算来,建议指定常量,避免反序列化失败\

- 多app 和多进程访问异常

  android 7.0后, sp 创建不支持读写权限,即其他app 无法访问, 建议改广播 contentProvider或者service

### 性能优化

- 避免存储大量数据

  apply /commit都是重新创建新的文件写入,不是增量写入

  结构化的数据建议用数据库

- 尽可能提前初始化   Activity#onCreate 或Fragment#onCreate中初始化 

- 避免key过长

- 多次操作,批量提交

- 缓存Editor对象   每次edit 都会创建一个新的对象,建议改为成员变量

- 不要存放html 和json   转移符号问题, 建议保存文件 ,sp 做地址映射

- 拆分高频和低频操作

### 封装SP

- 官方PrferenceDateStore  support
- [Treasure](https://github.com/baoyongzhang/Treasure)

### 扩展

- 偏好页面实现
  - PreferenceAcitivy  繁琐
  - 自定义界面+ SP     简单
- 多进程访问  [tray](https://github.com/grandcentrix/tray)