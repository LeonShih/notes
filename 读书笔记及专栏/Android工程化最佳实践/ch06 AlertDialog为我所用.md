# 让alertDialog 为我所用

## Dialog

### Dialog和Window

Dialog显示

```
Dialog dialog  = new Dialog(context);
dialog.setContentView(R.layout.dialog);
dialog.show();
```

dialog是挂载在PhoneWindow上

Theme中dialogTheme 属性,指定默认样式

Dialog是Window的封装

### show和dismiss

show  将设置的view挂载到window的过程, 在执行完毕后,发送一个已经显示的信号,用来标记当前Dialog状态并触发监听事件

dismiss 关闭后发送dismiss信号

自定义View 主要状态保存

## AlertDialog

### alertController

承担了管理dialog中所有view的工作, 而它的成员alertParams 承担了数据聚合的工作,alertParams 通过apply将数据和识度进行绑定,从而呈现一个完整的dialog

Activity  Theme中alertDialogTheme 属性,指定默认样式

## DialogFragment

### Fragment 和 Dialog

官方推荐使用DialogFragment管理对话框

不建议在 onCreateView inflate view,在onCreateDialog中建立Dialog对象

### show 和 dismiss 

**show**

DialogFragment#show --> Fragment#new --> FragmentManager#add-->DialogFragment#onCreateDialog

-->DialogFragment#onStart 中触发dialog.show()

**Note:**必须在onStart之后执行dialog.findView 操作

**dismiss**

dismiss 对应fm.commit()

 dismissAllowingStateLoss  对应fm.commitAllowingStateLoss  减少崩溃



DialogFragment创建流程

onCreate--> onCreateView-->fragment.getLayoutInflater-->onCreateDialog-->setUpDialog-->onActivityStart-->onStart

在onStart中getView 为onCreateView返回的View

在onStart中getDialog为onCreateDialog返回的Dialog

## 实际问题

### 输入法无法弹出

在onStart中手动弹出对话框

### 如何支持层叠弹窗

加入回退栈  显示B前隐藏A,返回时B消失,A再显示

### 容易内存泄露

DialogFragment的onDestroyView中置空监听器

Looper.myQueue() 空闲时发送空消息

### 修改尺寸 背景和动画

getDialog.getWindow() 进行相关操作

### 点击后自动关闭

获取AlertDialog重新设置监听

### 关闭或开始时崩溃

allowingStateLoss

## 封装Dialog

## [easyDialog](https://github.com/tianzhijiexian/EasyDialog)

## 可全局弹出Dialog

### 系统级

设置window type TYPE_SYSTEM_ALERT

 需要清单权限

### 应用级

在Application中通过当前Activity 显示dialog