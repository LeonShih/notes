## 调试

### [stetho](https://github.com/facebook/stetho)

## 插件

- statistic 代码统计
- Resource Usage Count 资源使用计数
- drawable preview /Android drawable viewer  预览webp svg,shape xml .9图,layer-list
- color manager  管理颜色
- Layout Master 动态调整ui
- gradle killer  卡死工具
- Translate Plugin 翻译插件

**配置同步**

Android studio config或plugin 目录保存到同步盘

在AS根目录下修改idea.properties

```
idea.config.path=${user.home}/.AndroidStudioPreview/config
idea.plugins.path=${idea.config.path}/plugins
```