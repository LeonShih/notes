# 打造高扩展性的Log系统

[TOC]

## 基本概念

### 作用  

​      定位问题,记录当前程序的状态

### 格式

```
date   time         PID- TID/package priority/tag: message
//Android Studio的日志, pid 进程id, tid 线程id,  单线程时 pid与tid 相同
10-18 16:14:30.052 4402-4402/me.leon D/rxTimer: [DEBUG] 19
```

### log级别

- verbose 可有可无,一般不看
- debug   开发定位问题使用,正式删除
- info      重要但数据量不大的信息,监控运行过程
- warning  警告,引起注意,比如
- error

## 命令行操作Log

### 相关参数

| 参数   | 说明                                                         |
| ------ | ------------------------------------------------------------ |
| **-s** | 设置过滤输出日志Tag                                          |
| -v     | 设置输出格式 只能设置以下中的一项<br>brief process tag thread raw time thread long |
| -c     | 清空日志并退出                                               |
| -d     | 非阻塞 dump 日志,不会新增日志 , 后面可以加参数 system  radio events  main(默认,包括前面3个) |
| -t     | 最近几行日志                                                 |
| -g     | 获取缓存区大小                                               |
| -b     | 加载一个日志缓冲区,如events, radio, 默认main                 |
| -B     | 以二进制形式输出日志的内容                                   |

```
adb logcat -s MyTag
adb logcat -v time 
adb logcat -v
adb logcat -d main
adb logcat -t 10
```

### 过滤级别

> logcat *:级别  级别不区分大小写

```
logcat *:E
```

### 过滤TAG

```
//过滤单个tag
logcat MyTag:E *:S
//过滤多个个tag
logcat TAG1:D TAG2:I *:S
```



### 过滤关键字

```
logcat| grep Keyword
logcat| grep i Keyword  //忽略大小写
```

### 过滤关键信息

```
logcat -d> /sdcard/my.log
//输出到电脑
adb logcat > my.log
```



## Android Studio Log

### 设置模板

Android Studio Setting--Editor-Live Templates 设置常用log库的模板

### 正则过滤

底部logcat 窗口 右上角 **Edit Filter Configuration** 自定义filter

```
//过滤掉系统无用TAG日志
^(?!.*(WifiTrafficPoller|SignalClusterView|WifiHW|agps|NetworkController|MAL-Daemon|Wifi|wpa|PowerManager|SQLite|KaerWeather|PerfService|InputReader|BufferQueue|ActivityThread|SurfaceFlinger|sdk2|AppOps|thermal|ConnectivityService|Watchdog|SettingsInterface|CursorWindowStats|ActivityManager|PhoneInterfaceManager|AES|memtrack|Tethering|PowerUI|nlp|MNLD|gps|mnl|libc|Posix|System|Native|SocketClient|AudioHardware|AudioRecord)).*$
```

### 热部署Log

**好处**

1. 无侵入,不需要清理log
2. 带log链接,直接跳到相关类
3. 仅本地生效

**如何使用**

在断点位置右键, 取消suspend, evaluate and log 编写需要打印的信息,比如 线程名 Thread.currentThread().getName(

## [微信XLog](https://github.com/Tencent/mars/tree/master/mars)

需要安装ndk(11c)和python 环境, 执行build_andorid.py

## [美团Logan](https://github.com/Meituan-Dianping/Logan)

与XLog类似,提供编译好的包

## 扩展Log的功能

- TAG自动化

  自动 填充

  ```
  //一般
  private static final String TAG = "MainActivity"
  
  //从父类获取
  protect String TAG;
  void onCreate(..){
     TAG = getClass().getSimpleName();
  }
  
  //从堆栈获取, 堆栈信息打日志可以点击导航到对应的类 获取堆栈比较消耗性能,release版建议不要使用
  Exception#getStackTrace
  Throwable#getStackTrace
  Thread#currentThread#getStackTrace
  ```

  组件化建议加个前缀进行区分

  **如果混淆了,建议手动设置TAG**

- 文本内容设计

  - 提升字符拼接效率

    多参数时建议使用占位符,使用String.format

  - 支持超长信息

    adroid Log有最大长度限制,多次输出

- 开关设计

  容易被逆向

## 封装Log库

[Timber](https://github.com/JakeWharton/timber/tree/master/timber-sample)   [学习](https://github.com/JakeWharton/timber/blob/f6d1b98d6280ec567d4e8b60a011f662fdce445b/timber/src/main/java/timber/log/Timber.kt)  外观模式,未实现log功能,需要搭配其他log库来用

[Logger](https://github.com/orhanobut/logger)  生产使用



## 使用日志

**统计时间** 

  TimingLogger 系统自带

```
TimingLogger tl = new TimingLogger(TAG,"INFO") 
...//需要统计时间的代码块
t.dumpToLog()
```

**页面跳转** 

建议注册Application#registerActivityLifecycleCallbacks,  在onCreate中进行日志操作

**网络请求日志** 

网络框架自带或者使用Okhttp可以用 LogIntercepter



## 疑问

P37 过滤关键信息 *adb 不支持将Log保存到开发者的计算机?*

P50 提升字符拼接效率 *多参数写法本身在原生Log中就是支持的  Log.d(info,args);*