# APP终极瘦身实践

## 安装包的构成

![1572399365808](C:\Users\Leon\Desktop\笔记\notes\读书笔记\Android工程化最佳实践\img\1572399365808.png)

- assets  配置文件护着资源文件,如字体,htm等
- lib           不同abi的 so库
- resource.arsc  二进制资源文件,描述[id, name, value]三者的映射关系
- META-INF 签名信息
  - CERT.SF
  - MANIFEST.MF
  - CERT.RSA
- res   资源文件,如图片,字符串,音频,xml
- dex  class文件打包后的文件
- AndroidManifest.xml

## 优化Assets目录

### 删除无用字体

只用使用到字,删除没有用到的字

[fontzip](https://github.com/forJrking/FontZip)  字体提取工具

输入需要提权的字体,会生成只有指定字的字体库

### 减少IconFont使用

IconFont在assets中难以管理,功能与svg重叠,文件较大,建议用svg替代

### 动态下载资源

字体,js,html尽量动态下载

### 压缩资源文件

非代码类压缩存储,使用时解压

js或html代码,混淆压缩



## 优化lib目录

### 配置abiFilters

```
 defaultConfig {
    ndk {
            abiFilters 'armeabi-v7a'//, 'x86' //,'armeabi', 'arm64-v8a'
       }
 }
```

有时需要设置gradle.properties

```
android.useDeprecatedNdk = true
```

apk拆分

```
android{
  splits{
  	density {
  		enable true
  		reset()
  		include "mdpi","hdpi"
  	}
  	
  	abi {
  		enable true
  		reset()
  		include "x86","mips"
  		universalApk false  # 默认false , 可不写
  	}
  
  }

}
```

以上会生成4个apk包

### 根据cpu引入so

统计用户cpu型号,然后删除不需要的so, 默认armeabi-v7a ,但现在64位的手机越来越多,google play也强制要求加入arm64-v8a 提升性能, 国内视情况而定,是否额外增加一个arm64-v8a

### 动态加载so

从云端服务器下载

```
使用 System.load(path) 加载
```

System.loadLibrary  so库查找顺序

1. /data/app/package-随机字符串/lib/arm
2. /data/app/package-随机字符串/base.apk!/lib/armeabi
3. /system/lib
4. /system/vendor/lib

**以上目录访问需要root 权限, android 5.1  随机字符 是阿拉伯数字**

### 避免复制so

系统安装apk时,并不会安装apk里所用的so, 根据当前cpu类型,从apk中复制最合适的so,并保存在apk 内部存储路径libs目录下 

**so解压的时候会解压到/data/data中 ?**  android 5.1是做了软连接到 /data/app/package-随机字符串/lib/arm

```
  //android 6.0后可以加上以上配置,防止packageManger在安装过程将so复制到文件系统中
  <application
   		...
        android:extractNativeLibs="false"
    >          
```

### 谨慎处理so

不同三方库可能使用的so的abi不一致,建议统一abi

```
android{
  ndk{
     abiFilters 'armeabi-v7a'
  }
}
```



## 优化Resources.arsc

### 删除无用映射

string 默认是包含多语言的

```
defaultConfig {
		resConfigs "zh"
}
```



### 进行资源混淆

[AndResProguard](https://github.com/shwenzhang/AndResGuard/blob/master/README.zh-cn.md)

[原理](https://mp.weixin.qq.com/s?__biz=MzAwNDY1ODY2OQ==&mid=208135658&idx=1&sn=ac9bd6b4927e9e82f9fa14e396183a8f#rd) : 修改资源名称后,再在Resources.arsc进行相应的修改

使用

```
apply plugin: 'AndResGuard'

buildscript {
    repositories {
        jcenter()
        google()
    }
    dependencies {
        classpath 'com.tencent.mm:AndResGuard-gradle-plugin:1.2.17'
    }
}

andResGuard {
    // mappingFile = file("./resource_mapping.txt")
    mappingFile = null
    use7zip = true
    useSign = true
    // 打开这个开关，会keep住所有资源的原始路径，只混淆资源的名字
    keepRoot = false
    // 设置这个值，会把arsc name列混淆成相同的名字，减少string常量池的大小
    fixedResName = "arg"
    // 打开这个开关会合并所有哈希值相同的资源，但请不要过度依赖这个功能去除去冗余资源
    mergeDuplicatedRes = true
    whiteList = [
        // for your icon
        "R.drawable.icon",
        // for fabric
        "R.string.com.crashlytics.*",
        // for google-services
        "R.string.google_app_id",
        "R.string.gcm_defaultSenderId",
        "R.string.default_web_client_id",
        "R.string.ga_trackingId",
        "R.string.firebase_database_url",
        "R.string.google_api_key",
        "R.string.google_crash_reporting_api_key"
    ]
    compressFilePattern = [
        "*.png",
        "*.jpg",
        "*.jpeg",
        "*.gif",
    ]
    sevenzip {
         artifact = 'com.tencent.mm:SevenZip:1.2.17'
         //path = "/usr/local/bin/7za"
    }

    /**
    * 可选： 如果不设置则会默认覆盖assemble输出的apk
    **/
    // finalApkBackupPath = "${project.rootDir}/final.apk"

    /**
    * 可选: 指定v1签名时生成jar文件的摘要算法
    * 默认值为“SHA-1”
    **/
    // digestalg = "SHA-256"
}
```



### 优化META-INF

主要有三个文件 MANIFEST.MF CERT.SF CERT.RSA

**首先了解下签名和校验过程**

![签名.webp](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/签名.webp.jpg)

**签名校验**

![校验.webp](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/校验.webp.jpg)

### MANIFEST.MF

遍历未签名 apk包中所有文件,对非文件夹,签名文件,逐个生成信息摘要( api18 以下SHA-1 , api18以上高版本SHA-256-Digest)

### CERT.SF

对MANIFEST.MF的签名文件,分三步生成

1. MANIFEST.MF文件信息摘要(base64编码)
2. 每一条内容进行摘要(base64编码)
3. 写入CERT.SF文件

### CERT.RSA

包含私钥和加密算法信息 ,对CERT.SF用私钥进行加密后的值

### 优化建议

除了CERT.RSA外,其他都可以通过资源混淆进行压缩



## 优化RES目录

### 删除无用资源

项目文件右键 清除无用资源(不影响assets)

### 打包时剔除无用资源

```
 buildTypes {
        debug {
        	...
			shrinkResources true
         }
  }
```

### 删除无用语言

```
 android{
 	defaultConfig{
 		 resConfigs "zh"
 	}
 }

```

### 控制raw中的资源大小

音频建议采用ogg格式

图片建议用jpg

### 减少shape文件

使用自带的style

改用第三方库代码生成drawable

### 减少menu文件

ActionBar时代的产物,现在都用ToolBar

### 减少layout文件

复用layout  include layout

### 动态下载图片

在线获取

### 分目录放置图片

微信做法

mdpi 基本没用

hdip  表情

xhdpi  大图高清图

xxhdpi 基本没用

### 合理使用图片资源

参考阿里腾讯建议如下

- 聊天图 放在hdpi
- 纯色小icon用svg
- 背景图等大图,放xhdpi或xxhdpi
- logo做多套  hdpi / xhpi /xxhdpi
- 特殊进行单独补图

### 移除lib库中的配置文件

```
android{
   packagingOptions {
        exclude 'META-INF/DEPENDENCIES'
        exclude 'META-INF/*android*'
        exclude 'okhttp3/**'
        exclude 'META-INF/ASL2.0'
    }
} 
```



## 优化图片资源

google给出图片选择建议 VectorDrawable --> webp -->  png --> jpg

### VectorDrawable 

它是SVG的实现类,AS支持svg单文件转vector  xml 文件

推荐放在drawable目录

### webp 

android 4.0开始支持, 但4.2.1支持透明度

AS webp和png可以互转

### 精简动画图片

帧动画改为svg动画,或者使用第三方库lottie

### 复用相同的icon

形状相同,大小不同的icon



## 优化dex

### 分析dex

gradle插件 [dexcount](https://github.com/KeepSafe/dexcount-gradle-plugin)

### 利用lint分析无用代码

AS Inspect code,对工程进行静态分析

### 删除R文件 (了解)

android 的R文件除了styleable外,其他字段都是int型变量或常量,且在运行时不会改变,可以编译时记录字段名和值,然后利用asm工具将R字段的地方替换成常量

[thinR插件](https://github.com/meili/ThinRPlugin)

### 启用proguard

```
buildTypes {
 debug {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard- 					rules.pro'
        }
        release {

            minifyEnabled true
            zipAlignEnabled true
            shrinkResources true
 
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-			rules.pro'
        }
        
}
```

追求直接可以试试proguard-android-optimize.txt 混淆规则

### 使用拆分后的support库

v4 库 ( support-compat)太大,google进行了拆分 

- support-core-ui
- support-core-utils
- support-fragment
- support-media-compat

support库 最高版本28.0.0,不在维护,迁移到androidx.support

### 尽量不要用Multidex

删除没有用到的库

替换精简库

### 使用更小的库或者合并现有库

同一个功能的库只用一个

封装成base库

### 根据环境依赖库

```
debugImplementation ""
releaseImplementation ""
```

