

## 基础概念

### 逻辑层与UI层分离

逻辑层不要包含与android相关的任何代码,这样方便UT测试

### 测试框架的选型

- UI 测试          Espresso Robotium  UI Automator
- 集成测试       Robolectric
- 单元测试       UT  JUnit , Mockito, PoweMock

## 逻辑测试

### JUnit

**相关注解**

- @Before @After   测试前初始化  测试结束后销毁
- @BeforeClass @AfterClass  同上,但方法必须是静态无参的
- @Ignore  忽略,不进行测试
- @Test
  - expected = Exception.class

**Assert 相关方法**

-  assertEquls  相等  浮点记得加上容差
-  assertThat      正则匹配

**自定义rule**  

实现TestRule接口,相当如对原来的实现进行包装或者代理

```
public class MyRule implements TestRule {
    @Override
    public Statement apply(Statement base, Description description) throws Throwable {
        System.out.println("before");
        base.evaluate();
        System.out.println("after");
        return base;
    }
}
```

在测试类声明rule即可

```
@Rule
public MyRule myRule = new MyRule();
```

另外google 也提供了android特有的rule

```
com.android.support.test.rules:1.0.2
```

### Mockito 与平台无关

mock对象

```
@RunWith(MockitoJUnitRunner.class)
public class MockTest {
    @Mock
    Context context;

    @Before
    public void setUp(){
        when(context.getString(R.string.app_name)).thenReturn("1111");
        when(context.getPackageName()).thenReturn("me.leon");
    }
    @Test(timeout = 10)
    public void  contextMock(){

        Assert.assertEquals(context.getString(R.string.app_name),"1111");
        Assert.assertEquals(context.getPackageName(),"me.leon");
    }
}
```

powermock可以用mock final方法

对于复杂调用链,需要mock所用的方法,所以单元测试的代码要尽量简单

### Robolectric

提供了android sdk所有的类,可以在电脑上测试

```
@RunWith(RobolectricTestRunner.class)
public class RobolectricTest {
    @Test
    public void helloTest(){
        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);
        System.out.println(((Button) mainActivity.findViewById(R.id.btn)).getText());
        System.out.println("Hello");
    }

}
```



### Espresso  真机运行

可以测试UI逻辑

语法 viewMacher + viewAction + viewAssertion

```
onView(withId(R.id.myView))
     .perform(click())
     .check(matches(isDisplayed))
```

​	