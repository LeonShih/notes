#  探寻高效易用的反射API

[TOC]

### 引言

> 无反射,不框架

#### **实际应用**

- EventBus
- ButterKnife
- Android AMS启动Activity
- ...

### 反射能力

**获取class对象**  

*内存只会加载一次,多次获取的对象都是一样的*

**有三种方式:**

- getClass
- 类.classs
- Class.forName(全类名) 一般配置性的框架会使用

**note: **

​       **android 9.0禁止调用@hide的api**

​       可以通过Class#forName判断是否依赖其他项目(组件化或者插件化会使用)

**操作filed**

- getFiled          只能获取public

- getDeclaredFiled 

  ​     Method#getModifiers 及 Modifier#isPrivate判断是否私有,在开启暴力反射 

  ​     Method.setAccessible(true)

  可不用反射,通过provided 依赖同样的class(包名,类型完全一样,成员方法可以只写用到的)文件,欺骗编译器

  手动javac 访问限制叫ide 编译松散

**调用Method**

Method.invoke(obj,prams...)

Method.invoke(null) 静态代码

**invoke性能**

本质 methodAccessor.invoke(), 有两种实现methodAccessorImpl 和nativeMethodAccessorImpl,JDK会根据情况选择不同的实现,但Android中实现都是nativeMethodAccessorImpl

ButterKnife 加入cache机制,减少反射带来的性能影响.

**动态代理**

运行时动态创建接口,hook相应的方法

Porxy.newProxyInstance(clazzLoader,new Class[class interface], proxyhandler)

**[cglib](https://github.com/cglib/cglib)**

 字节码生成库, 不要接口即可进行代理,依赖asm库

 cglib 生成的是class文件,android 中无法使用需要改造dex才能使用

android 实现,参考  [dexmaker](https://github.com/linkedin/dexmaker)  [cglib-for-android](https://github.com/leo-ouyang/CGLib-for-Android)

**实践**

- 修改app信息, hook ActivityThread.sPackageManager
- Retrofit.create方法



### 反射封装库 JOOR

[JOOR](https://github.com/jOOQ/jOOR)

```
// All examples assume the following static import:
import static org.joor.Reflect.*;

String world = on("java.lang.String")  // Like Class.forName()
                .create("Hello World") // Call most specific matching constructor
                .call("substring", 6)  // Call most specific matching substring() method
                .call("toString")      // Call toString()
                .get();                // Get the wrapped object, in this case a String
```

```
public interface StringProxy {
  String substring(int beginIndex);
}

String substring = on("java.lang.String")
                    .create("Hello World")
                    .as(StringProxy.class) // Create a proxy for the wrapped object
                    .substring(6);         // Call a proxy method
```

扩展 [VirtualApp](https://github.com/asLody/VirtualApp)  [VirtualXpose](https://github.com/android-hacker/VirtualXposed)

### 注意事项

- 性能问题

  对性能差的设备影响较大 ,现在的设备影响不大.

  反射的时间大约是普通的4倍,但正常使用时感觉不出来差别,如果需要高频调用,建议设计缓存

  ```
  try {
      for (int i = 0; i < 1_000_000; i++) {
          DummyItem.class.newInstance();
      }
  } catch (InstantiationException e) {
      e.printStackTrace();
  } catch (IllegalAccessException e) {
      e.printStackTrace();
  }
  ```

  |                                | NEXUS 5 (6.0) ART | GALAXY S5 (5.0) ART | GALAXY S3 mini (4.1.2) Dalvik |
  | :----------------------------: | :---------------: | :-----------------: | :---------------------------: |
  |           getFields            |       1108        |        1626         |             27083             |
  |       getDeclaredFields        |        347        |         951         |             7687              |
  |      getGenericInterfaces      |        16         |         23          |             2927              |
  |      getGenericSuperclass      |        247        |         298         |              665              |
  |         makeAccessible         |        14         |         147         |              449              |
  |           getObject            |        21         |         167         |              127              |
  |           setObject            |        21         |         201         |              161              |
  |        createDummyItems        |        312        |         358         |              774              |
  | createDummyItemsWithReflection |       1332        |        6384         |             2891              |

  [参考博客](https://blog.nimbledroid.com/2016/02/23/slow-Android-reflection.html)

  

- 使用时机

  - 没有public 的api
  - 依赖注入DI 和插件化

- 降低反射损耗

  - 减少反射使用的频率,设计缓存
  - APT代替反射, 编译时生成代码

- 危险性

  - api 改动时会报错

    建议测试时,进行相关库依赖的反射检测

- 反射和混淆的关系

  反射的代码不能混淆, 可以使用android.annotation  @Keep 注解,使代码不混淆