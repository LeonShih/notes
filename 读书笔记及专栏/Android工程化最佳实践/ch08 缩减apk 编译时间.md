## 缩减APK编译时间

### 分析项目状况

gradle profile

```
gradlew :app:aR --profile
```

[dexcount](https://github.com/KeepSafe/dexcount-gradle-plugin)

```
buildscript {
    repositories {
        mavenCentral() // or jcenter()
    }

    dependencies {
        classpath 'com.getkeepsafe.dexcount:dexcount-gradle-plugin:0.8.6'
    }
}

// make sure this line comes *after* you apply the Android plugin
apply plugin: 'com.getkeepsafe.dexcount'
```

## 编译环境优化

### 硬件升级

java程序比较占内存

旧设备  SSD+16G内存

新设备 m.2 SSD+ 16G x2 DDR4 3600 内存

### 升级软件

gradle plugin升级

gradle 升级

升级到jdk 8   建议使用 try-with-resources 和default method

### 优化工程配置 offline模式

### 配置可用内存

单击help-->Edit custom vm options 添加以下代码后重启

```
-Xmx3g
```

### 提升JVM堆内存

gralde.properties 修改以下参数

```
org.gradle.jvmargs=-Xmx2048m
```

build.gradle

```
  dexOptions {
        preDexLibraries true
        maxProcessCount 8
        javaMaxHeapSize "2g"
    }
```

### 开启并行编译

gralde.properties 启用,默认关闭, 多module 同时编译, 只是用无耦合的项目

```
 org.gradle.parallel=true
```



### 启用demand模式

gralde.properties 添加

```
org.gradle.configureondemand=true
```

### 配置dexoptions

```
 dexOptions {
        preDexLibraries true
        maxProcessCount 8
        javaMaxHeapSize "2g"
    }
```

## 善用缓存

### 减少动态方法

如动态时间 ,动态版本,开发版本不使用,release使用

### 硬编码BuildConfig 和Res

```
buildConfigField "boolean", "API_ENV", IS_DEBUG ? "true" : dynamicValue()
```

```
buildTypes{
     release{
        resValue "String", "name", dynamicValue().toString()
     }
     debug{
        resValue "String", "name", "leon"
     }
}
```

### 拆分脚本

gradle 拆成不同的plugin

### 拆分代码

项目依赖结构优化,解耦不同module间的项目依赖

### 写死依赖的版本号



## 精简工程

### 差异化加载plugin

插件或增加编译时间,请再合适的时机启用,一般在发版的时候使用

建议定义全局变量来控制

### 使用webp和svg

webp android 4.0开始支持, android4.2.1支持有透明度的webp,Android Studio自带转换webp,右键图片即可找到相应的选项

svg android上的实现是vectorDrawable,也是xml文件,减少aapt工作,压缩率高,但PC无法预览,Android Studio自带插件可以预览

### 精简语言和图片资源

对于开发模式中无用语言和图片资源,可用resConfigs做过滤

```
productFlavors{
  dev{
    resConfigs "zh", 'xxhdpi'
  }
}
```

### 善用no-op

第三方库提供no-op来区分debug和release环境

### exclude 无用库

```
  implementation ("com.shiduai.android:baselib:2.0.4"){
        exclude group: "cn.jzvd"
    }
```

### 删减module

依赖module越多,编译时间越久

### 去掉multidex

support库大小不断减少,未来将会是androidx, 方法数也不会超过64k.

### 删除无用资源

右键项目目录  refactor--->remove unused resource

注意不要勾选无用id选项, 可能会影响databing

## 综合技巧

### 构建开发的Flavor

```
productFlavors{
  dev{
  versionNameSuffix '-ver'
  applicationIdSuffix '.dev'
  }
}
```

### 优化Multidex

```
//方法数不超过64k时
if(BuildConfig.Debug){
   Multidex.install(this);
}
```

### 跳过无用task

```
gradlew :app:aR --profile -x lintVitalRelease -x lint
```

或者通过task.enabled属性进行设置

```
tasks.whenTaskAdded{
     if(it.name.equals("lint")||it.name.equals("lintVitalRelease"))
             it.enabled =falses
}
```

### 关闭aapt的图片优化

aapt图片压缩

```
aaptOptions{
  cruncherEnalbed false
}
```

### 调试时关闭异常跟踪功能

### 谨慎使用AspectJ

编译时,会进行代码插入,比较耗时



## 多渠道打包工具

[multichannelPackageTool](https://github.com/seven456/MultiChannelPackageTool)  不支持v2, 不维护

美团[walle](https://github.com/Meituan-Dianping/walle)

[腾讯VasDolly](https://github.com/Tencent/VasDolly)