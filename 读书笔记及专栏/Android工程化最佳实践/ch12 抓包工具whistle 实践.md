## 抓包工具

### charles

- 支持windows和Mac
- 收费
- 支持请求重发自定义次数
- 限制网速/断点调试

### Fiddle 

windows平台转包工具

### AnyProxy

基于node.js的HTTP/HTTPS 代理服务器

阿里出品

### [Whistle](https://github.com/avwo/whistle)

基于node.js全能抓包工具

 ![img](https://segmentfault.com/img/remote/1460000016058880?w=1829&h=1725) 

## Whistle安装和使用

### 安装和更新

```
npm install -g whistle
```

成功测试

```
w2 run
```

需要搭配浏览器插件 [Proxy SwitchyOmega](https://www.chromefor.com/proxy-switchyomega_v2-5-20/) 配置代理地址 127.0.0.1 8999

也可手动配置浏览器代理



### 手机配置

1. 连接同一wifi

2. 手机wifi设置代理ip 和端口

   w2 run后会显示ip,选择跟手机同一网段的

   ```
   	   http://127.0.0.1:8899/
          http://192.168.0.7:8899/
          http://169.254.234.46:8899/
          http://192.168.56.1:8899/
          http://192.168.102.2:8899/
          http://192.168.253.1:8899/
          http://192.168.40.1:8899/
   ```

   

### 过滤规则

| 关键字 | 说明         | 示例                |
| ------ | ------------ | ------------------- |
| h      | 请求响应头   | h:head1 head2 head3 |
| s      | 响应状态码   | s:200               |
| m      | 请求方法     | m:get               |
| b      | 请求响应内容 | b: keyword1 keywod2 |

