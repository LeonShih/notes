## 万变不离其宗的Intnet

### 源码分析

- Intent 深拷贝       Intent(Intent i)

  ```
  public Intent(Intent o) {
          this.mAction = o.mAction;
          this.mData = o.mData;
          this.mType = o.mType;
          this.mPackage = o.mPackage;
          this.mComponent = o.mComponent;
          this.mFlags = o.mFlags;
          this.mContentUserHint = o.mContentUserHint;
          if (o.mCategories != null) {
              this.mCategories = new ArraySet<String>(o.mCategories);
          }
          if (o.mExtras != null) {
              this.mExtras = new Bundle(o.mExtras);
          }
          if (o.mSourceBounds != null) {
              this.mSourceBounds = new Rect(o.mSourceBounds);
          }
          if (o.mSelector != null) {
              this.mSelector = new Intent(o.mSelector);
          }
          if (o.mClipData != null) {
              this.mClipData = new ClipData(o.mClipData);
          }
      }
  ```

  mSourceBounds 为矩形对象,存储activity位置信息, 共享元素动画

- makeMainActivity 启动主页面

  ```
   public static Intent makeMainActivity(ComponentName mainActivity) {
          Intent intent = new Intent(ACTION_MAIN);
          intent.setComponent(mainActivity);
          intent.addCategory(CATEGORY_LAUNCHER);
          return intent;
      }
  ```

  makeRestartActivityTask与之类似,推送时搭配PendingIntent#getActivities使用,跳转指定页面,返回时回到主页面,  context可以启动多个activity

- Intent Chooser 显示app选择窗口

  PackageManger#queryIntentActivies 确认可以跳转的activity, 只对需要发送的activity发送信息

- URI代替Intent

  URI和Intent可以相互转换,服务器动态下发uri

- 存取值的底层实习

  Intent内部bundle是关键,所有Intent的存取,都是对mExtras的操作

  mExtras 是Bundle类,底层是arrayMap 

- 显示与隐式Intent

  - 显示  指定Activity,有componentName
  - 隐式  不指定Activity



### 序列化方案

- Serializable /Externalizable  写入磁盘 反射

  - Serializable  标记接口, 由ObjectOutputStream ObjectInputStream 实现序列化与反序列化
  - Externalizable  标记接口,可以自行定义序列化实现

- Android Parcelable  写入共享内存,读写顺序需要一致

  [第三个库parceler](https://github.com/johncarl81/parceler) 支持注解自动生成

  kotlin 注解 @Parcelize

- Google Protocol buffer  数据小,速度快,向后兼容  非主流

- [Twitter Serial](https://github.com/twitter/Serial)

### 常见问题

- 父类序列化

  静态变量不会被序列化,  transient 的字段也不会

- 类型转换异常

  数组对象

- 重复启动

  在Activity #onNewIntent 中 setIntent(intent)

- 传递大对象

  不要传递bitmap 对象, 有最大值限制

### [三方库Parceler](https://github.com/JumeiRdGroup/Parceler) 

