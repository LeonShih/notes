## 寻找Fragment的继任者

自带bug较多,官方推荐support包的Fragment

先了解下生命周期

![img](https://images2015.cnblogs.com/blog/945877/201611/945877-20161123093212096-2032834078.png)

### 使用场景

- 日夜间模式

  detach attcah 只对view进行操作,不会影响数据

  修改主题 后,  fm.detach(f).attach(f).commit()

  attach()只会触发onCreateView, 不会触发onAttach

  detach 会执行到onDestroyView,不会执行onDetach

- 缓存界面数据 

  FragmentPagerAdapter 切换原理是 Fragment attach 和detach

- 作为搜索页

  Fragment支持栈  fragmentManager#addToBackStack , 自动响应返回键

- 作为Presenter

  Fragment 没有生命的, 只有生命周期函数, 由FragmentManger触发, 而FragmentManger生命周期与Acitivty关联

  Activity生命周期改变会影响Fragment ,但Fragment 生命周期的改变只和当前的执行操作有关

​       由上可以用无UI Fragment 监听Activity的状态,很多第三方框架都有采用,如Glide, AAC lifecycle

​		有兴趣可以看看最新support包 FragmentActivity ReportFragment源码

### 源码分析

- Transaction 事务

  与数据库的事务不同,Android的只支持一次性多fragment进行多次操作,没有任何事务相关的代码

- 提交操作

  commit是异步的,无法判断状态

  commitNow 同步执行不允许Fragment入栈 ,短时间高频add或remove时使用

- commitAllowingStateLoss

  Activity销毁后执行,会出现IlleagelStateException

  可以采用commitAllowingStateLoss 方式提交

  但最新版本suport包提供  isStateSaved,建议在提交之前进行状态判断

  ```
          FragmentActivity act = getActivity();
          FragmentManager fragmentManager = getFragmentManager();
          if (act==null || act.isFinishing() 
                  ||fragmentManager==null || fragmentManager.isDestroyed()
          ||fragmentManager.isStateSaved()){
              return;
          }
  ```

- add 操作原理

  addToFirstInLastOut    //onAttach-->onCreate

  moveFragmentToExpectedState  //onCreateView -->onStart-->onResume

- replace本质

  activity.findViewById(containerId).addView(fragment.getView())

- 可见性判断

  hide  show 不会触发生命周期,本质是Fragment rootView setVisibility

  isVisible 判断是否对用户可见

  onHiddenChanged 监听hide 或show操作

- ViewPager懒加载

  Fragment和Viewpager结合时,监听可见性setUserVisibleHint  getUserVisibleHInt

  

### 常见问题

- Activity 为空

  进行getActivity 空校验 

  Fragment destroy时将异步监听置空

- startActivityForResult

  调用Activity的方法onActivityResult, 不要删除Activity中的super

  fragment调用,才能收到自己的onActivityResult回调

  不支持setResult

  用Fragment 解耦Activity onActivityResult

- ViewPager的getItem

  外部调用,获取的是新的fragment,必须手动外部维护一个fragment数组,拖过index获取

- FragmentPagerAdapter

  FragmentPagerAdapter        对不需要的fragment进行detach, 仅销毁视图,不销毁实例

  FragmentStatePagerAdapter 销毁fragment, 但之前会进行onSaveInstanceState 保存数据

- 显示一个对话框  DialogFragment

  连续快速点击会崩溃

  ​		button 点击去重

  ​       fragment 状态判断  fragment.getDialog().isShowing()  或者fragment.isAdded()

  

- 重叠显示

  原因: 屏幕旋转后,重走Activity的生命周期,fragmentManager又添加了一遍fragment

  解决:根据onSaveInsatance判断是否从fragmentManager中查找

- Fragment的StateLoss

  fragment commit时会检查acitivity是否保存了状态,如果保存执行commit会报错

  重写show 和dismiss allowingStateLoss

  判断fragmentManager.isStateSaved判断状态

### Fragment 替代品

-   Jetpack Navigation 单Activity 多Fragment

   替代fragmentManager 和fragment栈, 并不能代替Fragment

  主要功能

  - 接管页面传值
  - 管理切换动画
  - 屏蔽Activity 和Fragment 回退栈差异
  - 屏蔽页面跳转链路
  - 支持Fragment和Fragment, Fragment 和Activity相互跳转

-  [Squre Flow](https://github.com/square/flow)  自定义View 替换Fragment

-  简化Fragment shatter

## [Shatter库](https://github.com/tianzhijiexian/Shatter)

  

