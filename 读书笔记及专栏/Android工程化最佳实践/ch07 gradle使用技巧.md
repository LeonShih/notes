# gradle 使用技巧

## 全局配置

设置UTF-8

```
allprojects{
 tasks.withType(JavaCompile){
        options.encoding ='UTF-8'       
    }
}
```

依赖库

```
maven {
	url  'https://xxxx.com'
	name 'google'
}
```

支持groovy

```
apply plugin: "groovy"
dependencies {
	compile localGroovy()
}
```

全局变量

```
ext{
    minSdkVer = 19
    targetSdkVer = 28
}

直接使用,或者rootProject.ext.minSdkVer 使用
```

在 gralde.properties中定义

bool变量,调用toBoolean



配置lint选项

```
android{
  lintOptions {
     disable 'InvalidPackage'
     checkReleaseBuilds false
     abortOnError false
  }

}
```

## 操控Task

更改输出apk的名字

```
 applicationVariants.all {
        def typo = it.buildType.name
        it.outputs.all {
            if (typo == "release") {
               outputFileName = "${project.name}-${VERSION}-${releaseTime}.apk"
            }
        }
```

更改aar位置

```
 applicationVariants.all {
        it.outputs.all {
            if (it.outputFile !=null && it.outputFile.name.endWith('.aar')
            	&&it.outputFile.size != 0
            ) {
               copy{
               		from it.outputFile
               		into "${rootDir}/libs"
               }
            }
        }
```

跳过AndroidTest

```
tasks.whenTaskAdded{
     if(it.name.contains("AndroidTest")){
        it.enable =false
     }
}
```

抽离task脚本

```
apply from: "xxx.gradle"
```

## 动态化

动态设置BuildConfig

```
android{
	defaultConfig{
	    buildConfigField "String", "BUILD_TIMESTAMP", date
        buildConfigField "boolean", "IS_DEMO", IS_DEMO
	}

}
//string的引号 需要转移
```

填充Manifest中的值 如微信登录与分享

```
defaultConfig{
    manifestPlaceholders = [
     "key1" : "val1"
     "key2" : "val2"
    ]
}

//flavor中设置
android{	
		productFlavor.all{
		   manifestPlaceholders.put("key",value)
		}
}
```

buildType支持继承

```
buildTypes{
   relase{
     ...
   }
   
   my.initWith(buildTypes.release)
   my{
      ...
   }
}
```

flavor支持继承

```
flavorDimensions ("isfree", 'channel') //多维参数[是否免费]+ [渠道] + [Debug/Release]
productFlavors {
  free { dimension "isfree"}
  paid { dimension "isfree"}
  
  tencent {dimension "channel"}
  xiaomi  {dimension "channel"}
}

//自动生成BuildCongfig.FLAVOR_chanel 参数
```

内侧版特定ICON

```
指定src 和res目录sourcesets
```

不同渠道不同包名

```
applicaionIdSuffix ".dev"  //包名后缀
```

自动填充版本信息

```
def autoVersonName{
    def cmd = 'git describe --tags'
    def version = cmd.execute().text.trim()
    return version.toString()
}
```

## 远程依赖

配置maven库

```
maven {
  url ""
  name ""
  credentials {
    username = ''
    password = ''
  }
}
```

依赖相关api

| 配置                      | 解析                                              |
| ------------------------- | ------------------------------------------------- |
| api                       | 类似于旧版compile,编译时和运行时依赖,支持依赖传递 |
| implementation            | 不支持依赖传递                                    |
| compileOnly               | 类似于旧版provided,仅编译时依赖                   |
| runtimeOnly               | 类似于旧版apk, 将依赖打到包里,但编译时无法获取类  |
| annotaionProcessor        | 类似于apt,是注解处理器的依赖                      |
| testImplementaion         | java测试库依赖,仅在测试环境中生效                 |
| andriodTestImplementation | android 测试库依赖,仅在测试环境生效               |
| [flavor]+Api              | 针对某个flavor的依赖                              |

组合依赖

```
implementaion([
  'cn.jiguang.sdk:jcore:$jcoreVer',
  'cn.jiguang.sdk:jpush:$jpushVer'
])
```

传递依赖

```
api("com.crashlytics.sdk.android:crashlytics:2.6.8@aar"){
    transive = true
}
// @aar 表示不支持依赖传递  transive = true 又开启了依赖传递
```

动态版本号 (不建议)

```
implementaion "com.android.support:appcompat-v7:28.0.+"
implementaion "com.android.support:appcompat-v7:28.0.0-SNAPSHOT"
```

```
//设置依赖缓存时间,默认24h
configuration.all{
    resolutionStrategy.cacheDynamicVersionFor 0, 'seconds'
    resolutionStrategy.cacheChangingModulesFor 0, 'seconds'
}
```

强制版本号

```
//再root build.gradle中加入  查看版本
subprojects {
    task allDeps(type : DependencyReportTask)
}
//module build.gradle中加入强制版本
android {
   configuraions.all {
          // 指定单个
         resolutionStrategy.force "com.android.support:appcompat-v7:26.1.0"
         //指定多个
        resolutionStrategy{
           force "com.android.support:appcompat-v7:26.1.0",
                 "com.android.support:design:26.1.0"
         }
   }
}

```

exclude 关键字

```
implementaion ("com.android.support:appcompat-v7:28.0.0") {
       exclude group: 'com.android.support' ,module:'support-v4'
       exclude group: 'com.android.support' ,module:'support-compact'
      // exclude group: 'com.android.support'
}

```

依赖管理

```
//单独建xx.gradle文件
ext {
	dep  = [
	"support":"com.android.support:appcompat-v7:28.0.0"
	]
} 
//根目录引入
apply from:"xx.gradle"
```

// p241apply from 的位置是否正确?

## 本地AAR

引用

```
//root build.gralde中引入
repositories {
    flatDir{
        dirs 'libs'
    }
}
//module中使用
implementaion(name:'lib-name', ext:'aar')
```

依赖module/jar

```
implementation project(':lib')
```

```
//多级目录 settings.gradle
include ":lv1:lv2"
//module中使用
implementaion project(':lv1:lv2')
```

```
//jar包 目录引用
 implementation fileTree(include: ['*.jar'], dir: 'libs')
 implementation fileTree(include: ['*.jar'], dir: '../somedir/libs')
 
 //jar包引用
 implementation files('libs/guava-19.0.jar')
```

自建本地库

```
apply plugin:'maven'
uploadArchives {
   repositories.mavenDeployer{
      pom.artifactId='locallib'
      pom.groupId ='group'
      pom.verison='1.0.0'
      repository(url:"file///${rootDir}/local/locallib")
   }
}

//构建库
gradlew -p <libname> clean build uploadArchives --info
```

```
//使用
repositories {
  maven {
      url "file///${rootDir}/local/locallib"
  }
}
```

重新打包[第三方jar](https://github.com/shevek/jarjar)

```
dependencies {
		// Use jarjar.repackage in place of a dependency notation.
		compile jarjar.repackage {
			from 'com.google.guava:guava:18.0'

			classDelete "com.google.common.base.**"

			classRename "com.google.**" "org.private.google.@1"
		}
	}
```

资源管理

```
 sourceSets {
        main {
            jniLibs.srcDirs += ['libs']
            manifest.srcFile 'src/main/release/AndroidManifest.xml'
        }
    }
```

