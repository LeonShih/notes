 synchronized 是 Java 语言中一个重量级的操作

### synchronized 实现原理

#### 对象头

 Java 对象在内存中的布局分为 3 部分：对象头、实例数据、对齐填充

当我们在 Java 代码中，使用 new 创建一个对象的时候，JVM 会在堆中创建一个 instanceOopDesc 对象，这个对象中包含了对象头以及实例数据

![img](https://s0.lgstatic.com/i/image3/M01/05/88/CgoCgV6ekFmAT_MRAAF5oEVoyMU852.png)

- _metadata 主要保存了类元数据

- _mark 是 markOop 类型数据，一般称它为标记字段（Mark Word），其中主要存储了对象的 hashCode、分代年龄、锁标志位，是否偏向锁等

  ![img](https://s0.lgstatic.com/i/image3/M01/05/89/CgoCgV6ekH2APD6oAADF-S0ti4g112.png)

![img](https://s0.lgstatic.com/i/image3/M01/12/B8/Ciqah16ekKGAFBIfAAFvVAWQ_js924.png)

从图中可以看出，根据"锁标志位”以及"是否为偏向锁"，Java 中的锁可以分为以下几种状态：

![img](https://s0.lgstatic.com/i/image3/M01/12/B8/Ciqah16ekLmANGZfAABKhV79kOU625.png)

在 Java 6 之前，并没有轻量级锁和偏向锁，只有重量级锁，也就是通常所说 synchronized 的对象锁，锁标志位为 10。从图中的描述可以看出：当锁是重量级锁时，对象头中 Mark Word 会用 30 bit 来指向一个“互斥量”，而这个互斥量就是 Monitor。

#### Monitor

Monitor 可以把它理解为一个同步工具，也可以描述为一种同步机制。实际上，它是一个保存在对象头中的一个对象。在 markOop 中有如下代码

![img](https://s0.lgstatic.com/i/image3/M01/05/89/CgoCgV6ekM2AL8knAACkN10EBo8887.png)

通过 monitor() 方法创建一个 ObjectMonitor 对象，而 ObjectMonitor 就是 Java 虚拟机中的 Monitor 的具体实现。因此 Java 中每个对象都会有一个对应的 ObjectMonitor 对象，这也是 Java 中所有的 Object 都可以作为锁对象的原因。

**ObjectMonitor 是如何实现同步机制的呢？**

ObjectMonitor 的结构

![img](https://s0.lgstatic.com/i/image3/M01/8B/CE/Cgq2xl6ekOGAQJO7AAFPRBhLNTQ519.png)

关键属性

![img](https://s0.lgstatic.com/i/image3/M01/8B/CE/Cgq2xl6ekPOAF03ZAACOUNhQATA372.png)

当多个线程同时访问一段同步代码时，首先会进入 _EntryList 队列中，当某个线程通过竞争获取到对象的 monitor 后，monitor 会把 _owner 变量设置为当前线程，同时 monitor 中的计数器 _count 加 1，即获得对象锁。

若持有 monitor 的线程调用 wait() 方法，将释放当前持有的 monitor，_owner 变量恢复为 null， _count 自减 1，同时该线程进入 _WaitSet 集合中等待被唤醒。若当前线程执行完毕也将释放 monitor（锁）并复位变量的值，以便其他线程进入获取 monitor（锁）。

![img](https://s0.lgstatic.com/i/image3/M01/8B/CE/Cgq2xl6ekWeALK00ADBnDWEfxrk052.png)





![img](https://s0.lgstatic.com/i/image3/M01/12/B8/Ciqah16ekY6ABhvhAC7y3FnGiFM721.png)

> 实际上，ObjectMonitor 的同步机制是 JVM 对操作系统级别的 Mutex Lock（互斥锁）的管理过程，其间都会转入操作系统内核态。也就是说 synchronized 实现锁，在“重量级锁”状态下，当多个线程之间切换上下文时，还是一个比较重量级的操作。



### Java 虚拟机对 synchronized 的优化

从 Java 6 开始，虚拟机对 synchronized 关键字做了多方面的优化，主要目的就是避免 ObjectMonitor 的访问，减少“重量级锁”的使用次数，并最终减少线程上下文切换的频率.

其中主要做了以下几个优化： **锁自旋、轻量级锁、偏向锁**

锁的获取次序依次是 偏向锁 -> 轻量级锁 -> 重量级锁。

#### 锁自旋

> 线程的阻塞和唤醒需要 CPU 从用户态转为核心态，频繁的阻塞和唤醒对 CPU 来说是一件负担很重的工作，势必会给系统的并发性能带来很大的压力，所以 Java 引入了自旋锁的操作

所谓自旋，就是**让该线程等待一段时间，不会被立即挂起**，看当前持有锁的线程是否会很快释放锁。而所谓的等待就是执行一段无意义的循环即可（自旋）

自旋锁也存在一定的缺陷：自旋锁要占用 CPU，如果锁竞争的时间比较长，那么自旋通常不能获得锁，白白浪费了自旋占用的 CPU 时间。这通常发生在锁持有时间长，且竞争激烈的场景中，此时应主动禁用自旋锁。



#### 轻量级锁

对于一块同步代码，虽然有多个不同线程会去执行，但是这些线程是在不同的时间段**交替请求这把锁对象**，也就是**不存在锁竞争**的情况。在这种情况下，锁会保持在轻量级锁的状态，从而避免重量级锁的阻塞和唤醒操作。

锁的标志位包含几种情况：00 代表轻量级锁、01 代表无锁（或者偏向锁）、10 代表重量级锁、11 则跟垃圾回收算法的标记有关。

当线程执行某同步代码时，Java 虚拟机会在当前线程的栈帧中开辟一块空间（Lock Record）作为该锁的记录，如下图所示：![img](https://s0.lgstatic.com/i/image3/M01/05/8A/CgoCgV6ekiKAPruLAADBBqAFhRY858.png)

然后 Java 虚拟机会尝试使用 CAS（Compare And Swap）操作，将锁对象的 Mark Word 拷贝到这块空间中，并且将锁记录中的 owner 指向 Mark Word。结果如下：

![img](https://s0.lgstatic.com/i/image3/M01/05/8A/CgoCgV6eki2ALuVBAADp64Sok_0397.png)

当线程再次执行此同步代码块时，判断当前对象的 Mark Word 是否指向当前线程的栈帧，如果是则表示当前线程已经持有当前对象的锁，则直接执行同步代码块；否则只能说明该锁对象已经被其他线程抢占了，这时轻量级锁需要膨胀为重量级锁。

> 轻量级锁所适应的场景是线程交替执行同步块的场合，如果存在同一时间访问同一锁的场合，就会导致轻量级锁膨胀为重量级锁。

#### 偏向锁

轻量级锁是在没有锁竞争情况下的锁状态，但是在有些时候锁不仅存在多线程的竞争，而且总是由同一个线程获得。因此为了让线程获得锁的代价更低引入了偏向锁的概念。偏向锁的意思是如果一个线程获得了一个偏向锁，如果在接下来的一段时间中没有其他线程来竞争锁，那么持有偏向锁的线程再次进入或者退出同一个同步代码块，不需要再次进行抢占锁和释放锁的操作。偏向锁可以通过 -XX:+UseBiasedLocking 开启或者关闭。

偏向锁的具体实现就是在锁对象的对象头中有个 ThreadId 字段，默认情况下这个字段是空的，当第一次获取锁的时候，就将自身的 ThreadId 写入锁对象的 Mark Word 中的 ThreadId 字段内，将是否偏向锁的状态置为 01。这样下次获取锁的时候，直接检查 ThreadId 是否和自身线程 Id 一致，如果一致，则认为当前线程已经获取了锁，因此不需再次获取锁，略过了轻量级锁和重量级锁的加锁阶段。提高了效率。

其实偏向锁并不适合所有应用场景, 因为一旦出现锁竞争，偏向锁会被撤销，并膨胀成轻量级锁，而撤销操作（revoke）是比较重的行为，只有当存在较多不会真正竞争的 synchronized 块时，才能体现出明显改善；因此实践中，还是需要考虑具体业务场景，并测试后，再决定是否开启/关闭偏向锁。