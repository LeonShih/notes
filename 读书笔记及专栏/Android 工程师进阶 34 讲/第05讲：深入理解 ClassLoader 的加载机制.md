## Java 中的类何时被加载器加载

- 调用类构造器

- 调用类中的静态（static）变量或者静态方法

## Java 中 ClassLoader

- 启动类加载器 BootstrapClassLoader

- 扩展类加载器 ExtClassLoader （JDK 1.9 之后，改名为 PlatformClassLoader）

- 系统加载器 APPClassLoader

### APPClassLoader 系统类加载器

![](https://s0.lgstatic.com/i/image3/M01/0B/07/Ciqah16MQCaAf-h3AAFnyY9SYn4768.png)

AppClassLoader 主要加载系统属性“java.class.path”配置下类文件，也就是环境变量 CLASS_PATH 配置的路径。因此 AppClassLoader 是面向用户的类加载器，我们自己编写的代码以及使用的第三方 jar 包通常都是由它来加载的。

### ExtClassLoader 扩展类加载器

![ExtClassLoader ](https://s0.lgstatic.com/i/image3/M01/0B/07/Ciqah16MQCaAGUjRAAIMUAR7Y3c186.png)

ExtClassLoader 加载系统属性“java.ext.dirs”配置下类文件

### BootstrapClassLoader 启动类加载器

由 C/C++ 语言编写的，它本身属于虚拟机的一部分

加载系统属性“sun.boot.class.path”配置下类文件



## 双亲委派模式（Parents Delegation Model）

当类加载器收到加载类或资源的请求时，通常都是先委托给父类加载器加载，也就是说，只有当父类加载器找不到指定类或资源时，自身才会执行实际的类加载过程

其具体实现代码是在 ClassLoader.java 中的 loadClass 方法中

![](https://s0.lgstatic.com/i/image3/M01/84/1D/Cgq2xl6MQCeAQezSAAQYyFDklrg999.png)

1. 判断该 Class 是否已加载，如果已加载，则直接将该 Class 返回
2. 如果该 Class 没有被加载过，则判断 parent 是否为空，如果不为空则将加载的任务委托给parent
3. 如果 parent == null，则直接调用 BootstrapClassLoader 加载该类
4. 如果 parent 或者 BootstrapClassLoader 都没有加载成功，则调用当前 ClassLoader 的 findClass 方法继续尝试加载



> 注意：“双亲委派”机制只是 Java 推荐的机制，并不是强制的机制。我们可以继承 java.lang.ClassLoader 类，实现自己的类加载器。如果想保持双亲委派模型，就应该重写 findClass(name) 方法；如果想破坏双亲委派模型，可以重写 loadClass(name) 方法。

### 自定义 ClassLoader

1. 自定义一个类继承抽象类 ClassLoader。
2. 重写 findClass 方法
3. 在 findClass 中，调用 defineClass 方法将字节码转换成 Class 对象，并返回



## Android 中的 ClassLoader

### PathClassLoader

用来加载系统 apk 和被安装到手机中的 apk 内的 dex 文件

### DexClassLoader

可以从 SD 卡上加载包含 class.dex 的 .jar 和 .apk 文件，这也是插件化和热修复的基础



## 总结

- ClassLoader 就是用来加载 class 文件的，不管是 jar 中还是 dex 中的 class。

- Java 中的 ClassLoader 通过双亲委托来加载各自指定路径下的 class 文件。

- 可以自定义 ClassLoader，一般覆盖 findClass() 方法，不建议重写 loadClass 方法。

- Android 中常用的两种 ClassLoader 分别为：PathClassLoader 和 DexClassLoader。