关于事件分发主要有几个方向可以展开深入分析：

- touch 事件是如何从驱动层传递给 Framework 层的 InputManagerService；

- WMS 是如何通过 ViewRootImpl 将事件传递到目标窗口；

- touch 事件到达 DecorView 后，是如何一步步传递到内部的子 View 中的。


其中与上层软件开发息息相关的就是第 3 条。

### 思路梳理

#### ViewGroup

ViewGroup 是一组 View 的组合，在其内部有可能包含多个子 View，当手指触摸屏幕上时，手指所在的区域既能在 ViewGroup 显示范围内，也可能在其内部 View 控件上。

因此它内部的事件分发的重心是处理当前 Group 和子 View 之间的逻辑关系：

- 当前 Group 是否需要拦截 touch 事件；

- 是否需要将 touch 事件继续分发给子 View；

- 如何将 touch 事件分发给子 View。

### View

View 是一个单纯的控件，不能再被细分，内部也并不会存在子 View，所以它的事件分发的重点在于当前 View 如何去处理 touch 事件，并根据相应的手势逻辑进行一些列的效果展示（比如滑动，放大，点击，长按等）。

- 是否存在 TouchListener；

- 是否自己接收处理 touch 事件（主要逻辑在 onTouchEvent 方法中）。



### 事件分发核心 dispatchTouchEvent

整个 View 之间的事件分发，实质上就是一个大的递归函数，而这个递归函数就是 dispatchTouchEvent 方法。在这个递归的过程中会适时调用 onInterceptTouchEvent 来拦截事件，或者调用 onTouchEvent 方法来处理事件。

 dispatch 的源码如下：

![img](https://s0.lgstatic.com/i/image/M00/04/0D/Ciqc1F6zq76AYtJPAADS4BOo8VM039.png)

dispatch 主要分为 3 大步骤：

- 步骤 1：判断当前 ViewGroup 是否需要拦截此 touch 事件，如果拦截则此次 touch 事件不再会传递给子 View（或者以 CANCEL 的方式通知子 View）。

  ![img](https://s0.lgstatic.com/i/image/M00/04/0D/Ciqc1F6zq86AZd-ZAAHnUYfq8iQ886.png)

- 步骤 2：如果没有拦截，则将事件分发给子 View 继续处理，如果子 View 将此次事件捕获，则将 mFirstTouchTarget 赋值给捕获 touch 事件的 View。

  ![img](https://s0.lgstatic.com/i/image/M00/04/0D/Ciqc1F6zq9mAMTcTAAdFapIjIMc616.png)

- 步骤 3：根据 mFirstTouchTarget 重新分发事件。

![img](https://s0.lgstatic.com/i/image/M00/04/0E/Ciqc1F6zq-OAV7ZcAAbboCsB_8o465.png)



### 为什么 DOWN 事件特殊

所有 touch 事件都是从 DOWN 事件开始的，这是 DOWN 事件比较特殊的原因之一。另一个原因是 DOWN 事件的处理结果会直接影响后续 MOVE、UP 事件的逻辑。

后续的 MOVE、UP 等事件的分发交给谁，取决于它们的起始事件 Down 是由谁捕获的。

#### mFirstTouchTarget 有什么作用

![img](https://s0.lgstatic.com/i/image/M00/04/0E/Ciqc1F6zrAqAHqzwAAFLLP0HYng235.png)

 TouchTarget 类型的链表结构。而这个 TouchTarget 的作用就是用来记录捕获了 DOWN 事件的 View，具体保存在上图中的 child 变量。

因为 Android 设备是支持多指操作的,所以是链表类型的结构

#### 容易被遗漏的 CANCEL 事件

在上面的步骤 3 中，继续向子 View 分发事件的代码中，有一段比较有趣的逻辑：

![img](https://s0.lgstatic.com/i/image/M00/04/0E/Ciqc1F6zrBKACMw7AAINKPG9H7g994.png)

dispatchTransformedTouchEvent 方法![img](https://s0.lgstatic.com/i/image/M00/04/0E/CgqCHl6zrBqAQOmTAAGn_cFIxaU530.png)

**什么情况下会触发这段逻辑呢？**

当父视图的 onInterceptTouchEvent 先返回 false，然后在子 View 的 dispatchTouchEvent 中返回 true（表示子 View 捕获事件），关键步骤就是在接下来的 MOVE 的过程中，父视图的 onInterceptTouchEvent 又返回 true，intercepted 被重新置为 true，此时上述逻辑就会被触发，子控件就会收到 ACTION_CANCEL 的 touch 事件。

**实际上有个很经典的例子可以用来演示这种情况：**

当在 Scrollview 中添加自定义 View 时，ScrollView 默认在 DOWN 事件中并不会进行拦截，事件会被传递给 ScrollView 内的子控件。只有当手指进行滑动并到达一定的距离之后，onInterceptTouchEvent 方法返回 true，并触发 ScrollView 的滚动效果。当 ScrollView 进行滚动的瞬间，内部的子 View 会接收到一个 CANCEL 事件，并丢失touch焦点。



### 总结

重点分析了 dispatchTouchEvent 的事件的流程机制，这一过程主要分 3 部分：

- 判断是否需要拦截 —> 主要是根据 onInterceptTouchEvent 方法的返回值来决定是否拦截；

- 在 DOWN 事件中将 touch 事件分发给子 View —> 这一过程如果有子 View 捕获消费了 touch 事件，会对 mFirstTouchTarget 进行赋值；

- 最后一步，DOWN、MOVE、UP 事件都会根据 mFirstTouchTarget 是否为 null，决定是自己处理 touch 事件，还是再次分发给子 View。


然后介绍了整个事件分发中的几个特殊的点。

- DOWN 事件的特殊之处：事件的起点；决定后续事件由谁来消费处理；

- mFirstTouchTarget 的作用：记录捕获消费 touch 事件的 View，是一个链表结构；

- CANCEL 事件的触发场景：当父视图先不拦截，然后在 MOVE 事件中重新拦截，此时子 View 会接收到一个 CANCEL 事件。