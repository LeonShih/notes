### synchronized

可以修饰

- 修饰实例方法 (同一个实例才有效)  

   方法加 flag  **ACC_SYNCHRONIZED**   

  当虚拟机访问一个被标记为 ACC_SYNCHRONIZED 的方法时，会自动在方法的开始和结束（或异常）位置添加 monitorenter 和 monitorexit 指令

- 修饰静态类方法 

- 修饰代码块 

  javap查看字节码

  ![img](https://s0.lgstatic.com/i/image3/M01/10/78/Ciqah16X-CKAZaaeAABaozQldl0900.png)

  ![img](https://s0.lgstatic.com/i/image3/M01/89/8E/Cgq2xl6X-COAC4hMAAEjW40t64s500.png)

上面字节码中有 1 个 monitorenter 和 2 个 monitorexit。这是因为虚拟机需要保证当异常发生时也能释放锁。

**关于 monitorenter 和 monitorexit，可以理解为一把具体的锁**。在这个锁中保存着两个比较重要的属性：计数器和指针。

- 计数器代表当前线程一共访问了几次这把锁

- 指针指向持有这把锁的线程

![img](https://s0.lgstatic.com/i/image3/M01/89/8E/Cgq2xl6X-COAEskYAABd1Qkprak432.png)

### ReentrantLock

#### ReentrantLock 基本使用

ReentrantLock 的使用同 synchronized 有点不同，它的加锁和解锁操作都需要手动完成

![img](https://s0.lgstatic.com/i/image3/M01/03/49/CgoCgV6X-COAM0TsAAHIQYMakhA463.png)

unlock 操作放在 finally 代码块中

### 公平锁实现

> 默认情况下，synchronized 和 ReentrantLock 都是非公平锁。但是 ReentrantLock 可以通过传入 true 来创建一个公平锁。所谓公平锁就是通过**同步队列来实现多个线程按照申请锁的顺序获取锁**。

### 读写锁（ReentrantReadWriteLock）

在读操作时获取读锁，写操作时获取写锁即可。**当写锁被获取到时，后续的读写锁都会被阻塞**，写锁释放之后，所有操作继续执行