### 什么是 Dalvik

Dalvik 是 Google 公司自己设计用于 Android 平台的 Java 虚拟机，Android 工程师编写的 Java 或者 Kotlin 代码最终都是在这台虚拟机中被执行的。在 Android 5.0 之前叫作 DVM，5.0 之后改为 ART（Android Runtime）。

在整个 Android 操作系统体系中，ART 位于以下图中红框位置：

![img](https://s0.lgstatic.com/i/image/M00/00/CA/CgqCHl6qeUKAa86MAAY44MY5alU343.png)

DVM 大多数实现与传统的 JVM 相同，但是因为 Android 最初是被设计用于手机端的，对内存空间要求较高，并且起初 Dalvik 目标是只运行在 ARM 架构的 CPU 上。针对这几种情况，Android DVM 有了自己独有的优化措施。

### Dex 文件

dex 文件去除了 class 文件中的冗余信息（比如重复字符常量），并且结构更加紧凑，因此在 dex 解析阶段，可以减少 I/O 操作，提高了类的查找速度

```
//多个class打成jar
jar -cvf AllDex.jar Dex1.class Dex2.class
//jar2dex
dx --dex --output AllDex.dex AllDex.jars
// Android SDK 中的工具 dexdump 查看其字节码
dexdump -d -l plain AllDex.dex
```

实际上，dex 文件在 App 安装过程中还会被进一步优化为 odex(optimized dex)

注意：这一优化过程也会伴随着一些副作用，最经典的就是 Android 65535 问题。出现这个问题的根本原因是在 DVM 源码中的 MemberIdsSection.java 类中，有如下一段代码：

![img](https://s0.lgstatic.com/i/image/M00/00/CA/CgqCHl6qeduARPd9AAFJZeldgmo025.png)

如果 items 个数超过 DexFormat.MAX_MEMBER_IDX 则会报错，DexFormat.MAX_MEMBER_IDX 的值为 65535，items 代表 dex 文件中的方法个数、属性个数、以及类的个数。

Android 提供了 MultiDex 来解决这个问题

### 架构基于寄存器&基于栈堆结构

JVM 的指令集是基于栈结构来执行的；而 Android 却是基于寄存器的，不过这里不是直接操作硬件的寄存器，而是在内存中模拟一组寄存器

Android 字节码和 Java 字节码完全不同，Android 的字节码（smali）更多的是二地址指令和三地址指令，具体Dalvik 指令可以参考[官网 Dalvik 字节码](https://source.android.google.cn/devices/tech/dalvik/dalvik-bytecode?hl=zh-cn)。

![img](https://s0.lgstatic.com/i/image/M00/00/CB/Ciqc1F6qe4eAZ1zLAAB2Q6lEkoE595.png)

### 内存管理与回收

DVM 与 JVM 另一个比较显著的不同就是内存结构的区别，主要体现在对”堆”内存的的管理。
Dalvik 虚拟机中的堆被划分为了 2 部分：Active Heap 和 Zygote Heap

![img](https://s0.lgstatic.com/i/image/M00/00/CC/CgqCHl6qe9WAY-x4AAHlcF3z4X8795.png)

图中的 Card Table 以及两个 Heap Bitmap 主要是用来记录垃圾收集过程中对象的引用情况，以便实现 Concurrent GC

### 为什么要分 Zygote 和 Active 两部分？

Android 系统的第一个 Dalvik 虚拟机是由 Zygote 进程创建的，而应用程序进程是由 Zygote 进程 fork 出来的。

Zygote 进程是在系统启动时产生的，它会完成虚拟机的初始化，库的加载，预置类库的加载和初始化等操作，而在系统需要一个新的虚拟机实例时，Zygote 通过复制自身，最快速的提供一个进程；另外，对于一些只读的系统库，所有虚拟机实例都和 Zygote 共享一块内存区域，大大节省了内存开销。如下图所示：

![img](https://s0.lgstatic.com/i/image/M00/00/CC/CgqCHl6qe-aATBEFAAEFkKPCQb4077.png)

解释说明：
当启动一个应用时，Android 操作系统需要为应用程序创建新的进程，而这一步操作是通过一种写时拷贝技术（COW）直接复制 Zygote 进程而来。这意味着在开始的时候，应用程序进程和 Zygote 进程共享了同一个用来分配对象的堆。然而，当 Zygote 进程或者应用程序进程对该堆进行写操作时，内核就会执行真正的拷贝操作，使得 Zygote 进程和应用程序进程分别拥有自己的一份拷贝。拷贝是一件费时费力的事情。因此，**为了尽量地避免拷贝，Dalvik 虚拟机将自己的堆划分为两部分。**

事实上，Dalvik 虚拟机的堆最初只有一个，也就是 Zygote 进程在启动过程中创建 Dalvik 虚拟机时，只有一个堆。但是当 Zygote 进程在 fork 第一个应用程序进程之前，会将已经使用的那部分堆内存划分为一部分，把还没有使用的堆内存划分为另外一部分。前者就称为 Zygote 堆，后者就称为 Active 堆。以后无论是 Zygote 进程，还是应用程序进程，当它们需要分配对象的时候，都在 Active 堆上进行。这样就可以使得 Zygote 堆尽可能少地被执行写操作，因而就可以减少执行写时拷贝的操作时间。

### Dalvik 虚拟机堆

在 Dalvik 虚拟机中，堆实际上就是一块匿名共享内存。Dalvik 虚拟机并不是直接管理这块匿名共享内存，而是将它封装成一个 mspace，交给 C 库来管理。因为内存碎片问题其实是一个通用的问题，不只是 Dalvik 虚拟机在 Java 堆为对象分配内存时会遇到，C 库的 malloc 函数在分配内存时也会遇到。

Android 系统使用的 C 库 bionic 使用了 Doug Lea 写的 dlmalloc 内存分配器，也就是说，我们调用函数 malloc 的时候，使用的是 dlmalloc 内存分配器来分配内存。这是一个成熟的内存分配器，可以很好地解决内存碎片问题。

关于 dlmalloc 内存分配器的设计，可以参考Doug Lea写的这篇文章：[A Memory Allocator](http://gee.cs.oswego.edu/dl/html/malloc.html)。



[Android Dalvik官方文档](https://source.android.com/devices/tech/dalvik/dalvik-bytecode.html)
[Dalvik虚拟机字节码和指令集对照表](http://www.zhangchuany.com/dalvik/dalvik-bytecode-instructionset-comparedtab)
[Dalvik虚拟机Java堆创建过程分析](https://blog.csdn.net/luoshengyang/article/details/41581063)


