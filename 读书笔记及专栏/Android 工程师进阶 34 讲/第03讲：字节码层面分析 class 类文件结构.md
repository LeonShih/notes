> java中 String 字符串的长度有限制吗？

## class 的来龙去脉

为了让 Java 语言具有良好的跨平台能力，Java 独具匠心的提供了一种可以在所有平台上都能使用的一种中间代码——字节码类文件（.class文件）。

目前 Java 虚拟机已经可以支持很多除 Java 语言以外的其他语言了，如 Groovy、JRuby、Jython、Scala 等。之所以可以支持其他语言，是因为这些语言经过编译之后也可以生成能够被 JVM 解析并执行的字节码文件。而虚拟机并不关心字节码是由哪种语言编译而来的。如下图所示

![class](https://gitee.com/LeonShih/Image/raw/master/class_compile.png)

### 上帝视角看 class 文件

只有两种数据结构：无符号数和表。

- **无符号数：**属于基本的数据类型，以 u1、u2、u4、u8 来分别代表 1 个字节、2 个字节、4 个字节和 8 个字节的无符号数，无符号数可以用来描述数字、索引引用、数量值或者字符串（UTF-8 编码）。
- **表：**表是由多个无符号数或者其他表作为数据项构成的复合数据类型，class文件中所有的表都以“_info”结尾。其实，整个 Class 文件本质上就是一张表。

关系图

![relation](https://gitee.com/LeonShih/Image/raw/master/class_cmp.png)

可以看出，在一张表中可以包含其他无符号数和其他表格。伪代码可以如下所示：

```
// 无符号数
u1 = byte[1];
u2 = byte[2];
u4 = byte[4];
u8 = byte[8];

// 表
class_table {
    // 表中可以引用各种无符号数，
    u1 tag;
    u2 index2;
    ...

    // 表中也可以引用其它表
    method_table mt;
    ...
}
```

### class 文件结构

按照预先规定好的顺序紧密的从前向后排列，相邻的项之间没有任何间隙。如下所示：

**魔数 版本号 常量池 访问标志 类/父类/接口 字段描述集合 方法描述集合 属性描述集合**

当 JVM 加载某个 class 文件时，JVM 就是根据上图中的结构去解析 class 文件，加载 class 文件到内存中，并在内存中分配相应的空间。具体某一种结构需要占用大多空间，可以参考下图：

![class_type_size](https://gitee.com/LeonShih/Image/raw/master/clase_type_size.png)

### 分析class

可以用 **010 Editor**分析 及自带的**CLASSAdv.bt模版**进行分析

#### 魔数 magic number (u4)

class 文件开头的四个字节是 class 文件的魔数, 是固定值--0XCAFEBABE

#### 版本号(u4)

前两个字节 0000 代表次版本号（minor_version） 后两个字节 0034 (52) 是主版本号（major_version)

52.0,即jdk1.8

#### 常量池大小(u2)

其中下标为 0 的常量被 JVM 留作其他特殊用途,实际的常量池大小为这个计数器的值减 1

#### 常量池（重点）

14种常量类型

![constant类型](https://gitee.com/LeonShih/Image/raw/master/class_constant.png)



CONSTANT_Class_info结构

```
table CONSTANT_Class_info {
    u1  tag = 7;
    u2  name_index; //是一个索引值，可以将它理解为一个指针 指向常量池中索引为 name_index 的常量表
}
```

CONSTANT_Utf8_info 结构

```
table CONSTANT_utf8_info {
    u1  tag =1;
    u2  length; // 长度
    u1[] bytes; // u1 类型数组,大小为上面length
}
```

java代码中声明的String字符串最终在class文件中的存储格式就 CONSTANT_utf8_info。因此一个字符串最大长度也就是u2所能代表的最大值65536个，但是需要使用2个字节来保存 null 值，因此一个字符串的最大长度为 65536 - 2 = 65534



字段引用表CONSTANT_FIeldref_info结构

```
CONSTANT_Fieldref_info{
    u1 tag = 9;
    u2 class_index;        //指向此字段的所属类 常量池中索引
    u2 name_type_index;    //指向此字段的名称和类型  常量池中索引

}
```

方法引用表CONSTANT_NameAndType_info结构

```
CONSTANT_NameAndType_info{
    u1 tag;
    u2 name_index;    指向某字段或方法的名称字符串
    u2 type_index;    指向某字段或方法的类型字符串

}
```

#### 访问标志（access_flags u2）

可以是多个标志符号&操作

![access_flags](https://gitee.com/LeonShih/Image/raw/master/class_access_flag.png)

#### 类索引、父类索引与接口索引计数器 (u2 u2 u2)

this_class  super_class  interfaces_count

#### 接口数组(u2 * 接口索引计数器数量)

#### 字段表

紧跟在接口索引集合后面的就是字段表了，字段表的主要功能是用来描述类或者接口中声明的变量

字段表的具体结构如下：

```
CONSTANT_Fieldref_info{
    u2  access_flags    字段的访问标志
    u2  name_index          字段的名称索引(也就是变量名)
    u2  descriptor_index    字段的描述索引(也就是变量的类型)
    u2  attributes_count    属性计数器
    attribute_info
}
```

**字段访问标志**

对于 Java 类中的变量，也可以使用 public、private、final、static 等标识符进行标识

![field_flags](https://gitee.com/LeonShih/Image/raw/master/class_filed_flags.png)

**注意事项：**

- 字段表集合中不会列出从父类或者父接口中继承而来的字段。
- 内部类中为了保持对外部类的访问性，会自动添加指向外部类实例的字段。

#### 方法表

字段表之后跟着的就是方法表常量 (默认构造器方法也被包含在方法表常量中)

方法表的结构如下所示：

```
CONSTANT_Methodref_info{
    u2  access_flags;        方法的访问标志
    u2  name_index;          指向方法名的索引
    u2  descriptor_index;    指向方法类型的索引
    u2  attributes_count;    方法属性计数器
    attribute_info attributes;
}
```

![method_access_flags](https://gitee.com/LeonShih/Image/raw/master/class_method_access_flags.png)

#### 属性表

在之前解析字段和方法的时候，在它们的具体结构中我们都能看到有一个叫作 attributes_info 的表，这就是属性表。

属性表并没有一个固定的结构，各种不同的属性只要满足以下结构即可：

```
CONSTANT_Attribute_info{
    u2 name_index;
    u2 attribute_length length;
    u1[] info;
}
```

JVM 中预定义了很多属性表，这里重点讲一下 Code 属性表

Code 属性表中，最主要的就是一些列的字节码。 (通过javap -v Test.class 查看方法的字节码)

### 总结

了解了一个 class 文件内容的数据结构到底长什么样子，并通过 Test.class 来模拟演示Java虚拟机解析字节码文件的过程。其中 class 常量池部分是重点内容，它就相当于是 class 文件中的资源仓库，其他的几种结构或多或少都会最终指向到这个资源仓库中。

可以使用 javap 等命令或者是其他工具010 Eidtor，来帮助我们查看 class 内部的数据结构。