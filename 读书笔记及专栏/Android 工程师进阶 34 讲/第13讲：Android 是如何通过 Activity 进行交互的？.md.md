### taskAffinity

每一个 Activity 都有一个 Affinity 属性，如果不在清单文件中指定，默认为当前应用的包名

#### taskAffinity 会默认使 Activity 在新的栈中分配吗？

单纯使用 taskAffinity 不能导致 Activity 被创建在新的任务栈中，需要配合 singleTask 或者 singleInstance！

#### taskAffinity + allowTaskReparenting

allowTaskReparenting 赋予 Activity 在各个 Task 中间转移的特性。一个在后台任务栈中的 Activity A，当有其他任务进入前台，并且 taskAffinity 与 A 相同，则会自动将 A 添加到当前启动的任务栈中。

### 通过 Binder 传递数据的限制

Android 系统对使用 Binder 传数据进行了限制。通常情况为 1M，但是根据不同版本、不同厂商，这个值会有区别。

#### 解决办法：

- 减少通过 Intent 传递的数据，将非必须字段使用 transient 关键字修饰
- 将对象转化为 JSON 字符串，减少数据体积
-  EventBus 来实现数据传递。

### [process 造成多个 Application](https://agehua.github.io/2017/02/21/Multi-Process-Dispatch/)

- onCreate 方法中判断进程的名称，只有在符合要求的进程里，才执行初始化操作
- 抽象出一个与 Application 生命周期同步的类，并根据不同的进程创建相应的 Application 实例。

### 后台启动 Activity 失效

 Android10（API 29）开始，Android 系统对后台进程启动 Activity 做了一定的限制

Android 官方建议我们使用通知来替代直接启动 Activity 操作