> RecyclerView 简称 RV， 是作为 ListView 和 GridView 的加强版出现的，目的是在有限的屏幕之上展示大量的内容，因此 RecyclerView 的复用机制的实现是它的一个核心部分。

常用方法

- setLayoutManager：必选项，设置 RV 的布局管理器，决定 RV 的显示风格。常用的有线性布局管理器（LinearLayoutManager）、网格布局管理器（GridLayoutManager）、瀑布流布局管理器（StaggeredGridLayoutManager）。
- setAdapter：必选项，设置 RV 的数据适配器。当数据发生改变时，以通知者的身份，通知 RV 数据改变进行列表刷新操作。
- addItemDecoration：非必选项，设置 RV 中 Item 的装饰器，经常用来设置 Item 的分割线。
- setItemAnimator：非必选项，设置 RV 中 Item 的动画。

### 绘制流程分析

#### onMeasure

![img](https://s0.lgstatic.com/i/image/M00/09/9D/CgqCHl68ufiAe8_uAAV5DJyDB7M094.png)



1. 在 XML 布局文件中，RV 的宽高被设置为 match_parent 或者具体值，那么直接将 skipMeasure 置为 true，并调用 mLayout（传入的 LayoutManager）的 onMeasure 方法测量自身的宽高即可。
2. 在 XML 布局文件中，RV 的宽高设置为 wrap_content，则会执行下面的 dispatchLayoutStep2()，其实就是测量 RecyclerView 的子 View 的大小，最终确定 RecyclerView 的实际宽高。

 [dispatchLayoutStep1()](https://mp.weixin.qq.com/s?__biz=MzU3Mjc5NjAzMw==&mid=2247484487&idx=1&sn=bb0b7de72d20011199dcc140d6925f8e&chksm=fcca39a9cbbdb0bf29a48db16f3e7a019aa3d98da88d60389f79c91cbcf8092bdb6862513191&token=1367838045&lang=zh_CN#rd) 方法，这个方法并不是本节重点介绍内容，但是它跟RV的动画息息相关

#### onLayout ![img](https://s0.lgstatic.com/i/image/M00/09/9D/CgqCHl68ugWAAd26AAC9tEb_ppo563.png)



![img](https://s0.lgstatic.com/i/image/M00/09/9D/CgqCHl68ugyAIYT4AAUpCeyxNoY082.png)

如果在 onMeasure 阶段没有执行 dispatchLayoutStep2() 方法去测量子 View，则会在 onLayout 阶段重新执行。

dispatchLayoutStep2() 源码如下：

![img](https://s0.lgstatic.com/i/image/M00/09/9D/Ciqc1F68uhOAau0zAADCKkMOvDk556.png)

核心逻辑是调用了 mLayout 的 onLayoutChildren 方法。这个方法是 LayoutManager 中的一个空方法，主要作用是测量 RV 内的子 View 大小，并确定它们所在的位置。LinearLayoutManager、GridLayoutManager，以及 StaggeredLayoutManager 都分别复写了这个方法，并实现了不同方式的布局。

以 LinearLayoutManager 的实现为例，展开分析，实现如下 

![img](https://s0.lgstatic.com/i/image/M00/09/9D/Ciqc1F68uhyAbDK-AAVKbLTTats711.png)

1. 在 onLayoutChildren 中调用 fill 方法，完成子 View 的测量布局工作；
2. 在 fill 方法中通过 while 循环判断是否还有剩余足够空间来绘制一个完整的子 View；
3. layoutChunk 方法中是子 View 测量布局的真正实现，每次执行完之后需要重新计算 remainingSpace。

layoutChunk 是一个非常核心的方法，这个方法执行一次就填充一个 ItemView 到 RV，部分代码如下：

![img](https://s0.lgstatic.com/i/image/M00/09/9D/Ciqc1F68uiWAUf1UAAXuHvOgyg0072.png)



1. 从缓存（Recycler）中取出子 ItemView，然后调用 addView 或者 addDisappearingView 将子 ItemView 添加到 RV 中。
2. 测量被添加的 RV 中的子 ItemView 的宽高。
3. 根据所设置的 Decoration、Margins 等所有选项确定子 ItemView 的显示位置。

#### onDraw 

测量和布局都完成之后，就剩下最后的绘制操作了

![img](https://s0.lgstatic.com/i/image/M00/09/9D/CgqCHl68ui6ATOvDAADD582WJrs779.png)

### 小结

RV 会将测量 onMeasure 和布局 onLayout 的工作委托给 LayoutManager 来执行，不同的 LayoutManager 会有不同风格的布局显示，这是一种策略模式。用一张图来描述这段过程如下

![img](https://s0.lgstatic.com/i/image/M00/09/9D/Ciqc1F68ujuAJSpLAACb76c7LSA053.png)



### 缓存复用原理 Recycler

缓存复用是 RV 中另一个非常重要的机制，这套机制主要实现了 ViewHolder 的缓存以及复用。

核心代码是在 Recycler 中完成的，它是 RV 中的一个内部类，主要用来缓存屏幕内 ViewHolder 以及部分屏幕外 ViewHolder，部分代码如下：

![img](https://s0.lgstatic.com/i/image/M00/09/9D/Ciqc1F68ukSAZT3uAAD9_pc55Io230.png)

Recycler 的缓存机制就是通过上图中的这些数据容器来实现的，实际上 Recycler 的缓存也是分级处理的，根据访问优先级从上到下可以分为 4 级，如下：

![img](https://s0.lgstatic.com/i/image/M00/09/9D/Ciqc1F68ukuAM0LVAACHb_a34AY925.png)

#### 各级缓存功能

- 第一级缓存 mAttachedScrap&mChangedScrap

  主要用来缓存屏幕内的 ViewHolder

  通过下拉刷新列表中的内容，当刷新被触发时，只需要在原有的 ViewHolder 基础上进行重新绑定新的数据 data 即可，而这些旧的 ViewHolder 就是被保存在 mAttachedScrap 和 mChangedScrap 中。

- 第二级缓存 mCachedViews

  用来缓存移除屏幕之外的 ViewHolder，默认情况下缓存个数是 2，不过可以通过 setViewCacheSize 方法来改变缓存的容量大小。如果 mCachedViews 的容量已满，则会根据 FIFO 的规则将旧 ViewHolder 抛弃

  ![img](https://s0.lgstatic.com/i/image/M00/09/9D/CgqCHl68umKATRCWAEsI2pK1Mbo977.gif)

- 第三级缓存 ViewCacheExtension

  这是 RV 预留给开发人员的一个抽象类，在这个类中只有一个抽象方法

  ![img](https://s0.lgstatic.com/i/image/M00/09/9D/Ciqc1F68umuAdLe4AACjFw2gqeI470.png)

- 第四级缓存 RecycledViewPool

  RecycledViewPool 同样是用来缓存屏幕外的 ViewHolder，当 mCachedViews 中的个数已满（默认为 2），则从 mCachedViews 中淘汰出来的 ViewHolder 会先缓存到 RecycledViewPool 中。

  ViewHolder 在被缓存到 RecycledViewPool 时，会将内部的数据清理，因此从 RecycledViewPool 中取出来的 ViewHolder 需要重新调用 onBindViewHolder 绑定数据。

  **需要注意的是，RecycledViewPool 是根据 type 来获取 ViewHolder，每个 type 默认最大缓存 5 个**

### RV 是如何从缓存中获取 ViewHolder 的

在上文介绍 onLayout 阶段时，有介绍在 layoutChunk 方法中通过调用 layoutState.next 方法拿到某个子 ItemView，然后添加到 RV 中。

看一下 layoutState.next 的详细代码：

![img](https://s0.lgstatic.com/i/image/M00/09/9E/CgqCHl68unSAJ7fRAAEcvzM2UaA043.png)

![img](https://s0.lgstatic.com/i/image/M00/09/9E/CgqCHl68un6AKPaiAADjaBH3UEk983.png)

![img](https://s0.lgstatic.com/i/image/M00/09/9E/Ciqc1F68uoaAefdEAASuYKk5V44193.png)

会从上面介绍的 4 级缓存中依次查找

如图中红框处所示，如果在各级缓存中都没有找到相应的 ViewHolder，则会使用 Adapter 中的 createViewHolder 方法创建一个新的 ViewHolder。

### 何时将 ViewHolder 存入缓存

#### 第一次 layout

当调用 setLayoutManager 和 setAdapter 之后，RV 会经历第一次 layout 并被显示到屏幕上

![img](https://s0.lgstatic.com/i/image/M00/09/9E/CgqCHl68upCATZ2cAACZFf9DHQ0775.png)

此时并不会有任何 ViewHolder 的缓存，所有的 ViewHolder 都是通过 createViewHolder 创建的。

#### 刷新列表

如果通过手势下拉刷新，获取到新的数据 data 之后，我们会调用 notifyXXX 方法通知 RV 数据发生改变，这回 RV 会先将屏幕内的所有 ViewHolder 保存在 Scrap 中，如下所示：

![img](https://s0.lgstatic.com/i/image/M00/09/9E/CgqCHl68upeAaYMqAAChKtO2D8A471.png)

当缓存执行完之后，后续通过 Recycler 就可以从缓存中获取相应 position 的 ViewHolder（姑且称为旧 ViewHolder），然后将刷新后的数据设置到这些 ViewHolder 上，如下所示：

![img](https://s0.lgstatic.com/i/image/M00/09/9E/Ciqc1F68up6AKO1yAAChI3xlrRw121.png)

最后再将新的 ViewHolder 绘制到 RV 上

![img](https://s0.lgstatic.com/i/image/M00/09/9E/CgqCHl68uqSAS-0JAACL6DDqSsw611.png)