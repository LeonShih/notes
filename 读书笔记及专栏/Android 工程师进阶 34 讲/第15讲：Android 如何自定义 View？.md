自定义 UI 控件有 2 种方式：

- 继承系统提供的成熟控件（比如 LinearLayout、RelativeLayout、ImageView 等）；
- 直接继承自系统 View 或者 ViewGroup，并自绘显示内容。

### 继承现有控件

#### 自定义属性

#### attrs.xml 中声明自定义属性

在 res 的 values 目录下的 attrs.xml 文件中

![img](https://s0.lgstatic.com/i/image/M00/08/47/Ciqc1F66bpuAIr0aAAFqNwcLsJ0889.png)

#### 在 XML 布局文件中使用自定义属性

加入xmlns:app

#### 获取自定义属性的引用值

Context.obtainStyleAttributes 方法获取到自定义属性的集合，

### 直接继承自系统 View 或者 ViewGroup

这种方式相比第一种麻烦一些，但是更加灵活，也能实现更加复杂的 UI 界面。一般情况下使用这种实现方式需要解决以下几个问题：

- 如何根据相应的属性将 UI 元素绘制到界面；
- 自定义控件的大小，也就是宽和高分别设置多少；
- 如果是 ViewGroup，如何合理安排其内部子 View 的摆放位置。
  以上 3 个问题依次在如下 3 个方法中得到解决：

- onDraw
- onMeasure
- onLayout

#### onDraw

onDraw 方法接收一个 Canvas 类型的参数。Canvas 可以理解为一个画布，在这块画布上可以绘制各种类型的 UI 元素。

 Canvas 操作方法，如下

![img](https://s0.lgstatic.com/i/image/M00/08/47/Ciqc1F66brqANYwaAAFgenmfG7o790.png)

Paint的方法如下

![img](https://s0.lgstatic.com/i/image/M00/08/47/CgqCHl66bsKAC3aYAAEfignRLSI590.png)



#### onMeasure

自定义 View 为什么需要重新测量。正常情况下，我们直接在 XML 布局文件中定义好 View 的宽高，然后让自定义 View 在此宽高的区域内显示即可。**但是为了更好地兼容不同尺寸的屏幕，Android 系统提供了 wrap_contetn 和 match_parent 属性来规范控件的显示规则**。它们分别代表自适应大小和填充父视图的大小，但是这两个属性并没有指定具体的大小，因此我们需要在 onMeasure 方法中过滤出这两种情况，真正的测量出自定义 View 应该显示的宽高大小。



```
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec{
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  }
```

 widthMeasureSpec 和 heightMeasureSpec。这两个参数是从父视图传递给子 View 的两个参数，看起来很像宽、高，但是它们所表示的不仅仅是宽和高，还有一个非常重要的测量模式。

一共有 3 种测量模式。

- EXACTLY：表示在 XML 布局文件中宽高使用 match_parent 或者固定大小的宽高；
- AT_MOST：表示在 XML 布局文件中宽高使用 wrap_content；
- UNSPECIFIED：父容器没有对当前 View 有任何限制，当前 View 可以取任意尺寸，比如 ListView 中的 item。

实际上 widthMeasureSpec 和 heightMeasureSpec 都是使用二进制高 2 位表示测量模式，低 30 位表示宽高具体大小。

View 中的 onMeasure 默认实现如下：

setMeasuredDimension  传入的值直接决定 View 的宽高，也就是说如果调用 setMeasuredDimension(100,200)，最终 View 就显示宽 100 * 高 200 的矩形范围。



##### ViewGroup 中的 onMeasure

 ViewGroup 在测量自己的宽高之前，需要先确定其内部子 View 的所占大小，然后才能确定自己的大小。

- 调用 measureChild 方法递归测量子 View；
- 通过叠加每一行的高度，计算出ViewGroup的最终高度 totalHeight。

#### onLayout

上面的  onMeasure 方法只是计算出 ViewGroup 的最终显示宽高，但是并没有规定某一个子 View 应该显示在何处位置。要定义 ViewGroup 内部子 View 的显示规则，则需要复写并实现 onLayout 方法。

它是一个抽象方法，也就是说每一个自定义 ViewGroup 都必须主动实现如何排布子 View，具体就是遍历每一个子 View，调用 child.layout(l, t, r, b) 方法来为每个子 View 设置具体的布局位置。四个参数分别代表左上右下的坐标位置