第07讲：Java 内存模型与线程

### 为什么有 Java 内存模型

 JMM

![img](https://s0.lgstatic.com/i/image3/M01/0F/5C/Ciqah16VdfyAKW4PAADwKkKf3ho258.png)

在 Java 线程中并不存在所谓的工作内存（working memory），它只是对 CPU 寄存器和高速缓存的抽象描述



Java 中所有数据都是存放在主内存（RAM）当中的，这一过程可以参考下图

![img](https://s0.lgstatic.com/i/image3/M01/88/72/Cgq2xl6VddeAEMpoAAB8hyBTG6s068.png)

为了“压榨”处理性能，达到“高并发”的效果，在 CPU 中添加了高速缓存 （cache）来作为缓冲。

![img](https://s0.lgstatic.com/i/image3/M01/0F/5B/Ciqah16VddeAQQn6AACp-HxFwfo279.png)

在执行任务时，CPU 会先将运算所需要使用到的数据复制到高速缓存中，让运算能够快速进行，当运算完成之后，再将缓存中的结果刷回（flush back）主内存，这样 CPU 就不用等待主内存的读写操作了。



### 缓存一致性问题

如果你的 Java 程序是多线程的，那么就有可能存在多个线程在同一时刻被不同的CPU执行的情况

### 指令重排

为了使 CPU 内部的运算单元能够尽量被充分利用，处理器可能会对输入的字节码指令进行重排序处理，也就是处理器优化

以下面的代码为例：

![img](https://s0.lgstatic.com/i/image3/M01/88/72/Cgq2xl6VddiAFch6AAANXK2e6Ic978.png)

编译之后的字节码指令如下：

![img](https://s0.lgstatic.com/i/image3/M01/88/72/Cgq2xl6Vdk2ADgOkAAEolgP7ZVw813.png)

可以看出在上述指令中，有两处指令表达的是同样的语义，并且指令 7 并不依赖指令 2 和指令 3。在这种情况下，CPU 会对指令的顺序做优化，如下：

![img](https://s0.lgstatic.com/i/image3/M01/0F/5B/Ciqah16VddmAMhEsAAGv_L-gOOI288.png)

从 Java 语言的角度看这层优化就是：

![img](https://s0.lgstatic.com/i/image3/M01/88/72/Cgq2xl6VddmAe3StAACvXj7iPyM456.png)

也就是说在 CPU 层面，有时候代码并不会严格按照 Java 文件中的顺序去执行。再看一下之前 r1/r2 的实例，刚才我们分析会有 3 种情况发生，其实在极端情况下，还会出现第 4 种情况：

r1 = 2，r2 = 1

线程 p2 中的代码经过 CPU 优化之后，会被重排序为：

![img](https://s0.lgstatic.com/i/image3/M01/88/72/Cgq2xl6VdnCAI0SsAAEI1td3ZTw250.png)

经过优化之后，p2 线程将 x 赋值为 2，这时 CPU 将时间片段分配给线程 p1，线程 p1 在执行过程中，将 r1 赋值为 x，此时 x = 2，所以 r1 的值为 2。然后将 y 赋值为 1，此时 CPU 再将时间片段重新分配给 p2。



代码回到 p2 中，将 y 值赋值给 r2，此时 y = 1，所以 r2 = 1，整个过程如下图所示：

![img](https://s0.lgstatic.com/i/image3/M01/0F/5B/Ciqah16VddqAPWm2AAF1DtJNPU4827.png)

图中红色图标代表代码执行的顺序。

上面两小部分内容表明，如果我们任由 CPU 优化或者编译器指令重排，那我们编写的 Java 代码最终执行效果可能会极大的出乎意料。为了解决这个问题，让 Java 代码在不同硬件、不同操作系统中，输出的结果达到一致，Java 虚拟机规范提出了一套机制——Java 内存模型。

### 什么是内存模型

> 内存模型是一套共享内存系统中多线程读写操作行为的规范，这套规范屏蔽了底层各种硬件和操作系统的内存访问差异，解决了 CPU 多级缓存、CPU 优化、指令重排等导致的内存访问问题，从而保证 Java 程序（尤其是多线程程序）在各种平台下对内存的访问效果一致。

在 Java 内存模型中，我们统一用工作内存（working memory）来当作 CPU 中寄存器或高速缓存的抽象。线程之间的共享变量存储在主内存（main memory）中，每个线程都有一个私有工作内存（类比 CPU 中的寄存器或者高速缓存），本地工作内存中存储了该线程读/写共享变量的副本。

在这套规范中，有一个非常重要的规则——happens-before。

### happens-before 先行发生原则

> happens-before 用于描述两个操作的内存可见性，通过保证可见性的机制可以让应用程序免于数据竞争干扰。它的定义如下：
>
> ​      如果一个操作 A happens-before 另一个操作 B，那么操作 A 的执行结果将对操作 B 可见。
>
> 上述定义我们也可以反过来理解：如果操作 A 的结果需要对另外一个操作 B 可见，那么操作 A 必须 happens-before 操作 B。

 JMM 中定义了以下几种情况是自动符合 happens-before 规则的：

- 程序次序规则

  若前后无依赖关系会发生指令重排

  ```
  //会重排
  int a = 10;  // 1
  b = b + 1;   // 2
  ```

  ```
  //不会重排
  int a = 10;  // 1
  b = b + a;   // 2
  
  ```

- 锁定规则

  无论是在单线程环境还是多线程环境，一个锁如果处于被锁定状态，那么必须先执行 unlock 操作后才能进行 lock 操作

- 变量规则

  volatile 保证了线程可见性

- 线程启动规则

  Thread 对象的 start() 方法先行发生于此线程的每一个动作。假定线程 A 在执行过程中，通过执行 ThreadB.start() 来启动线程 B，那么线程 A 对共享变量的修改在线程 B 开始执行后确保对线程 B 可见。

- 线程中断规则

  对线程 interrupt() 方法的调用先行发生于被中断线程的代码检测，直到中断事件的发生

- 线程终结规则

  线程中所有的操作都发生在线程的终止检测之前，我们可以通过 Thread.join() 方法结束、Thread.isAlive() 的返回值等方法检测线程是否终止执行。假定线程 A 在执行的过程中，通过调用 ThreadB.join() 等待线程 B 终止，那么线程 B 在终止之前对共享变量的修改在线程 A 等待返回后可见。

- 对象终结规则

  一个对象的初始化完成发生在它的 finalize() 方法开始前

happens-before 原则还具有传递性



### Java 内存模型应用

 happens-before 原则非常重要，它是判断数据是否存在竞争、线程是否安全的主要依据

happens-before 化

- 使用 volatile 修饰 value
- 使用synchronized关键字修饰操作

### 总结

**Java 内存模型的来源**：主要是因为 CPU 缓存和指令重排等优化会造成多线程程序结果不可控。

**Java 内存模型是什么**：本质上它就是一套规范，在这套规范中有一条最重要的 happens-before 原则。

最后介绍了 **Java 内存模型的使用**，其中简单介绍了两种方式：volatile 和 synchronized。其实除了这两种方式，Java 还提供了很多关键字来实现 happens-before 原则。 

