

##  class 文件加载到内存的步骤

![ class 文件加载到内存的步骤](https://s0.lgstatic.com/i/image3/M01/85/E8/Cgq2xl6O2nSAXgX1AAAk3WIjy2w291.png)

## 装载

> 装载是指 Java 虚拟机查找 .class 文件并生成字节流，然后根据字节流创建 java.lang.Class 对象的过程。

装载主要完成以下 3 件事

- ClassLoader 通过一个类的全限定名（包名 + 类名）来查找 .class 文件，并生成二进制字节流：其中 class 字节码文件的来源不一定是 .class 文件，也可以是 jar 包、zip 包，甚至是来源于网络的字节流。
- 把 .class 文件的各个部分分别解析（parse）为 JVM 内部特定的数据结构，并存储在方法区。
- 在内存中创建一个 java.lang.Class 类型的对象

接下来程序在运行过程中所有对该类的访问都通过这个类对象，也就是这个 Class 类型的类对象是提供给外界访问该类的接口。

#### 加载时机

一个项目经过编译之后，往往会生成大量的 .class 文件。当程序运行时，JVM 并不会一次性的将这些 .class 文件全部加载到内存中

- 隐式装载：在程序运行过程中，当碰到通过 new 等方式生成对象时，系统会隐式调用 ClassLoader 去装载对应的 class 到内存中
- 显示装载：在编写源代码时，主动调用 Class.forName() 等方法也会进行 class 装载操作，这种方式通常称为显示装载

## 链接

链接过程分为 3 步：验证、准备、解析。

示例代码

![img](https://s0.lgstatic.com/i/image3/M01/0C/D1/Ciqah16O2nSAJocBAADgYuyGOFU999.png)

### 验证

> 确保 .class 文件的字节流中包含的信息符合当前虚拟机的要求，并且不会危及虚拟机本身的安全

**检验内容**

- 文件格式检验：检验字节流是否符合 class 文件格式的规范，并且能被当前版本的虚拟机处理。 

- 元数据检验：对字节码描述的信息进行语义分析，以保证其描述的内容符合 Java 语言规范的要求。 

- 字节码检验：通过数据流和控制流分析，确定程序语义是合法、符合逻辑的。 

- 符号引用检验：符号引用检验可以看作是对类自身以外（常量池中的各种符号引用）的信息进行匹配性校验。

### 准备

> 主要目的是为类中的静态变量分配内存

```
public static int value = 100;
//在准备阶段，JVM 会为 value 分配内存，并将其设置为 0。而真正的值 100 是在初始化阶段设置
public static final int value = 100;
//在准备阶段就为 value 分配内存，并设置为 100
```

**Java 中基本类型的默认”0值“如下：**

- 基本类型（int、long、short、char、byte、boolean、float、double）的默认值为 0；

- 引用类型默认值是 null；

### 解析

> 把常量池中的符号引用转换为直接引用，也就是具体的内存地址
>
> 在这一阶段，JVM 会将常量池中的类、接口名、字段名、方法名等转换为具体的内存地址。

![](https://s0.lgstatic.com/i/image3/M01/0C/D2/Ciqah16O2niAFbl-AAZKjAhPB7w610.png)

在 main 方法中通过 invokevirtual 指令调用了 print 方法，“Foo.print:()V"就是一个符号引用，当main 方法执行到此处时，会将符号引用“Foo.print:()V”解析（resolve）成直接引用，可以将直接引用理解为方法真正的内存地址。

### 初始化

> 执行类构造器<clinit>方法的过程，并真正初始化类变量

**初始化的时机** 

- 虚拟机启动时，初始化**包含 main 方法的主类**；

- 遇到 **new** 指令创建对象实例时，如果目标对象类没有被初始化则进行初始化操作；

- 当遇到访问**静态方法或者静态字段**的指令时，如果目标对象类没有被初始化则进行初始化操作；

- 子类的初始化过程如果发现其父类还没有进行过初始化，则需要先触发其父类的初始化；

- 使用反射 API 进行**反射调用**时，如果类没有进行过初始化则需要先触发其初始化；

- 第一次调用 java.lang.invoke.MethodHandle 实例时，需要初始化 MethodHandle 指向方法所在的类。

上述的 6 种情况在 JVM 中被称为主动引用,之外的称为**被动引用**。**被动引用并不会触发 class 的初始化**

如子类继承父类的静态变量,用子类调用是不会触发子类初始化

```
public class ClazzInit {
    public static void main(String[] args) {
        System.out.println(Bar.a);
        Bar.a = 2;
        System.out.println(Bar.a);
        System.out.println(Foo.a);
        System.out.println(Bar.b);
        new Bar();
    }
}

class Foo {
    static {
        System.out.println("Foo");
    }
    public static int a = 1;
    public static final int b = 1;
}

class Bar extends Foo {
    static {
        System.out.println("Bar");
    }
}
```

### 对象的初始化顺序如下

静态变量/静态代码块 -> 普通代码块 -> 构造函数

- 父类静态变量和静态代码块；

- 子类静态变量和静态代码块；

- 父类普通成员变量和普通代码块；

- 父类的构造函数；

- 子类普通成员变量和普通代码块；

- 子类的构造函数。

## 总结

主要介绍了 **.class 文件被加载到内存**中所经过的详细过程，主要分 3 大步：**装载、链接、初始化**。其中链接中又包含验证、准备、解析 3 小步。

- 装载：指查找字节流，并根据此字节流创建类的过程。装载过程成功的标志就是在方法区中成功创建了类所对应的 Class 对象。

- 链接：指验证创建的类，并将其解析到 JVM 中使之能够被 JVM 执行。

- 初始化：则是将标记为 static 的字段进行赋值，并且执行 static 标记的代码语句 。