## Windows Batch

### 0x00 命令一览 help | more

```
>help | more
有关某个命令的详细信息，请键入 HELP 命令名
ASSOC          显示或修改文件扩展名关联。
ATTRIB         显示或更改文件属性。
BREAK          设置或清除扩展式 CTRL+C 检查。
BCDEDIT        设置启动数据库中的属性以控制启动加载。
CACLS          显示或修改文件的访问控制列表(ACL)。
CALL           从另一个批处理程序调用这一个。
CD             显示当前目录的名称或将其更改。
CHCP           显示或设置活动代码页数。
CHDIR          显示当前目录的名称或将其更改。
CHKDSK         检查磁盘并显示状态报告。
CHKNTFS        显示或修改启动时间磁盘检查。
CLS            清除屏幕。
CMD            打开另一个 Windows 命令解释程序窗口。
COLOR          设置默认控制台前景和背景颜色。
COMP           比较两个或两套文件的内容。
COMPACT        显示或更改 NTFS 分区上文件的压缩。
CONVERT        将 FAT 卷转换成 NTFS。你不能转换当前驱动器。
COPY           将至少一个文件复制到另一个位置。
DATE           显示或设置日期。
DEL            删除至少一个文件。
DIR            显示一个目录中的文件和子目录。
DISKPART       显示或配置磁盘分区属性。
DOSKEY         编辑命令行、撤回 Windows 命令并创建宏。
DRIVERQUERY    显示当前设备驱动程序状态和属性。
ECHO           显示消息，或将命令回显打开或关闭。
ENDLOCAL       结束批文件中环境更改的本地化。
ERASE          删除一个或多个文件。
EXIT           退出 CMD.EXE 程序(命令解释程序)。
FC             比较两个文件或两个文件集并显示它们之间的不同。
FIND           在一个或多个文件中搜索一个文本字符串。
FINDSTR        在多个文件中搜索字符串。
FOR            为一组文件中的每个文件运行一个指定的命令。
FORMAT         格式化磁盘，以便用于 Windows。
FSUTIL         显示或配置文件系统属性。
FTYPE          显示或修改在文件扩展名关联中使用的文件类型。
GOTO           将 Windows 命令解释程序定向到批处理程序中某个带标签的行。
GPRESULT       显示计算机或用户的组策略信息。
GRAFTABL       使 Windows 在图形模式下显示扩展字符集。
HELP           提供 Windows 命令的帮助信息。
ICACLS         显示、修改、备份或还原文件和目录的 ACL。
IF             在批处理程序中执行有条件的处理操作。
LABEL          创建、更改或删除磁盘的卷标。
MD             创建一个目录。
MKDIR          创建一个目录。
MKLINK         创建符号链接和硬链接
MODE           配置系统设备。
MORE           逐屏显示输出。
MOVE           将一个或多个文件从一个目录移动到另一个目录。
OPENFILES      显示远程用户为了文件共享而打开的文件。
PATH           为可执行文件显示或设置搜索路径。
PAUSE          暂停批处理文件的处理并显示消息。
POPD           还原通过 PUSHD 保存的当前目录的上一个值。
PRINT          打印一个文本文件。
PROMPT         更改 Windows 命令提示。
PUSHD          保存当前目录，然后对其进行更改。
RD             删除目录。
RECOVER        从损坏的或有缺陷的磁盘中恢复可读信息。
REM            记录批处理文件或 CONFIG.SYS 中的注释(批注)。
REN            重命名文件。
RENAME         重命名文件。
REPLACE        替换文件。
RMDIR          删除目录。
ROBOCOPY       复制文件和目录树的高级实用工具
SET            显示、设置或删除 Windows 环境变量。
SETLOCAL       开始本地化批处理文件中的环境更改。
SC             显示或配置服务(后台进程)。
SCHTASKS       安排在一台计算机上运行命令和程序。
SHIFT          调整批处理文件中可替换参数的位置。
SHUTDOWN       允许通过本地或远程方式正确关闭计算机。
SORT           对输入排序。
START          启动单独的窗口以运行指定的程序或命令。
SUBST          将路径与驱动器号关联。
SYSTEMINFO     显示计算机的特定属性和配置。
TASKLIST       显示包括服务在内的所有当前运行的任务。
TASKKILL       中止或停止正在运行的进程或应用程序。
TIME           显示或设置系统时间。
TITLE          设置 CMD.EXE 会话的窗口标题。
TREE           以图形方式显示驱动程序或路径的目录结构。
TYPE           显示文本文件的内容。
VER            显示 Windows 的版本。
VERIFY         告诉 Windows 是否进行验证，以确保文件正确写入磁盘。
VOL            显示磁盘卷标和序列号。
XCOPY          复制文件和目录树。
WMIC           在交互式命令 shell 中显示 WMI 信息。
```



如何查看命令解释----- 命令 help [子命令]  或者命令 /？

### 0x01批处理基础

#### 0x0100 常用批处理内部命令简介

##### 预热一下

```
@echo off
rem say hello 
echo hello batch
pause
```

##### REM 和 ::   注释

- ​	rem  可回显
- ​    ::    不回显 

##### ECHO 和 @   回显 及隐藏命令

```
>help echo 
显示消息，或者启用或关闭命令回显。

  ECHO [ON | OFF]
  ECHO [message]
```

@字符放在命令前将关闭该命令回显，无论此时 echo 是否为打开状态。

通常以@echo off作为批处理程序的首行



##### PAUSE  暂停

```
C:\Users\Leon>pause
请按任意键继续. . .

:: 修改提示语
>echo 继续 & pause>nul
继续
```



##### ERRORLEVEL

```
:: 默认是0  无效命令时值为9009
>echo %errorlevel%
0

>dd
'dd' 不是内部或外部命令，也不是可运行的程序
或批处理文件。

>echo %errorlevel%
9009
```

##### TITLE 修改标题

```
>title hello
:: 注意窗口的标题变化
```

##### COLOR

> 设置默认的控制台前景和背景颜色。
>
> COLOR [attr]
>
>   attr        指定控制台输出的颜色属性。
>
> 颜色属性由两个十六进制数字指定 -- 第一个
> 对应于背景，第二个对应于前景。每个数字
> 可以为以下任何值:
>
>     0 = 黑色       8 = 灰色
>     1 = 蓝色       9 = 淡蓝色
>     2 = 绿色       A = 淡绿色
>     3 = 浅绿色     B = 淡浅绿色
>     4 = 红色       C = 淡红色
>     5 = 紫色       D = 淡紫色
>     6 = 黄色       E = 淡黄色
>     7 = 白色       F = 亮白色
>
> 如果没有给定任何参数，此命令会将颜色还原到 CMD.EXE 启动时
> 的颜色。这个值来自当前控制台
> 窗口、/T 命令行开关或 DefaultColor 注册表
> 值。
>
> 如果尝试使用相同的
> 前景和背景颜色来执行
>  COLOR 命令，COLOR 命令会将 ERRORLEVEL 设置为 1。

```
>color fc
:: 注意命令窗口的背景及字体变化
```



##### mode 配置系统设备

> 配置系统设备。
>
> 串行端口:          MODE COMm[:] [BAUD=b] [PARITY=p] [DATA=d] [STOP=s]
>                                 [to=on|off] [xon=on|off] [odsr=on|off]
>                                 [octs=on|off] [dtr=on|off|hs]
>                                 [rts=on|off|hs|tg] [idsr=on|off]
>
> 设备状态:          MODE [device] [/STATUS]
>
> 打印重定向:        MODE LPTn[:]=COMm[:]
>
> 选择代码页:        MODE CON[:] CP SELECT=yyy
>
> 代码页状态:        MODE CON[:] CP [/STATUS]
>
> 显示模式:          MODE CON[:] [COLS=c] [LINES=n]
>
> 击键率:            MODE CON[:] [RATE=r DELAY=d]



```
>mode con cols=113 lines=15 & color 9f
::此命令设置 DOS 窗口大小：15 行，113 列
```

##### GOTO 和 :  跳转标签

> 将 cmd.exe 定向到批处理程序中带标签的行。
>
> GOTO label
>
>   label   指定批处理程序中用作标签的文字字符串。
>
> 标签必须单独一行，并且以冒号打头。
>
> 如果命令扩展被启用，GOTO 会如下改变:
>
> GOTO 命令现在接受目标标签 :EOF，这个标签将控制转移到当前
> 批脚本文件的结尾。不定义就退出批脚本文件，这是一个容易的
> 办法。有关能使该功能有用的 CALL 命令的扩展描述，请键入



```
@echo off
:start
set /a var+=1
echo %var%
if %var% leq 3 GOTO start
pause
:: 输出  1 2 3 4
```

##### FIND

> 在文件中搜索字符串。
>
> FIND [/V] [/C] [/N] [/I] [/OFF[LINE]] "string" [[drive:][path]filename[ ...]]
>
>   /V         显示所有未包含指定字符串的行。
>   /C         仅显示包含字符串的行数。
>   /N         显示行号。
>   /I         搜索字符串时忽略大小写。
>   /OFF[LINE] 不要跳过具有脱机属性集的文件。
>   "string" 指定要搜索的文本字符串。
>   [drive:][path]filename
>              指定要搜索的文件。
>
> 如果没有指定路径，FIND 将搜索在提示符处键入
> 的文本或者由另一命令产生的文本。

find 常与type 一起使用

```
@echo off
echo 111 >test.txt
echo 222 >>test.txt
find "111" test.txt 
type test.txt|find "222"
pause
del test.txt
pause
```



##### FINDSTR

> 在文件中寻找字符串。
>
> FINDSTR [/B] [/E] [/L] [/R] [/S] [/I] [/X] [/V] [/N] [/M] [/O] [/P] [/F:file]
>         [/C:string] [/G:file] [/D:dir list] [/A:color attributes] [/OFF[LINE]]
>         strings [[drive:][path]filename[ ...]]
>
>   /B         在一行的开始配对模式。
>   /E         在一行的结尾配对模式。
>   /L         按字使用搜索字符串。
>   /R         将搜索字符串作为一般表达式使用。
>   /S         在当前目录和所有子目录中搜索匹配文件。
>   /I         指定搜索不分大小写。
>   /X         打印完全匹配的行。
>   /V         只打印不包含匹配的行。
>   /N         在匹配的每行前打印行数。
>   /M         如果文件含有匹配项，只打印其文件名。
>   /O         在每个匹配行前打印字符偏移量。
>   /P         忽略有不可打印字符的文件。
>   /OFF[LINE] 不跳过带有脱机属性集的文件。
>   /A:attr    指定有十六进位数字的颜色属性。请见 "color /?"
>   /F:file    从指定文件读文件列表 (/ 代表控制台)。
>   /C:string  使用指定字符串作为文字搜索字符串。
>   /G:file    从指定的文件获得搜索字符串。 (/ 代表控制台)。
>   /D:dir     查找以分号为分隔符的目录列表
>   strings    要查找的文字。
>   [drive:][path]filename
>              指定要查找的文件。
>
> 除非参数有 /C 前缀，请使用空格隔开搜索字符串。
> 例如: 'FINDSTR "hello there" x.y' 在文件 x.y 中寻找 "hello" 或
> "there"。'FINDSTR /C:"hello there" x.y' 文件 x.y  寻找
> "hello there"。
>
> 一般表达式的快速参考:
>   .        通配符: 任何字符
>   *        重复: 以前字符或类出现零或零以上次数
>     ^        行位置: 行的开始
>     $        行位置: 行的终点
>     [class]  字符类: 任何在字符集中的字符
>     [^class] 补字符类: 任何不在字符集中的字符
>     [x-y]    范围: 在指定范围内的任何字符
>     \x       Escape: 元字符 x 的文字用法
>     \<xyz    字位置: 字的开始
>     xyz\>    字位置: 字的结束
>
> 有关 FINDSTR 常见表达法的详细情况，请见联机命令参考。



```
findstr "111" test.txt 
type test.txt|findstr "222"
```



##### START

> 启动一个单独的窗口以运行指定的程序或命令。

```
>start cmd
:: 打开命令行
```



##### assoc 和 ftype  显示或修改文件扩展名关联

> ASSOC [.ext[=[fileType]]]
>
>   .ext      指定跟文件类型关联的文件扩展名
>   fileType  指定跟文件扩展名关联的文件类型
>
> 键入 ASSOC 而不带参数，显示当前文件关联。如果只用文件扩展
> 名调用 ASSOC，则显示那个文件扩展名的当前文件关联。如果不为
> 文件类型指定任何参数，命令会删除文件扩展名的关联。

```
assoc #显示所有'文件扩展名'关联
assoc.txt #显示.txt 代表的'文件类型'，结果显示 .txt=txtfile
assoc.doc #显示.doc代表的'文件类型'，结果显示 .doc=Word.Document.8
assoc.exe #显示.exe代表的'文件类型'，结果显示 .exe=exefile
ftype #显示所有'文件类型'关联
ftypeexefile #显示 exefile 类型关联的命令行，结果显示 exefile="%1" %*
assoc.txt=Word.Document.8
设置.txt 为 word 类型的文档，可以看到.txt 文件的图标都变了
assoc.txt=txtfile
恢复.txt 的正确关联
ftypeexefile="%1" %*
恢复 exefile 的正确关联
如果该关联已经被破坏，可以运行 command.com ，
```



##### pushd 和 popd  切换当前目录

```
@echo off
c: & cd\ & md mp3 #在 C:\ 建立 mp3 文件夹
md d:\mp4 #在 D:\ 建立 mp4 文件夹
cd /d d:\mp4 #更改当前目录为 d:\mp4
pushd c:\mp3 #保存当前目录，并切换当前目录为 c:\mp3
popd #恢复当前目录为刚才保存的 d:\mp4
一般用处不大，在当前目录名不确定时，会有点帮助。（dos 编程中很有用）
```



##### CALL

> 从批处理程序调用另一个批处理程序。
>
> CALL [drive:][path]filename [batch-parameters]
>
>   batch-parameters   指定批处理程序所需的命令行信息。
>
> 如果命令扩展被启用，CALL 会如下改变:
>
> CALL 命令现在将卷标当作 CALL 的目标接受。语法是:
>
>     CALL:label arguments
>
> 一个新的批文件上下文由指定的参数所创建，控制在卷标被指定
> 后传递到语句。你必须通过达到批脚本文件末两次来 "exit" 两次。
> 第一次读到文件末时，控制会回到 CALL 语句的紧后面。第二次
> 会退出批脚本。键入 GOTO /?，参看 GOTO :EOF 扩展的描述，
> 此描述允许你从一个批脚本返回。
>
> 另外，批脚本文本参数参照(%0、%1、等等)已如下改变:
>
>
>      批脚本里的 %* 指出所有的参数(如 %1 %2 %3 %4 %5 ...)
>    
>      批参数(%n)的替代已被增强。你可以使用以下语法:
>    
>          %~1         - 删除引号(")，扩展 %1
>          %~f1        - 将 %1 扩展到一个完全合格的路径名
>          %~d1        - 仅将 %1 扩展到一个驱动器号
>          %~p1        - 仅将 %1 扩展到一个路径
>          %~n1        - 仅将 %1 扩展到一个文件名
>          %~x1        - 仅将 %1 扩展到一个文件扩展名
>          %~s1        - 扩展的路径只含有短名
>          %~a1        - 将 %1 扩展到文件属性
>          %~t1        - 将 %1 扩展到文件的日期/时间
>          %~z1        - 将 %1 扩展到文件的大小
>          %~$PATH:1   - 查找列在 PATH 环境变量的目录，并将 %1
>                        扩展到找到的第一个完全合格的名称。如果
>                        环境变量名未被定义，或者没有找到文件，
>                        此修改符会扩展到空字符串
>    
>     可以组合修改符来取得多重结果:
>    
>         %~dp1       - 只将 %1 扩展到驱动器号和路径
>         %~nx1       - 只将 %1 扩展到文件名和扩展名
>         %~dp$PATH:1 - 在列在 PATH 环境变量中的目录里查找 %1，
>                       并扩展到找到的第一个文件的驱动器号和路径。
>         %~ftza1     - 将 %1 扩展到类似 DIR 的输出行。
>    
>     在上面的例子中，%1 和 PATH 可以被其他有效数值替换。
>     %~ 语法被一个有效参数号码终止。%~ 修定符不能跟 %*使用

```
@echo off
chcp 65001
Echo 产生一个临时文件 > tmp.txt
Rem 下行先保存当前目录，再将 c:\windows设为当前目录
pushd c:\windows
Call :sub tmp.txt
Rem 下行恢复前次的当前目录
Popd
Call :sub tmp.txt
pause
Del tmp.txt
:sub
Echo 删除引号： %~1
Echo 扩充到路径： %~f1
Echo 扩充到一个驱动器号： %~d1
Echo 扩充到一个路径： %~p1
Echo 扩充到一个文件名： %~n1
Echo 扩充到一个文件扩展名： %~x1
Echo 扩充的路径指含有短名： %~s1
Echo 扩充到文件属性： %~a1
Echo 扩充到文件的日期/时间： %~t1
Echo 扩充到文件的大小： %~z1
Echo 扩展到驱动器号和路径：%~dp1
Echo 扩展到文件名和扩展名：%~nx1
Echo 扩展到类似 DIR 的输出行：%~ftza1
Echo.
Goto :eof
```



##### shift

> 更改批处理文件中可替换参数的位置。
>
> SHIFT [/n]
>
> 如果命令扩展被启用，SHIFT 命令支持/n 命令行开关；该命令行开关告诉
> 命令从第 n 个参数开始移位；n 介于零和八之间。例如:
>
>     SHIFT /2
>
> 会将 %3 移位到 %2，将 %4 移位到 %3，等等；并且不影响 %0 和 %1。

##### IF

> 执行批处理程序中的条件处理。
>
> IF [NOT] ERRORLEVEL number command
> IF [NOT] string1==string2 command
> IF [NOT] EXIST filen
>
> ame command
>
>   NOT               指定只有条件为 false 的情况下，Windows 才
>                     应该执行该命令。
>
>   ERRORLEVEL number 如果最后运行的程序返回一个等于或大于
>                     指定数字的退出代码，指定条件为 true。
>
>   string1==string2  如果指定的文字字符串匹配，指定条件为 true。
>
>   EXIST filename    如果指定的文件名存在，指定条件为 true。
>
>   command           如果符合条件，指定要执行的命令。如果指定的
>                     条件为 FALSE，命令后可跟 ELSE 命令，该命令将
>                     在 ELSE 关键字之后执行该命令。
>
> ELSE 子句必须出现在同一行上的 IF 之后。例如:
>
>     IF EXIST filename. (
>         del filename.
>     ) ELSE (
>         echo filename. missing.
>     )
>
> 由于 del 命令需要用新的一行终止，因此以下子句不会有效:
>
> IF EXIST filename. del filename. ELSE echo filename. missing
>
> 由于 ELSE 命令必须与 IF 命令的尾端在同一行上，以下子句也
> 不会有效:
>
>     IF EXIST filename. del filename.
>     ELSE echo filename. missing
> 如果都放在同一行上，以下子句有效:
>
>     IF EXIST filename. (del filename.) ELSE echo filename. missing
>
> 如果命令扩展被启用，IF 会如下改变:
>
>     IF [/I] string1 compare-op string2 command
>     IF CMDEXTVERSION number command
>     IF DEFINED variable command
>
> 其中， compare-op 可以是:
>
>     EQU - 等于
>     NEQ - 不等于
>     LSS - 小于
>     LEQ - 小于或等于
>     GTR - 大于
>     GEQ - 大于或等于
>
> 而 /I 开关(如果指定)说明要进行的字符串比较不分大小写。
> /I 开关可以用于 IF 的 string1==string2 的形式上。这些
> 比较都是通用的；原因是，如果 string1 和 string2 都是
> 由数字组成的，字符串会被转换成数字，进行数字比较。
>
> CMDEXTVERSION 条件的作用跟 ERRORLEVEL 的一样，除了它
> 是在跟与命令扩展有关联的内部版本号比较。第一个版本
> 是 1。每次对命令扩展有相当大的增强时，版本号会增加一个。
> 命令扩展被停用时，CMDEXTVERSION 条件不是真的。
>
> 如果已定义环境变量，DEFINED 条件的作用跟 EXIST 的一样，
> 除了它取得一个环境变量，返回的结果是 true。
>
> 如果没有名为 ERRORLEVEL 的环境变量，%ERRORLEVEL%
> 会扩充为 ERROLEVEL 当前数值的字符串表达式；否则，你会得到
> 其数值。运行程序后，以下语句说明 ERRORLEVEL 的用法:
>
>     goto answer%ERRORLEVEL%
>     :answer0
>     echo Program had return code 0
>     :answer1
>     echo Program had return code 1
>
> 你也可以使用以上的数字比较:
>
>     IF %ERRORLEVEL% LEQ 1 goto okay
>
> 如果没有名为 CMDCMDLINE 的环境变量，%CMDCMDLINE%
> 将在 CMD.EXE 进行任何处理前扩充为传递给 CMD.EXE 的原始
> 命令行；否则，你会得到其数值。
>
> 如果没有名为 CMDEXTVERSION 的环境变量，
> %CMDEXTVERSION% 会扩充为 CMDEXTVERSION 当前数值的
> 字串符表达式；否则，你会得到其数值。

##### setlocal 与 变量延迟

> 开始批处理文件中环境改动的本地化操作。在执行 SETLOCAL 之后
> 所做的环境改动只限于批处理文件。要还原原先的设置，必须执
> 行 ENDLOCAL。达到批处理文件结尾时，对于该批处理文件的每个
> 尚未执行的 SETLOCAL 命令，都会有一个隐含的 ENDLOCAL 被执行。
>
> SETLOCAL
>
> 如果启用命令扩展，则 SETLOCAL 更改如下:
>
> SETLOCAL 批命令现在可以接受可选参数:
>         ENABLEEXTENSIONS / DISABLEEXTENSIONS
>             启用或禁用命令处理器扩展。这些
>             参数比 CMD /E:ON 或 /E:OFF
>             开关有优先权。请参阅 CMD /? 获取详细信息。
>         ENABLEDELAYEDEXPANSION / DISABLEDELAYEDEXPANSION
>             启用或禁用延缓环境变量
>             扩展。这些参数比 CMD
>             /V:ON 或 /V:OFF 开关有优先权。请参阅 CMD /? 获取详细信息。
> 无论在 SETLOCAL 命令之前的设置是什么，这些修改会一直
> 生效，直到出现相应的 ENDLOCAL 命令。
>
> 在给定参数的情况下，
> SETLOCAL 命令将设置 ERRORLEVEL 值。如果给定两个有效参数中的一个，另一个未给定，
> 则该值为零。
> 通过以下方法，你可以在批脚本中
> 使用此项来确定扩展是否可用:
>
>     VERIFY OTHER 2>nul
>     SETLOCAL ENABLEEXTENSIONS
>     IF ERRORLEVEL 1 echo Unable to enable extensions
>
> 此方法之所以有效，是因为在 CMD.EXE 的旧版本上，SETLOCAL
> 不设置 ERRORLEVEL 值。如果参数不正确，VERIFY 命令会将
> ERRORLEVEL 值初始化为非零值。



```
set a=4
set a=5 & echo %a%
pause
:: 显示4

setlocal enabledelayedexpansion
set a=4
set a=5 & echo !a!
::显示5
pause

for /l %%i in (1,1,5) do (
set a=%%i
echo !a!
)
:: 显示 1 2 3 4 5 
```



#### 0x0101 常用特殊符号

##### 1、@ 命令行回显屏蔽符

##### 2、% 批处理变量引导符

> 引用变量用%var%，调用程序外部参数用%1 至%9 等等
> %0 %1 %2 %3 %4 %5 %6 %7 %8 %9 %*为命令行传递给批处理的参数
> %0 批处理文件本身，包括完整的路径和扩展名
>
> ```
> 例：最简单的复制文件自身的方法
> copy %0 d:\wind.bat
> ```
>
> 

##### 3、> 重定向符

> 这个字符的意思是传递并且覆盖，他所起的作用是将运行的结果传递到后面的范围（后边可
> 以是文件，也可以是默认的系统控制台）
> 在 NT 系列命令行中，重定向的作用范围由整个命令行转变为单个命令语句，受到了命
> 令分隔符&,&&,||和语句块的制约限制。
> 比如：
> 使用命令：echo hello >1.txt 将建立文件 1.txt，内容为”hello “（注意行尾有一空格）
> 使用命令：echo hello>1.txt 将建立文件 1.txt，内容为”hello“（注意行尾没有空格）

##### 4、>> 重定向符 追加

```
echo hello > 1.txt
echo world >>1.txt

这时候 1.txt 内容如下 :
hello
world
```



##### 5、<、>&、<& 重定向符   了解

> <，输入重定向命令，从文件中读入命令输入，而不是从键盘中读入。
>
> &，将一个句柄的输出写入到另一个句柄的输入中。
>
> <&，刚好和>&相反，从一个句柄读取输入并将其写入到另一个句柄输出中。
>
> 常用句柄：0、1、2，未定义句柄：3—9
> 1>nul 表示禁止输出正确的信息
> 2>nul 表示禁止输出错误信息。



##### 6、| 命令管道符

> 格式：第一条命令 | 第二条命令 [| 第三条命令...]
> 将第一条命令的结果作为第二条命令的参数来使用，记得在 unix 中这种方式很常见

```
dir c:\|find "txt"
dir c:\|findstr "txt"
::必须双引号

echo y|format a: /s /q /v:system
::格式化
```



##### 7、 ^ 转义字符

> ^ 是对特殊符号<,>,&的前导字符，在命令中他将以上 3 个符号的特殊功能去掉，仅仅只把他
> 们当成符号而不使用他们的特殊意义。

```
echo test ^>1.txt
结果则是：test > 1.txt
```

> 另外，此转义字符还可以用作续行符号。

```
@echo off
echo 英雄 ^
是 ^
好 ^
男人
pause
```



##### 8、& 组合命令

> 语法：第一条命令 & 第二条命令 [& 第三条命令...]
> &、&&、||为组合命令，顾名思义，就是可以把多个命令组合起来当一个命令来执行。这在
> 批处理脚本里是允许的，而且用的非常广泛
>
> **允许在一行中使用 2 个以上不同的命令，当第一个命令执行失败了，也不影响后边**
> **的命令执行。**

```
dir z:\ & dir y:\ & dir c:\
```



##### 9、&& 组合命令

> 语法：第一条命令 && 第二条命令 [&& 第三条命令...]
>
> 可以同时执行多条命令，当碰到执行**出错**的命令后**将不执行后面**的命令，如果一
> 直没有出错则一直执行完所有命令

```
dir z:\ && dir y:\ && dir c:\
```



##### 10、|| 组合命令

> 语法：第一条命令 || 第二条命令 [|| 第三条命令...]
> 可以同时执行多条命令，**当一条命令失败后才执行第二条命令**，当碰到执行正确的命令后将不执行后面的命令，如果没有出现正确的命令则一直执行完所有命令；
>
> 注意: 管道命令的优先级高于重定向命令





##### 11、"" 字符串界定符

> 双引号允许在字符串中包含空格，进入一个特殊目录可以用如下方法

```
cd "program files"
cd progra~1
cd pro
```



##### 12、, 逗号

> 逗号相当于空格，在某些情况下“,”可以用来当做空格使

```
dir,c:\
```



##### 13、; 分号

> 当命令相同时，可以将不同目标用；来隔离，但执行效果不变，如执行过程中发生错
> 误，则只返回错误报告，但程序仍会执行。

```
dir c:\;d:\;e:\;z:\
:: 找不到路径报错
dir c:\;d:\;e:\1.txt
::e盘存在，有错误提示，但命令仍会执行
```



##### 14、() 括号

> 小括号在批处理编程中有特殊的作用，左右括号必须成对使用，括号中可以包括多行命令 ，
> 这些命令将被看成一个整体，视为一条命令行。
> 括号在 for语句和 if 语句中常见，用来嵌套使用循环或条件语句，其实括号()也可以单独使用

```
(
echo 1
echo 2
echo 3
)
注意：这种多条命令被视为一条命令行时，如果其中有变量，就涉及到变量延迟的问题。
```



##### 15、! 感叹号

> 在变量延迟问题中，用来表示变量，即%var%应该表示为!var!

### 0x02 FOR 命令详解

> 对一组文件中的每一个文件执行某个特定命令。
>
> FOR %variable IN (set) DO command [command-parameters]
>
>   %variable  指定一个单一字母可替换的参数。
>   (set)      指定一个或一组文件。可以使用通配符。
>   command    指定对每个文件执行的命令。
>   command-parameters
>              为特定命令指定参数或命令行开关。
>
> 在批处理程序中使用 FOR 命令时，指定变量请使用 %%variable
> 而不要用 %variable。变量名称是区分大小写的，所以 %i 不同于 %I.
>
> 如果启用命令扩展，则会支持下列 FOR 命令的其他格式:
>
> FOR /D %variable IN (set) DO command [command-parameters]
>
>     如果集中包含通配符，则指定与目录名匹配，而不与文件名匹配。
>
> FOR /R [[drive:]path] %variable IN (set) DO command [command-parameters]
>
>     检查以 [drive:]path 为根的目录树，指向每个目录中的 FOR 语句。
>     如果在 /R 后没有指定目录规范，则使用当前目录。如果集仅为一个单点(.)字符，
>     则枚举该目录树。
>
> FOR /L %variable IN (start,step,end) DO command [command-parameters]
>
>     该集表示以增量形式从开始到结束的一个数字序列。因此，(1,1,5)将产生序列
>     1 2 3 4 5，(5,-1,1)将产生序列(5 4 3 2 1)
>
> FOR /F ["options"] %variable IN (file-set) DO command [command-parameters]
> FOR /F ["options"] %variable IN ("string") DO command [command-parameters]
> FOR /F ["options"] %variable IN ('command') DO command [command-parameters]
>
>     或者，如果有 usebackq 选项:
>
> FOR /F ["options"] %variable IN (file-set) DO command [command-parameters]
> FOR /F ["options"] %variable IN ("string") DO command [command-parameters]
> FOR /F ["options"] %variable IN ('command') DO command [command-parameters]
>
>     fileset 为一个或多个文件名。继续到 fileset 中的下一个文件之前，
>     每份文件都被打开、读取并经过处理。处理包括读取文件，将其分成一行行的文字，
>     然后将每行解析成零或更多的符号。然后用已找到的符号字符串变量值调用 For 循环。
>     以默认方式，/F 通过每个文件的每一行中分开的第一个空白符号。跳过空白行。
>     你可通过指定可选 "options" 参数替代默认解析操作。这个带引号的字符串包括一个
>     或多个指定不同解析选项的关键字。这些关键字为:
>    
>         eol=c           - 指一个行注释字符的结尾(就一个)
>         skip=n          - 指在文件开始时忽略的行数。
>         delims=xxx      - 指分隔符集。这个替换了空格和制表符的
>                           默认分隔符集。
>         tokens=x,y,m-n  - 指每行的哪一个符号被传递到每个迭代
>                           的 for 本身。这会导致额外变量名称的分配。m-n
>                           格式为一个范围。通过 nth 符号指定 mth。如果
>                           符号字符串中的最后一个字符星号，
>                           那么额外的变量将在最后一个符号解析之后
>                           分配并接受行的保留文本。
>         usebackq        - 指定新语法已在下类情况中使用:
>                           在作为命令执行一个后引号的字符串并且一个单
>                           引号字符为文字字符串命令并允许在 file-set
>                           中使用双引号扩起文件名称。
>    
>     某些范例可能有助:
> FOR /F "eol=; tokens=2,3* delims=, " %i in (myfile.txt) do @echo %i %j %k
>
>     会分析 myfile.txt 中的每一行，忽略以分号打头的那些行，将
>     每行中的第二个和第三个符号传递给 for 函数体，用逗号和/或
>     空格分隔符号。请注意，此 for 函数体的语句引用 %i 来
>     获得第二个符号，引用 %j 来获得第三个符号，引用 %k
>     来获得第三个符号后的所有剩余符号。对于带有空格的文件
>     名，你需要用双引号将文件名括起来。为了用这种方式来使
>     用双引号，还需要使用 usebackq 选项，否则，双引号会
>     被理解成是用作定义某个要分析的字符串的。
>    
>     %i 在 for 语句中显式声明，%j 和 %k 是通过
>     tokens= 选项隐式声明的。可以通过 tokens= 一行
>     指定最多 26 个符号，只要不试图声明一个高于字母 "z" 或
>     "Z" 的变量。请记住，FOR 变量是单一字母、分大小写和全局的变量；
>     而且，不能同时使用超过 52 个。
>    
>     还可以在相邻字符串上使用 FOR /F 分析逻辑，方法是，
>     用单引号将括号之间的 file-set 括起来。这样，该字符
>     串会被当作一个文件中的一个单一输入行进行解析。
>    
>     最后，可以用 FOR /F 命令来分析命令的输出。方法是，将
>     括号之间的 file-set 变成一个反括字符串。该字符串会
>     被当作命令行，传递到一个子 CMD.EXE，其输出会被捕获到
>     内存中，并被当作文件分析。如以下例子所示:
>    
>       FOR /F "usebackq delims==" %i IN (`set`) DO @echo %i
>    
>     会枚举当前环境中的环境变量名称。
>
> 另外，FOR 变量参照的替换已被增强。你现在可以使用下列
> 选项语法:
>
>      %~I          - 删除任何引号(")，扩展 %I
>      %~fI        - 将 %I 扩展到一个完全合格的路径名
>      %~dI        - 仅将 %I 扩展到一个驱动器号
>      %~pI        - 仅将 %I 扩展到一个路径
>      %~nI        - 仅将 %I 扩展到一个文件名
>      %~xI        - 仅将 %I 扩展到一个文件扩展名
>      %~sI        - 扩展的路径只含有短名
>      %~aI        - 将 %I 扩展到文件的文件属性
>      %~tI        - 将 %I 扩展到文件的日期/时间
>      %~zI        - 将 %I 扩展到文件的大小
>      %~$PATH:I   - 查找列在路径环境变量的目录，并将 %I 扩展
>                    到找到的第一个完全合格的名称。如果环境变量名
>                    未被定义，或者没有找到文件，此组合键会扩展到
>                    空字符串
>
> 可以组合修饰符来得到多重结果:
>
>      %~dpI       - 仅将 %I 扩展到一个驱动器号和路径
>      %~nxI       - 仅将 %I 扩展到一个文件名和扩展名
>      %~fsI       - 仅将 %I 扩展到一个带有短名的完整路径名
>      %~dp$PATH:I - 搜索列在路径环境变量的目录，并将 %I 扩展
>                    到找到的第一个驱动器号和路径。
>      %~ftzaI     - 将 %I 扩展到类似输出线路的 DIR
>
> 在以上例子中，%I 和 PATH 可用其他有效数值代替。%~ 语法
> 用一个有效的 FOR 变量名终止。选取类似 %I 的大写变量名
> 比较易读，而且避免与不分大小写的组合键混淆。

##### 一、参数 /d

> FOR /D %variable IN (set) DO command [command-parameters]
> 如果集中包含通配符，则指定与目录名匹配，而不与文件名匹配。

```
@echo off
for /d %%i in (c:\*) do echo %%i
:: bat 或cmd 文件 %%i , 命令行 %i
pause

for /d %%i in (???) do echo %%i
pause
::通配符 ? 一个字符 * 任意个字符
```



##### 二、参数 /R

> FOR /R [[drive:]path] %variable IN (set) DO command [command-parameters]
> 检查以 [drive:]path 为根的目录树，指向每个目录中的FOR 语句。如果在 /R 后没有指定目录，则使用当前
> 目录。如果集仅为一个单点(.)字符，则枚举该目录树。

```
@echo off
for /r c:\ %%i in (*.exe) do echo %%i
pause
for /r c:\ %%i in (boot.ini) do echo %%i
::枚举了 c 盘所有目录,改为以下
pause

for /r c:\ %%i in (py.exe) do if exist %%i echo %%i
pause

```



##### 三、参数 /L

> FOR /L%variable IN (start,step,end) DO command [command-parameters]
>
> 该集表示以增量形式从开始到结束的一个数字序列。因此，(1,1,5) 将产生序列 1 2 3 4 5，(5,-1,1) 将产生序列 (5 4 3 2 1)

```
@echo off
for /l %%i in (1,1,5) do @echo %%i
pause
```



##### 四、参数 /F

> FOR /F ["options"] %variable IN (file-set) DO command [command-parameters]
> FOR /F ["options"] %variable IN ("string") DO command [command-parameters]
> FOR /F ["options"] %variable IN ('command') DO command [command-parameters
>
> 带引号的字符串"options"包括一个或多个指定不同解析选项的关键字。这些关键字为 :
> 		eol=c - 指一个行注释字符的结尾(就一个)
> 		skip=n - 指在文件开始时忽略的行数。
> 		delims=xxx - 指分隔符集。这个替换了空格和跳格键的默认分隔符集。
> 		tokens=x,y,m-n - 指每行的哪一个符号被传递到每个迭代的 for 本身。这会导致额外变量名称的分配。	    
>
> ​                               m-n格式为一个范围。通过 nth 符号指定 mth。如果符号字符串中的最后一个字符星号， 								那么额外的变量将在最后一个符号解析之后分配并接受行的保留文本。
> ​		usebackq - 使用后引号（键盘上数字 1 左面的那个键 ` ）未使用参数 usebackq 时：file-set 表示文  			` 
>
> ​                          `件，但不能含有空格
> 双引号表示字符串，即"string"
> 单引号表示执行命令，即'command'
> 使用参数 usebackq 时：file-set 和"file-set"都表示文件
> 当文件路径或名称中有空格时，就可以用双引号括起来
> 单引号表示字符串，即'string'
> 后引号表示命令执行，即`command`

```
@echo off
rem 首先建立临时文件 test.txt
echo ;注释行,这是临时文件,用完删除 >test.txt
echo 11 段 12 段 13 段 14 段 15 段 16 段 >>test.txt
echo 21 段,22 段,23 段,24 段,25 段,26 段 >>test.txt
echo 31 段-32 段-33 段-34 段-35 段-36 段 >>test.txt
FOR /F "eol=; tokens=1,3* delims=,- " %%i in (test.txt)do echo %%i %%j %%k
Pause
Del test.txt
```



### 0x03 FOR 命令中的变量

##### 一、 ~I - 删除任何引号(")，扩展 %I

```
@echo off
echo ^"1111>temp.txt
echo "2222">>temp.txt
echo 3333^">>temp.txt
echo "4444"44>>temp.txt
echo ^"55"55"55>>temp.txt
rem 上面建立临时文件，注意不成对的引号要加转义字符 ^ ，重定向符号前不要留空格
FOR /F "delims=" %%i IN (temp.txt) DO echo %%~i
pause
del temp.txt

输出：
1111
2222
3333"
4444"44
55"55"55

```

删除引号规则如下(BAT 兄补充!)
1、若字符串首尾同时存在引号，则删除首尾的引号；
2、若字符串尾不存在引号，则删除字符串首的引号；
3、如果字符串中间存在引号，或者只在尾部存在引号，则不删除。



##### 二、 %~fI - 将 %I 扩展到一个完全合格的路径名

```
@echo off
FOR /F "delims==" %%i IN ('dir /b') DO @echo %%~fi
pause
```



##### 三、 %~dI - 仅将 %I 扩展到一个驱动器号

```
@echo off
FOR /F "delims==" %%i IN ('dir /b') DO @echo %%~di
pause
:: 仅输入磁盘目录 如 C:
```



##### 四、 %~pI - 仅将 %I 扩展到一个路径

```
@echo off
FOR /F "delims==" %%i IN ('dir /b') DO @echo %%~pi
pause

：路径  \Users\Leon\Desktop\
```



##### 五、 %~nI - 仅将 %I 扩展到一个文件名

```
@echo off
FOR /F "delims==" %%i IN ('dir /b') DO @echo %%~ni
pause

：文件名无扩展后缀
```



##### 六、 %~xI - 仅将 %I 扩展到一个文件扩展名

​       只打印文件的扩展名

##### 七、 %~sI - 扩展的路径只含有短名

​		打印绝对短文件名

##### 八、 %~aI - 将 %I 扩展到文件的文件属性

​		打印文件的属性

##### 九、 %~tI - 将 %I 扩展到文件的日期/时间

​		打印文件建立的日期

##### 十、 %~zI - 将 %I 扩展到文件的大小

​		打印文件的大小

##### 十一、 %~$PATH:I

在 PATH 变量里指定的路径里搜索 notepad.exe 文件，如果有 notepad.exe 则会把
他所在绝对路径打印出来，没有就打印一个错误！

```
@echo off
FOR /F "delims=" %%i IN (“notepad.exe”) DO echo %%~$PATH:i
pause
```



### 0x04 批处理中的变量

##### 一、系统变量  不需要赋值,调用而以

```
%ALLUSERSPROFILE% 本地 返回“所有用户”配置文件的位置。
%APPDATA% 本地 返回默认情况下应用程序存储数据的位置。
%CD% 本地 返回当前目录字符串。
%CMDCMDLINE% 本地 返回用来启动当前的 Cmd.exe 的准确命令行。
%CMDEXTVERSION% 系统 返回当前的“命令处理程序扩展”的版本号。
%COMPUTERNAME% 系统 返回计算机的名称。
%COMSPEC% 系统 返回命令行解释器可执行程序的准确路径。
%DATE% 系统 返回当前日期。
%ERRORLEVEL% 系统 返回上一条命令的错误代码。通常用非零值表示错误。
%HOMEDRIVE% 系统 返回连接到用户主目录的本地工作站驱动器号。
%HOMEPATH% 系统 返回用户主目录的完整路径。
%LOGONSERVER% 
%NUMBER_OF_PROCESSORS%
%OS%
%PATH%
%PATHEXT% 系统 返回操作系统认为可执行的文件扩展名的列表。
%PROCESSOR_ARCHITECTURE% 系统 返回处理器的芯片体系结构。
%PROCESSOR_IDENTFIER% 系统 返回处理器说明。
%PROCESSOR_LEVEL% 系统 返回计算机上安装的处理器的型号。
%PROCESSOR_REVISION% 系统 返回处理器的版本号。
%PROMPT% 本地 返回当前解释程序的命令提示符设置。由 Cmd.exe 生成。
%RANDOM% 系统 返回 0 到 32767 之间的任意十进制数字。由 Cmd.exe 生成。
%SYSTEMDRIVE% 系统 返回包含 Windows server operating system 根目录（即系统根目录）
%SYSTEMROOT% 系统 返回 Windows server operating system 根目录的位置。
%TEMP% 和 %TMP% 系统和用户 返回对当前登录用户可用的应用程序所使用的默认临时目录。
%TIME% 系统 返回当前时间。
%USERDOMAIN% 本地 返回包含用户帐户的域的名称。
%USERNAME% 本地 返回当前登录的用户的名称。
%USERPROFILE% 本地 返回当前用户的配置文件的位置。
%WINDIR% 系统 返回操作系统目录的位置。


```

%1 %2 %3 %4 %5第几个参数  

```
@echo off
echo %1 %2 %3 %4
echo %1
echo %2
echo %3
echo %4
：：保存为test.BAT  进入 CMD 
   输入  test.bat 我是第一个参数 我是第二个参数 我是第三个参数 我是第四个参数
```

%* 显示全部参数

%0 返回批处理所在绝对路径  或者无限循环执行 BAT

```
@echo off
echo %0
pause

@echo off
net user
%0
```



##### 二、自定义变量  由我们来给他赋予值的变量

```
@echo off
set var=我是值
echo %var%
pause
```

让用户手工输入变量的值

```
@echo off
set /p var=请输入变量的值
echo %var%
pause
```



### 0x05 set 命令详解

##### 一、用 set 命令设置自定义变量

> 显示、设置或删除 cmd.exe 环境变量。
>
> SET [variable=[string]]
>
>   variable  指定环境变量名。
>   string    指定要指派给变量的一系列字符串。
>
> 要显示当前环境变量，键入不带参数的 SET。
>
> 可仅用一个变量激活 SET 命令，等号或值不显示所有前缀匹配
> SET 命令已使用的名称的所有变量的值。例如:
>
>     SET P
>
> 会显示所有以字母 P 打头的变量
>
> 在 SET 命令中添加了两个新命令行开关:
>
>     SET /A expression
>     SET /P variable=[promptString]	

##### 二、用 set 命令进行简单计算

> /A 命令行开关指定等号右边的字符串为被评估的数字表达式。该表达式
> 评估器很简单并以递减的优先权顺序支持下列操作:
>
>     ()                  - 分组
>     ! ~ -               - 一元运算符
>     * / %               - 算数运算符
>     + -                 - 算数运算符
>     << >>               - 逻辑移位
>     &                   - 按位“与”
>     ^                   - 按位“异”
>     |                   - 按位“或”
>     = *= /= %= += -=    - 赋值
>       &= ^= |= <<= >>=
>     ,                   - 表达式分隔符
>     注意：DOS 计算只能精确到整数



```
@echo off
set /p input=请输入计算表达式：
set /a var=%input%
echo 计算结果：%input%=%var%
pause
```

```
@echo off
set /p n=请输入 2 的几次方 :
set /a num=1^<^<n
echo %num%
pause
```



##### 三、用 set 命令进行字符串处理

###### 1、字符串替换

> %PATH:str1=str2%  字符串变量%PATH%中的 str1 替换为 str2

```
@echo off
set a= bbs.verybat. cn
echo 替换前的值 : "%a%"
set var=%a: =%
echo 替换后的值 : "%var%"
pause
::输出bbs.verybat.cn
```



###### 2、字符串截取

> 截取功能统一语法格式为：%a:~[m[,n]]%
>
> 方括号表示可选，%为变量标识符，a 为变量名，不可少，冒号用于分隔变量名和说明部分 ，
> 符号～可以简单理解为“偏移”即可，m 为偏移量（缺省为 0），n 为截取长度（缺省为全部）

跳过一个，取两个

```
@echo off
set a=bbs.verybat.cn
set var=%a:~1,2%
echo %var%
pause
```

取最后3个

```
@echo off
set a=bbs.verybat.cn
set var=%a:~3%
echo %
```

从第几个开始， 不要最后几个

```
@echo off
set a=bbs.verybat.cn
set var=%a:~2,-3%
echo %var%
pause
```

求字符串单长度

```
@echo off
set /p str=请输入任意长度的字符串 :
echo 你输入了字符串:"%str%"
if not defined str(pause& goto :eof)
set num=0
:len
set /a num+=1
set str=%str:~0,-1%
if defined str goto len
echo 字符串长度为：%num%
pause
```



### 0x06 if 命令讲解

##### 第一种用法：IF [NOT] ERRORLEVEL number command

```
@echo off
net user
IF %ERRORLEVEL% == 0 echo net user 执行成功了!
pause
```

##### 第二种用法：IF [NOT] string1==string2 command

```
@echo off
set /p var=请输入第一个比较字符 :
set /p var2=请输入第二个比较字符 :
if %var% == %var2% (echo 我们相等) ELSE echo 我们不相等
pause

```

  上面这个例子 判断你输入的值是不是相等,但是你如果输入相同的字符,但是如果其中一个后面打了一个空格,
   这个例子还是会认为相等,如何让有空格的输入不相等呢?我们在比较字符上加个双引号

```
@echo off
set /p var=请输入第一个比较字符 :
set /p var2=请输入第二个比较字符(多输入个空格试试 ):
if "%var%" == "%var2%" (echo 我们相等) ELSE echo 我们不相等
pause
```



##### 第三种用法：IF [NOT] EXISTfilename command  判断某个文件或者文件夹是否存在

```
@echo off
if exist "c:\test" (echo 存在文件) ELSE echo 不存在文件
pause
```



##### 第四种用法：IF 增强的用法

> IF [/I]string1 compare-op string2 command
> IF CMDEXTVERSION number command
> IF DEFINED variable command

后面两个用法,跟前面的类似,不做介绍,



```
@echo off
if a == A (echo 我们相等) ELSE echo 我们不相等
pause
```

不相等

```
@echo off
if /i a == A (echo 我们相等) ELSE echo 我们不相等
pause
```

加上/I 不区分大小 结果 相等

**判断数字的符号**

> EQU - 等于
> NEQ - 不等于
> LSS - 小于
> LEQ - 小于或等于
> GTR- 大于
> GEQ - 大于或等于

```
@echo off
set /p var=请输入一个数字 :
if %var% LEQ 4 (echo 我小于等于 4) ELSE echo 我不小于等于 4
pause
```



### 0x07 DOS 编程高级技巧

##### 一、界面设计

```
@echo off
cls
title 终极多功能修复
:menu
cls
color 0A
echo.
echo ==============================
echo 请选择要进行的操作，然后按回车
echo ==============================
echo.
echo 1.网络修复及上网相关设置,修复 IE,自定义屏蔽网站
echo.
echo 2.病毒专杀工具，端口关闭工具,关闭自动播放
echo.
echo 3.清除所有多余的自启动项目，修复系统错误
echo.
echo 4.清理系统垃圾,提高启动速度
echo.
echo Q.退出
echo.
echo.
:cho
set choice=
set /p choice= 请选择 :
IF NOT "%choice%"=="" SET choice=%choice:~0,1%
if /i "%choice%"=="1" goto ip
if /i "%choice%"=="2" goto setsave
if /i "%choice%"=="3" goto kaiji
if /i "%choice%"=="4" goto clean
if /i "%choice%"=="Q" goto endd
echo 选择无效，请重新输入
echo.
goto cho
:ip
echo ip
pause
goto :menu
:setsave
echo setsave
pause
goto :menu
:kaiji
echo kaiji
pause
goto :menu
:clean
echo clean
pause
goto :menu
:endd
exit
```

##### 二、 if …else…条件语句

```
IF EXIST filename (
del filename
) ELSE (
echo filename missing
)
```

##### 三、循环语句

> FOR /L%variable IN (start,step,end) DO command [command-parameters]
>
> FOR %%variable IN (set) DO command [command-parameters]
>
> FOR /R[[drive:]path] %variable IN (set) DO command [command-parameters]

```
@echo off
set var=0
rem ************循环开始了
:continue
set /a var+=1
echo 第%var%此循环
if %var% lss 100 goto continue
rem ************循环结束了
echo 循环执行完毕
pause
```



##### 四、子程序

> CALL:label arguments
> :label
> command1
> command2
> ...
> commandn
> goto :eof

```
@echo off
call :sub return 你好
echo 子程序返回值：%return%
pause
:sub
set %1=%2
goto :eof
运行结果：你好
```

设计一个求多个整数相加的子程序

```
@echo off
set sum=0
call :sub sum 10 20 35
echo 数据求和结果：%sum%
pause
:sub
rem 参数 1 为返回变量名称
set /a %1=%1+%2
shift /2
if not "%2"=="" goto sub
goto :eof
```



##### 五、用 ftp 命令实现自动下载

```
ftp   #进入ftp
open 90.52.8.3 ＃打开 ip
user iware ＃用户为 iware
password8848 ＃密码
bin ＃二进制传输模式
prompt
cd tmp1 ＃切换至 iware 用户下的 tmp1 目录
pwd
lcd d:\download ＃本地目录
mget * ＃下载 tmp1 目录下的所有文件
bye ＃退出 ftp
```



##### 六、调用 VBScript 程序

> 用法：CScript scriptname.extension [option...] [arguments...]
>
> 选项：
>  //B         批模式：不显示脚本错误及提示信息
>  //D         启用 Active Debugging
>  //E:engine  使用执行脚本的引擎
>  //H:CScript 将默认的脚本宿主改为 CScript.exe
>  //H:WScript 将默认的脚本宿主改为 WScript.exe （默认）
>  //I         交互模式（默认，与 //B 相对)
>  //Job:xxxx  执行一个 WSF 工作
>  //Logo      显示徽标（默认）
>  //Nologo    不显示徽标：执行时不显示标志
>  //S         为该用户保存当前命令行选项
>  //T:nn      超时设定秒：允许脚本运行的最长时间
>  //X         在调试器中执行脚本
>  //U         用 Unicode 表示来自控制台的重定向 I/O

##### 七、将批处理转化为可执行文件 BAT2EXE第三方工具

> 在 DOS 环境下，可执行文件的优先级由高到低依次为.com>.exe>.bat>.cmd

##### 八、时间延迟

```
@echo off
chcp 65001
echo 延时前！%time%
ping /n 5 127.0.0.1 >nul
echo 延时后！%time%
pause
echo 延时前！%time%
for /l %%i in (1,1,5000) do echo %%i>nul
echo 延时后！%time%
pause
```



##### 九、模拟进度条

```
@echo off
mode con cols=113 lines=15 &color 9f
chcp 65001
cls
echo.
echo 程序正在初始化. . .
echo.
echo ┌──────────────────────────────────────┐
set /p a=■<nul
for /L %%i in (1 1 38) do set /p a=■<nul&ping /n 1 127.0.0.1>nul
echo 100%%
echo └──────────────────────────────────────┘
pause
```

