## AspectJ in Android



### what is AspectJ 

> - a seamless aspect-oriented extension to the Java programming language
> - Java platform compatible
> - easy to learn and use



### Where AspectJ uses

> clean modularization of crosscutting concerns, such as error checking and handling, synchronization, context-sensitive behavior, performance optimizations, monitoring and logging, debugging support, and multi-object protocols



更多AspectJ信息,请参考官网

[AspectJ官网](https://www.eclipse.org/aspectj/)

[官网编程指南](https://eclipse.org/aspectj/doc/released/progguide/index.html)

### Android中 如何使用

- gradle 配置

  project build.gradle

  ```
  dependencies {
  	classpath 'com.hujiang.aspectjx:gradle-android-plugin-aspectjx:2.0.4'
  }
  # 此版本支持kotlin,org.aspectj目前不支持
  ```

  module build.gradle

  ```
  apply plugin: 'android-aspectjx'
  #设置扫描或者忽略的包,文件,以加快编译速度
  aspectjx {
      include 'me.leon.aop' 
      //指定只对含有关键字'universal-image-loader', 'AspectJX-Demo/library'的库进行织入扫描，忽略其他库，提升编译效率
  //    includeJarFilter 'universal-image-loader', 'AspectJX-Demo/library'
  //    excludeJarFilter '.jar'
  //    ajcArgs '-Xlint:warning'
  }
  
  dependencies {
   implementation 'org.aspectj:aspectjrt:1.8.13'
  }
  ```

  

- 项目使用

  ```
  
  @Aspect
  public class LeonAop {
  
     // 设置切点, 该方法就是 aspectj 的id, 即debugLog()
      @Pointcut("execution(* me.leon.aop.*.aop*(..))")
      public void debugLog() {
      }
  
     // 对id debugLog() 进行环绕织入
      @Around("debugLog()")
      public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
          long startNanos = System.nanoTime();
          //执行原方法
          Object result = joinPoint.proceed();
          long stopNanos = System.nanoTime();
          long lengthMillis = TimeUnit.NANOSECONDS.toMillis(stopNanos - startNanos);
          Log.d("AOP", joinPoint.getSignature().getName() + lengthMillis);
          return result;
      }
  }
  ```

  aspect 写好之后, 只要在me.leon.aop 包下, 方法是aop开头的,都会打印日志.

  如果需要获取每个方法的配置参数,那就需要加入注解.

  ### 注解 + AOP 

  ```
  @Retention(RetentionPolicy.RUNTIME)
  @Target(ElementType.METHOD)
  public @interface SingleClick {
      int value() default 3000;
  }
  ```

  在aspect 类中 加入以下代码

  ```
    @Pointcut("execution(@me.leon.aop.SingleClick * *(..))")
      public void clickFilter() {
     }
     
      @Around("clickFilter()")
      public void injectClick(ProceedingJoinPoint joinPoint) throws Throwable {
  // 取出方法的参数
          View view = null;
          for (Object arg : joinPoint.getArgs()) {
              if (arg instanceof View) {
                  view = (View) arg;
                  break;
              }
          }
          if (view == null) {
              return;
          }
  
          MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
          Method method = methodSignature.getMethod();
          if (!method.isAnnotationPresent(SingleClick.class)) {
              return;
          }
          SingleClick singleClick = method.getAnnotation(SingleClick.class);
          // 判断是否快速点击
          if (!isFastDoubleClick(view, singleClick.value())) {
              // 不是快速点击，执行原方法
              joinPoint.proceed();
          }else {
              Log.d("AOP", "快速 click 过滤 : " + joinPoint.getSignature().getName());
              return;
          }
          Log.d("AOP", "click : " + joinPoint.getSignature().getName());
      }
       public boolean isFastDoubleClick(View v, long intervalMillis) {
          int viewId = v.getId();
          long time = System.currentTimeMillis();
          long timeInterval = Math.abs(time - mLastClickTime);
          if (timeInterval < intervalMillis && viewId == mLastClickViewId) {
              return true;
          } else {
              mLastClickTime = time;
              mLastClickViewId = viewId;
              return false;
          }
      }
  ```



### Kotlin 实现

**注解**

```
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION,AnnotationTarget.PROPERTY_GETTER,AnnotationTarget.PROPERTY_GETTER)
annotation class SingleClickKotlin(val value: Int = 3000)
```

**aspect**

```
@Aspect
class LeonAop2 {
    @Pointcut("execution(@me.leon.aop.SingleClickKotlin * *(..))")
    fun click(){}

    @Around("click()")
    fun clickHandle(joinPoint: ProceedingJoinPoint){
        var view: View? = null
        for (arg in joinPoint.args) {
            if (arg is View) {
                view = arg
                break
            }
        }
        if (view == null) {
            return
        }

        val methodSignature = joinPoint.signature as MethodSignature
        val method = methodSignature.method
        if (!method.isAnnotationPresent(SingleClickKotlin::class.java)) {
            return
        }
        val singleClick = method.getAnnotation(SingleClickKotlin::class.java)
        // 判断是否快速点击
        if (!isFastDoubleClick(view, singleClick.value.toLong())) {
            // 不是快速点击，执行原方法
            joinPoint.proceed()
        } else {
            Log.d("AOP Kt", "快速 click 过滤 : " + joinPoint.getSignature().getName())
            return
        }
        Log.d("AOP Kt", "click : " + joinPoint.getSignature().getName())

    }
}
```

