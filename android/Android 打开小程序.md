## 小程序原理

参考   [微信小程序底层实现原理](https://www.jianshu.com/p/339d76342c87)

其实跟我们的一体机类似,都是基于H5开发, APP 负责提供功能接口给H5调用

不同的是微信的用的QQ浏览器的x5内核, 提供功能的是微信APP

**综上,小程序的运行依赖微信本体,无法在第三方APP里运行小程序,但可以通过微信开放的接口拉起小程序(微信里打开小程序)**



## 微信开放平台支持

参考官方  [移动应用拉起小程序功能]( https://developers.weixin.qq.com/doc/oplatform/Mobile_App/Launching_a_Mini_Program/Launching_a_Mini_Program.html )  

官方提供通第三方 APP拉起小程序的接口, 但是必须用跟小程序同一个的微信开发者id

