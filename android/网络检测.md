## 网络检测

监测网络连通情况是日常项目维护时期常遇到的，其中我们常用到的有ping、telnet、tracert (Linux traceroute)，三个命令总结一下。

### 1.Ping

#### 	1.1介绍

​        ping称为因特网包探索器，用于测试网络连接量的程序，通常是用来检查**网络是否通畅或者网络连接速度的命令**，评估网络质量，ping域名还可以得出解析IP。

####   1.2 原理

​       ping命令工作在OSI参考模型的第三层-网络层。

​       利用网络上机器IP地址的唯一性，给目标IP地址发送一个数据包，再要求对方返回一个同样大小的数据包来确定两台网络机器是否连接相通，时延是多少。这个测试命令可以帮助网络管理者测试到达目的主机的网络是否连接。

####   1.3 注意

​       ping指的是端对端连通，通常用来作为可用性的检查，但是某些病毒木马会强行大量远程执行ping命令抢占你的网络资源，导致系统变慢，网速变慢。严禁ping入侵作为大多数防火墙的一个基本功能提供给用户进行选择。通常的情况下你如果不用作服务器或者进行网络测试，ping是被防火墙禁用的（如果被禁用可以采取下面提到的telnet方式）。

    ping命令是用来检测网络是否畅通的，而Telnet命令则用来远程登陆。 但Ping不通并不一定代表网络不通。ping是基于ICMP协议的命令，就是你发出去一个数据包，对方收到后返给你一个！就好比声纳。这个协议是可以禁止的！禁止后，如果你ping对方，对方收到后就不回馈给你，这样你就显示无法ping通，但实际你们还是连着的。

####   1.4 使用

ping [option] [dst_ip | dst_host]

### 2.telnet

#### 2.1 介绍

​    telnet 用于远程管理连接主机, 同时也是测试**目标机器的TCP端口是否开放**。

#### 2.2 原理

​      Telnet是位于OSI模型的第7层---应用层上的一种协议，是一个通过创建虚拟终端提供连接到远程主机终端仿真的TCP/IP协议。这一协议需要通过用户名和口令进行认证，是Internet远程登陆服务的标准协议。应用Telnet协议能够把本地用户所使用的计算机变成远程主机系统的一个终端。

#### 2.3 注意

​      传统Telnet会话所传输的资料并未加密，帐号和密码等敏感资料，容易会被窃听，因此很多服务器都会封锁Telnet服务，改用更安全的SSH。

#### 2.4 使用

telnet host [port] 

-a      企图自动登录。除了用当前已登陆的用户名以外，与 -l 选项相同。 

-e      跳过字符来进入 telnet 客户端提示。 

-f      客户端登录的文件名 

-l      指定远程系统上登录用的用户名称。

-t      指定终端类型。 

host    指定要连接的远程计算机的主机名或 IP 地址。 

port    指定端口号或服务名。
telnet [ipaddr] [port

### 3.tracert

#### 3.1 介绍

​     tracert是路由跟踪程序，用于**确定 IP 数据报访问目标所经过的路径**。Tracert 命令用 IP 生存时间 (TTL) 字段和 ICMP 错误消息来确定从一个主机到网络上其他主机的路由。 在工作环境中有多条链路出口时，可以通过该命令查询数据是经过的哪一条链路出口。tracert一般用来检测故障的位置，我们可以使用用tracert IP命令确定数据包在网络上的停止位置，来判断在哪个环节上出了问题，虽然还是没有确定是什么问题，但它已经告诉了我们问题所在的地方，方便检测网络中存在的问题。

#### 3.2 使用

用法: tracert [option] [ip | domain]

选项:
    -d                 不将地址解析成主机名。
    -h maximum_hops    搜索目标的最大跃点数。
    -j host-list       与主机列表一起的松散源路由(仅适用于 IPv4)。
    -w timeout         等待每个回复的超时时间(以毫秒为单位)。
    -R                 跟踪往返行程路径(仅适用于 IPv6)。
    -S srcaddr         要使用的源地址(仅适用于 IPv6)。
    -4                 强制使用 IPv4。
    -6                 强制使用 IPv6。

#### 3.3 实例

```
C:\Users\Leon>tracert baidu.com

通过最多 30 个跃点跟踪
到 baidu.com [123.125.114.144] 的路由:

  1    <1 毫秒   <1 毫秒   <1 毫秒 192.168.0.1
  2    <1 毫秒   <1 毫秒   <1 毫秒 192.168.9.1
  3    <1 毫秒   <1 毫秒   <1 毫秒 192.168.1.1
  4    24 ms     6 ms     9 ms  115.206.244.1
  5     5 ms     5 ms     4 ms  61.164.8.125
  6    10 ms     7 ms     6 ms  220.191.128.117
  7     *       36 ms    36 ms  202.97.68.125
  8     *        *        *     请求超时。
  9    36 ms    38 ms    38 ms  219.158.44.125
 10    39 ms    38 ms    38 ms  219.158.3.69
 11    37 ms    36 ms    37 ms  125.33.186.22
 12    31 ms    31 ms    31 ms  202.106.37.46
 13    34 ms    34 ms    34 ms  202.106.48.38
 14     *        *        *     请求超时。
 15    37 ms    37 ms    37 ms  123.125.114.144

跟踪完成。
```


第一列：1..9，这标明在我使用的宽带上，经过8个路由节点，可以到达百度的服务器；如果是电信可能有不同；其他的IP，也有可能不同；各位可以多去测试。

第二，三，四列：单位是ms，是表示我们连接到每个路由节点的速度，返回速度和多次链接反馈的平均值；这个值有一定的参考性，但不是唯一的，也不作为主要的参考；比如一个主机，200和300，这100ms，其实说明不了什么。

第四列：就是每个路由节点对应的IP，每个ip输入什么，各位通过http://tool.chinaz.com的ip详细查询去了解。

注：在第13个路由节点上，返回消息是超时，这表示这个路由节点和当前我们使用的宽带，是无法连通的。至于原因，就有很多种了，比如：特意在路上上做了过滤限制；比如确实是路由的问题等，具体问题具体分析；如果在测试的时候，大量的都是*和返回超时，那就说明这个IP，在各个路由节点都有问题。

这个命令，更多的使用各位也可以搜索一下，我只是介绍了基本常用的功能，可以用来做判断。



### 4. Android 实现

####     4.1 ping 实现

​     调用Android 系统 sh 执行ping 命令

```
String command = "ping -c 3 baidu.com";

try(DataOutputStream os = null) {
    process = Runtime.getRuntime().exec(isRoot ? "su" : "sh");
    os = new DataOutputStream(process.getOutputStream());
    os.write(command.getBytes());
    os.writeBytes(LINE_SEP);
    os.flush();
    result = process.waitFor();
}catch (Exception e) {
     e.printStackTrace();
} 
```



####  4.2 Telnet实现

  使用apache commons-net包TelnetClient实现

```

public class TelnetUtils {
   private TelnetClient client;
    public TelnetUtils() {
        client = new TelnetClient();
        client.setConnectTimeout(30000);
    }
    **
     * 连接及登录
     * @param ip   目标主机IP
     * @param port 端口号（Telnet 默认 23）
     */
    public String connect(String ip, int port) {
        try {
            Log.d(TAG, "connecting : " + ip + " port: " + port);
            client.connect(ip, port);
            return "success";
        } catch (Exception e) {
            return "err: " + e.getMessage();
        } finally {
            disconnect();
        }
    }
      /**
     * 关闭连接
     */
    public void disconnect() {
        try {
            if (client != null) {
                client.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
  }
```

 通过TelnetUtils#connect 判断端口是否打开



#### 4.3 Tracert(Trancerouter)

实现方式通过 android ping 命令 通过循环指定 -t的值, 获取router ip 的网络状况, 

其中返回值中 From的ip, 就是对应ttl的route的ip, 结果为对应ip 的网络状况

一般会指定 maxTtl 为30次

```
>ping -c 1 -w 4 -t 1 www.baidu.com
PING www.a.shifen.com (180.101.49.11) 56(84) bytes of data.
From 192.168.3.1 (192.168.3.1): icmp_seq=1 Time to live exceeded

--- www.a.shifen.com ping statistics ---
1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms

>ping -c 1 -w 4 -t 2 www.baidu.com
PING www.a.shifen.com (180.101.49.11) 56(84) bytes of data.
From 192.168.1.1 (192.168.1.1): icmp_seq=1 Time to live exceeded

--- www.a.shifen.com ping statistics ---
1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms

>ping -c 1 -w 4 -t 3 www.baidu.com
PING www.a.shifen.com (180.101.49.11) 56(84) bytes of data.
From 122.235.84.1: icmp_seq=1 Time to live exceeded

--- www.a.shifen.com ping statistics ---
1 packets transmitted, 0 received, +1 errors, 100% packet loss, time 0ms
```



原理已经说明, 请自行通过前面的ping 命令实现traceroute 功能