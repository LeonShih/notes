# init进程启动过程 (system/core/init/init.cpp)

### Android 系统启动流程(前几步,引出init进程)

1. 启动电源及系统启动

   按下电源是 引导芯片代码从预定义的地方(固化在ROM)开始执行,加载bootloader 到RAM,然后执行.

2. 引导程序 BootLoader 

   Android系统启动前的一个小程序, 主要将Android OS 拉起并运行

3. Linux 内核启动

   内核启动时,设置缓存,被保护的存储器,计划列表,加载驱动,内核加载完成系统设置后,它受限在系统文件寻找init.rc, 并启动init 进程

4. init进程启动

   主要用来初始化和启动属性服务,也用来启动Zygote进程

### 一切从main函数开始

```
int main(int argc, char** argv) {
    //strcmp是String的一个函数，比较字符串，相等返回0
    //basename是C库中的一个函数，得到特定的路径中的最后一个'/'后面的内容，
    if (!strcmp(basename(argv[0]), "ueventd")) {
        return ueventd_main(argc, argv);
    }

    if (!strcmp(basename(argv[0]), "watchdogd")) {
        return watchdogd_main(argc, argv);
    }

    // Clear the umask.
    umask(0);

    add_environment("PATH", _PATH_DEFPATH);

   // 只有一个参数 或者不是--second-stage时 才是第一阶段
    bool is_first_stage = (argc == 1) || (strcmp(argv[1], "--second-stage") != 0);

    // 创建和挂载启动所需的文件目录
    if (is_first_stage) {
        mount("tmpfs", "/dev", "tmpfs", MS_NOSUID, "mode=0755");
        mkdir("/dev/pts", 0755);
        mkdir("/dev/socket", 0755);
        mount("devpts", "/dev/pts", "devpts", 0, NULL);
        mount("proc", "/proc", "proc", 0, NULL);
        mount("sysfs", "/sys", "sysfs", 0, NULL);
    }

    // We must have some place other than / to create the device nodes for
    // kmsg and null, otherwise we won't be able to remount / read-only
    // later on. Now that tmpfs is mounted on /dev, we can actually talk
    // to the outside world.
    open_devnull_stdio();
    klog_init();
    klog_set_level(KLOG_NOTICE_LEVEL);

    NOTICE("init%s started!\n", is_first_stage ? "" : " second stage");
	
	//第二阶段 进行属性初始化
    if (!is_first_stage) {
        // Indicate that booting is in progress to background fw loaders, etc.
        close(open("/dev/.booting", O_WRONLY | O_CREAT | O_CLOEXEC, 0000));

        property_init();

        // If arguments are passed both on the command line and in DT,
        // properties set in DT always have priority over the command-line ones.
        process_kernel_dt();
        process_kernel_cmdline();

        // Propogate the kernel variables to internal variables
        // used by init as well as the current required properties.
        export_kernel_boot_props();
    }

    // SELinux 初始化
    selinux_initialize(is_first_stage);

    //如果还在第一阶段 kernel domain, 切换到 init domain
    if (is_first_stage) {
        if (restorecon("/init") == -1) {
            ERROR("restorecon failed: %s\n", strerror(errno));
            security_failure();
        }
        char* path = argv[0];
        char* args[] = { path, const_cast<char*>("--second-stage"), nullptr };
        //重新执行main函数(--second-stage) 进入第二阶段 
        if (execv(path, args) == -1) {
            ERROR("execv(\"%s\") failed: %s\n", path, strerror(errno));
            security_failure();
        }
    }

    ...

    // 创建epoll 句柄
    epoll_fd = epoll_create1(EPOLL_CLOEXEC);
    ...

   //子进程信号处理函数
    signal_handler_init();

    property_load_boot_defaults();
    //启动属性服务
    start_property_service();

   // 最后进行解析init.rc 配置
    init_parse_config_file("/init.rc");
    
    //事件回调函数绑定

    action_for_each_trigger("early-init", action_add_queue_tail);

    // Queue an action that waits for coldboot done so we know ueventd has set up all of /dev...
    queue_builtin_action(wait_for_coldboot_done_action, "wait_for_coldboot_done");
    // ... so that we can start queuing up actions that require stuff from /dev.
    queue_builtin_action(mix_hwrng_into_linux_rng_action, "mix_hwrng_into_linux_rng");
    queue_builtin_action(keychord_init_action, "keychord_init");
    queue_builtin_action(console_init_action, "console_init");

    // Trigger all the boot actions to get us started.
    action_for_each_trigger("init", action_add_queue_tail);

    // Repeat mix_hwrng_into_linux_rng in case /dev/hw_random or /dev/random
    // wasn't ready immediately after wait_for_coldboot_done
    queue_builtin_action(mix_hwrng_into_linux_rng_action, "mix_hwrng_into_linux_rng");

    // Don't mount filesystems or start core system services in charger mode.
    char bootmode[PROP_VALUE_MAX];
    if (property_get("ro.bootmode", bootmode) > 0 && strcmp(bootmode, "charger") == 0) {
        action_for_each_trigger("charger", action_add_queue_tail);
    } else {
        action_for_each_trigger("late-init", action_add_queue_tail);
    }

    // Run all property triggers based on current state of the properties.
    queue_builtin_action(queue_property_triggers_action, "queue_property_triggers");

    while (true) {
        if (!waiting_for_exec) {
            execute_one_command();
            restart_processes();
        }

        int timeout = -1;
        if (process_needs_restart) {
            timeout = (process_needs_restart - gettime()) * 1000;
            if (timeout < 0)
                timeout = 0;
        }

        if (!action_queue_empty() || cur_action) {
            timeout = 0;
        }

        bootchart_sample(&timeout);

        epoll_event ev;
        int nr = TEMP_FAILURE_RETRY(epoll_wait(epoll_fd, &ev, 1, timeout));
        if (nr == -1) {
            ERROR("epoll_wait failed: %s\n", strerror(errno));
        } else if (nr == 1) {
            ((void (*)()) ev.data.ptr)();
        }
    }

    return 0;
}

```

主要做的事情有三个部分

1. 第一阶段 创建和挂载启动所需的文件目录
2. 第二阶段 属性初始化
3. init.rc 文件解析,并且创建zygote 进程

### init.rc

> 配置文件,它由Android初始化语言(Android Init Language)编写的脚本,主要包含5种类型语句
>
> - Action
> - Command
> - Service
> - Option
> - Import



#### 部分init.rc 内容

```
import /init.environ.rc
import /init.usb.rc
import /init.${ro.hardware}.rc
import /init.usb.configfs.rc
import /init.${ro.zygote}.rc
import /init.trace.rc

on early-init
    # Set init and its forked children's oom_adj.
    write /proc/1/oom_score_adj -1000

    # Set the security context of /adb_keys if present.
    restorecon /adb_keys

    start ueventd

on init
    sysclktz 0

    # Backward compatibility.
    symlink /system/etc /etc
    symlink /sys/kernel/debug /d

    # Link /vendor to /system/vendor for devices without a vendor partition.
    symlink /system/vendor /vendor

    # Create cgroup mount point for cpu accounting
    mkdir /acct
    mount cgroup none /acct cpuacct
    mkdir /acct/uid
```

#### on init  /on early-init 是Action的语法

```
on <trigger> [&& <trigger>] * //设置触发器
 <command>
 <command>   //动作出发后执行的命令
```

#### init.zygote64.rc 文件

```
service zygote /system/bin/app_process64 -Xzygote /system/bin --zygote --start-system-server
    class main
    socket zygote stream 660 root system
    onrestart write /sys/android_power/request_state wake
    onrestart write /sys/power/state on
    onrestart restart media
    onrestart restart netd
    writepid /dev/cpuset/foreground/tasks

```

#### service 相关语法

```
service <name> <pathname> [<argument>] *
<option>  //option 是service的修饰词,影响什么后,如何启动service
<option>
```

#### 如何启动zygote 服务

通过init.zygote64.rc 了解 zygote的classname为 main

```
#init.rc
on nonencrypted
    class_start main
    class_start late_start

```

init.rc 会启动 main class,这就启动了zygote进程