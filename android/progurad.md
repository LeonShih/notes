# proguard 问题处理

https://www.guardsquare.com/en/products/proguard/manual/troubleshooting

### 1.找不到引用

> **Note: can't find dynamically referenced class ...**
>
> ProGuard can't find a class or interface that your code is accessing by means of introspection. You should consider adding the jar that contains this class.

**原因:**未使用该类,但混淆加了, 

**解决:**删除没有使用的混淆代码

### 2.未指定成员

> **Note: the configuration doesn't specify which class members to keep for class '...'**
>
> Your configuration contains a[`-keepclassmembers`](https://www.guardsquare.com/en/products/proguard/manual/usage#keepclassmembers)/[`-keepclasseswithmembers`](https://www.guardsquare.com/en/products/proguard/manual/usage#keepclasseswithmembers)option to preserve fields or methods in the given class, but it doesn't specify which fields or methods. This way, the option simply won't have any effect. You probably want to specify one or more fields or methods, as usual between curly braces. You can specify all fields or methods with a wildcard "`*;`". You should also consider if you just need the more common[`-keep`](https://www.guardsquare.com/en/products/proguard/manual/usage#keep) option, which preserves all specified classes *and*class members. The [overview of all `keep` options](https://www.guardsquare.com/en/products/proguard/manual/usage#keepoverview) can help. You can switch off these notes by specifying the[`-dontnote`](https://www.guardsquare.com/en/products/proguard/manual/usage#dontnote) option.

原因:**未使用该类,但混淆加了, 

解决: 使用keep

3

> **Note: the configuration explicitly specifies '...' to keep library class '...'**
>
> Your configuration contains a [`-keep`](https://www.guardsquare.com/en/products/proguard/manual/usage#keep) option to preserve the given library class. However, you don't need to keep any library classes. ProGuard always leaves underlying libraries unchanged. You can switch off these notes by specifying the [`-dontnote`](https://www.guardsquare.com/en/products/proguard/manual/usage#dontnote)option.

原因: Progard不会混淆提示的库,不需要混淆

解决:  删除混淆 或者 使用-dontnote 

4.动态引用类找不到

> **Note: can't find dynamically referenced class ...**
>
> ProGuard can't find a class or interface that your code is accessing by means of introspection. You should consider adding the jar that contains this class.



5.  问题 找不动引用

> **Note: the configuration keeps the entry point '...', but not the descriptor class '...'**
>
> Your configuration contains a [`-keep`](https://www.guardsquare.com/en/products/proguard/manual/usage#keep) option to preserve the given method (or field), but no `-keep` option for the given class that is an argument type or return type in the method's descriptor. You may then want to keep the class too. Otherwise, ProGuard will obfuscate its name, thus changing the method's signature. The method might then become unfindable as an entry point, e.g. if it is part of a public API. You can automatically keep such descriptor classes with the `-keep` option modifier[`includedescriptorclasses`](https://www.guardsquare.com/en/products/proguard/manual/usage#includedescriptorclasses)(`-keep,includedescriptorclasses` ...). You can switch off these notes by specifying the [`-dontnote`](https://www.guardsquare.com/en/products/proguard/manual/usage#dontnote) option.