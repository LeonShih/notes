# Android签名证书

## 背景

因业务需求,经常与系统厂商打交道,大多需要厂商开放root权限

**目前获取系统权限的总结有三种方式(均有实际应用)**

1. **platform签名**
2. **root固件 (有su)**
3. **调用厂商封装好的系统权限的sdk**

后两者调试比较简单,不多做介绍. 针对第一点,厂商会提供两个文件

​       platform.pk8                保存private key 加密

​       platform.x509.pem     X.509证书 **存储数字证书，公钥信息还能存各种key**

但是这两种文件不能直接进行签名,需要调用

```
java -jar signapk.jar platform.x509.pem platform.pk8 old.apk [new].apk
```

Android Studio 生成apk,还需额外执行以上步骤,调试非常不方便,故想将pk8 和pem 转成通用的jks或者keystore的签名

**Note:**

> android一般除了使用jarsigner签名,还有使用signapk 后者位于android源码,前者位于jdk中。两者使用的文件格式不同前者使用JKS,后者使用公钥+私钥是分开的

signapk.jar 不是系统自带,需要额外下载

## 环境配置 

**下载 [openssl ](http://slproweb.com/products/Win32OpenSSL.html)**

![1571887687153](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/1571887687153.png)

**选择合适的文件下载安装,完成后配置环境变量**

![openssl_env](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/openssl_env.png)

## pem/pk8 转换jks/keystore

*keystore,jks本身是一个东西，没有区别；在eclipse上生成的是.keystore后缀，在andorid studio上生成的是.jks后缀；*

**主要分三步**

1. **pk8 私钥解密pem格式**

   ```
   openssl pkcs8 -in platform.pk8 -inform DER -outform PEM -out platform.priv.pem -nocrypt
   ```

2. **私钥通过公钥pem加密pk12**

   ```
   openssl pkcs12 -export -in platform.x509.pem -inkey platform.priv.pem -out platform.pk12 -name [别名]
   ```

   需要输入两次密码

3. **通过java的keytool 工具生成 keystore**

   ```
   keytool -importkeystore -destkeystore platform.jks -srckeystore platform.pk12 -srcstoretype PKCS12 -srcstorepass [密码] -alias [别名]
   ```

   别名需要跟步骤2的一致,同样输入两次密码

**windows 下合并后如下**

```
openssl pkcs8 -in platform.pk8 -inform DER -outform PEM -out platform.priv.pem -nocrypt
openssl pkcs12 -export -in platform.x509.pem -inkey platform.priv.pem -out platform.pk12 -name [别名]
keytool -importkeystore -destkeystore platform.jks -srckeystore platform.pk12 -srcstoretype PKCS12 -srcstorepass [密码] -alias [别名]
pause
```

## Android Studio 项目使用jks

```
android {
 signingConfigs {
        MySignName {
            keyAlias "[别名]"
            keyPassword "[密码]"
            storeFile file('[jks路径]')
            storePassword "[密码]"
        }
    }
    
 buildTypes {
        debug {
         		signingConfig signingConfigs.MySignName
        }
        release{
             signingConfig signingConfigs.MySignName
        }
   }  
}
```

现在Android Studio 调试安装都是platform签名

## 扩展  jks 转pem/pk8

**主要分三步**

1. **java keytool 转成 pkcs12格式**

   ```
   keytool   -importkeystore -srckeystore debug.keystore   -destkeystore tmp.p12 -srcstoretype JKS         -deststoretype PKCS12
   ```

2. **pkcs12 dump pem格式**

   ```
   openssl pkcs12 -in tmp.p12 -nodes -out tmp.rsa.pem
   ```

   dump的文件格式

   ```
   Bag Attributes
       friendlyName: [别名]
       localKeyID: 54 69 6D 65 20 31 35 37 31 38 39 30 30 31 35 30 30 30 
   Key Attributes: <No Attributes>
   -----BEGIN PRIVATE KEY-----
   ....
   -----END PRIVATE KEY-----
   Bag Attributes
       friendlyName: [别名]
       localKeyID: 54 69 6D 65 20 31 35 37 31 38 39 30 30 31 35 30 30 30 
   subject=C = US, ST = California, L = Mountain View, O = Android, OU = Android, CN = Android, emailAddress = android@android.com
   issuer=C = US, ST = California, L = Mountain View, O = Android, OU = Android, CN = Android, emailAddress = android@android.com
   
   -----BEGIN CERTIFICATE-----
   ...
   -----END CERTIFICATE-----
   ```

3. **复制“BEGIN CERTIFICATE”  “END CERTIFICATE” 到（新建个文件） cert.x509.pem** 

   dos命令不好实现,建议采用bash

   ```bash
   grep -zoe '-----BEGIN CERTIFICATE-----.*-----END CERTIFICATE-----' tmp.rsa.pem>cert.x509.pem
   ```

   

4. **复制 “BEGIN RSA PRIVATE KEY”   “END RSA PRIVATE KEY” 到（同上） private.rsa.pem** 

   dos命令不好实现,建议采用bash

   ```
   grep -zoe '-----BEGIN CERTIFICATE-----.*-----END CERTIFICATE-----' tmp.rsa.pem>private.rsa.pem
   ```

5. **生成pk8格式的私钥**

   ```
   openssl pkcs8 -topk8 -outform DER -in   private.rsa.pem -inform PEM -out private.pk8 -nocrypt
   ```

   至此已经生成pem pk8 格式的两个签名文件

## 参考

[.pem和.pk8是什么文件？](https://blog.csdn.net/qq_34352738/article/details/79360044)

[签名证书keystore,jks,pk8,x509.pem](http://www.voidcn.com/article/p-zkvlyaht-bpg.html)

[keystore文件转换格式为pk8+x509.pem](http://www.voidcn.com/article/p-ulekofsk-bdq.html)