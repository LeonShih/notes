## Apk打包过程

Android的包文件APK分为两个部分：代码和资源，所以打包方面也分为资源打包和代码打包两个方面

APK整体的的打包流程如下图所示：

![打包过程](https://gitee.com/LeonShih/Image/raw/master/apk_package_flow.png)

具体说来：



1. 通过AAPT工具进行资源文件（包括AndroidManifest.xml、布局文件、各种xml资源等）的打包，生成R.java文件。
2. 通过AIDL工具处理AIDL文件，生成相应的Java文件。
3. 通过Javac工具编译项目源码，生成Class文件。
4. 通过DX工具将所有的Class文件转换成DEX文件，该过程主要完成Java字节码转换成Dalvik字节码，压缩常量池以及清除冗余信息等工作。
5. 通过ApkBuilder工具将资源文件、DEX文件打包生成APK文件。
6. 利用KeyStore对生成的APK文件进行签名。
7. 如果是正式版的APK，还会利用ZipAlign工具进行对齐处理，对齐的过程就是将APK文件中所有的资源文件举例文件的起始距离都偏移4字节的整数倍，这样通过内存映射访问APK文件
    的速度会更快。

## apk 安装过程

APK的安装流程如下：

1. 复制APK到/data/app目录下，解压并扫描安装包。
2. 资源管理器解析APK里的资源文件。
3. 解析AndroidManifest文件，并在/data/data/目录下创建对应的应用数据目录。
4. 然后对dex文件进行优化，并保存在dalvik-cache目录下。
5. 将AndroidManifest文件解析出的四大组件信息注册到PackageManagerService中。
6. 安装完成后，发送广播。 data/system/packages.xml





## 虚拟机

**dalvik (Android 5.0以下 执行 odex)**

​     dex--dexopt -->odex

**art (执行 oat)**

​     dex--dex2oat -->oat

**dalvik字节码**

​	寄存器(默认32位) 命名规则 

​			变量 v0-vn  

​			参数 p0-pn

### java签名

| 签名                       | 对应Java中的类型                |
| -------------------------- | ------------------------------- |
| V                          | void，只能用于返回类型          |
| Z                          | <font color=red>boolean </font> |
| B                          | byte                            |
| S                          | short                           |
| C                          | char                            |
| I                          | int                             |
| J                          | <font color=red>long</font>     |
| F                          | float                           |
| D                          | double                          |
| Ljava/lang/String;         | string                          |
| Lpackage/name/ObjectName;  | ObjectName类                    |
| [I                         | int[]                           |
| [Ljava/lang/String;        | string[]                        |
| [Lpackage/name/ObjectName; | ObjectName[]                    |

**方法声明及调用**

```
Lpackage/name/ObjectName;->MethodName(III)Z
           类名签名       调用  方法名   参数 返回值
                          构造的方法名<init>
```

**成员变量**

```
Lpackage/name/ObjectName;->FieleName:Z
对象类型                   -> 成员名  : 成员类型
```

**寄存器声明及使用**
在Smali中，如果需要存储变量，必须先声明足够数量的寄存器，1个寄存器可以存储32位长度的类型，比如Int，而两个寄存器可以存储64位长度类型的数据，比如Long或Double

声明可使用的寄存器数量的方式为：.registers N，N代表需要的寄存器的总个数，同时，还有一个关键字.locals，它用于声明非参数的寄存器个数（包含在registers声明的个数当中），也叫做本地寄存器，只在方法内有效


## [dalvik指令集](http://pallergabor.uw.hu/androidblog/dalvik_opcodes.html)

一般的指令格式为：

```
[op]-[type](可选)/[位宽，默认4位] [目标寄存器],[源寄存器](可选)
move v1,v2，
move-wide/from16 v1,v2
```



- 数据操作

> 用于将源寄存器的值移动到目标寄存器中。

其格式为：**move 目标寄存器 源寄存器**。

此类操作常用于赋值

|         命令          |                          说明                           |
| :-------------------: | :-----------------------------------------------------: |
|      move vx,vy       |                   将 vy 的值赋值给 vx                   |
|    move-wide vx,vy    |     将 vy 中存储的 long 或 double 类型数据赋值给 vx     |
|   move-object vx,vy   |              将 vy 存储的对象引用赋值给 vx              |
|    move-result vx     |            将上一个方法调用的结果值移到vx中             |
|  move-result-wide vx  | 将上个方法调用的结果 ( 类型为 double 或 long ) 移到vx中 |
| move-result-object vx |       将上一个方法调用后得到的对象的引用赋值给 vx       |
|   move-exception vx   |         将本方法在执行过程中抛出的异常赋值给 vx         |

- 返回操作：

  用于返回值，对应Java中的return语句

  |     **指令**     | **说明**                 |
  | :--------------: | :----------------------- |
  |   return-void    | 返回void，即直接返回     |
  |    return v1     | 返回v1寄存器中的值       |
  | return-object v1 | 返回v1寄存器中的对象指针 |
  |  return-wide v1  | 回双字型结果给v1寄存器   |

- 常量操作

  |               **指令**                | **说明**                                                     |
  | :-----------------------------------: | :----------------------------------------------------------- |
  |    const(/4、/16、/hight16) v1 xxx    | 将常量xxx赋值给v1寄存器，`/`后的类型，需要根据xxx的长度选择  |
  | const-wide(/16、/32、/hight16) v1 xxx | 将双字型常量xxx赋值给v1寄存器，`/`后的类型，需要根据xxx的长度选择 |
  |     const-string(/jumbo) v1 “aaa”     | 将字符串常量”aaa”赋给v1寄存器，过长时需要加上jumbo           |
  |    const-class v1 La/b/TargetClass    | 将Class常量a.b.TargetClass赋值给v1，等价于a.b.TargetClass.class |

  ​	

- 调用操作：

  |     **指令**     | **说明**                                                     |
  | :--------------: | :----------------------------------------------------------- |
  |  invoke-virtual  | 用于调用一般的，非private、非static、非final、非构造函数的方法，<br>它的第一个参数往往会传p0，也就是this指针 |
  |   invoke-super   | 用于调用父类中的方法，其他和invoke-virtual保持一致           |
  |  invoke-direct   | 用于调用private修饰的方法，或者构造方法                      |
  |  invoke-static   | 用于调用静态方法，比如一些工具类                             |
  | invoke-interface | 用于调用interface中的方法                                    |

- 判断操作

  |       命令        | 说明                                    |
  | :---------------: | :-------------------------------------- |
  |    if-eq v1,v2    | 是否相等     if-eqz v1  是否与0相等     |
  |    if-ne v1,v2    | 是否不相等  if-nez v1  是否不等于0      |
  |    if-lt v1,v2    | 是否小于                                |
  |    if-ge v1,v2    | 是否大于等于                            |
  |    if-gt v1,v2    | 是否大于                                |
  |    if-le v1,v2    | 是否小于等于                            |
  | move-exception vx | 将本方法在执行过程中抛出的异常赋值给 vx |

- 属性操作

  |  **指令**   | **说明**                                            |
  | :---------: | :-------------------------------------------------- |
  | iget v1 ,v2 | 取值，用于操作int这种的值类型  v1 =v2  可以扩展类型 |
  | iput v1 ,v2 | 赋值，用于操作int这种的值类型  v2 = v1              |
  | sget v1 ,v2 | 静态变量取值  ，用于操作int这种的值类型  v1 =v2     |
  | sput v1 ,v2 | 静态变量赋值  ，用于操作int这种的值类型  v2 =v1     |

  

- 实例操作

  |            **指令**            | **说明** |
  | :----------------------------: | :------- |
  |   check-cast v1 , type@BBBB    | 类型强转 |
  | instance-of v1, v2 , type@BBBB | 类型判断 |
  |  new-instance v1 , type@BBBB   | 对象创建 |
  |           new-array            | 数组创建 |

- 异常操作 throw

- 跳转指令

   goto   直接跳转

  switch 分支跳转

- 比较操作 cmp/cmpl/cmpg

- 数据转换

  - neg-数据类型  求补
  - not-数据类型  求反
  - 数据类型1-to-数据类型2   将 类型1转成类型2 ,如 int-to-float

-    数据操作
  - add/sub/nul/div/rem  加减乘除 取余
  - and/or/xor   与或异或
  - shl/shr/ushr  左移右移 无符号右移

## smali

![smali](https://gitee.com/LeonShih/Image/raw/master/smali_format.png)