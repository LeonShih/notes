# DataBinding

## 简介

> ​	数据绑定库是一种支持库，借助该库，您可以使用声明性格式（而非程序化地）将布局中的界面组件绑定到应用中的数据源。

```
<TextView
        android:text="@{viewmodel.userName}" />
```

借助布局文件中的绑定组件，您可以移除 Activity 中的许多界面框架调用，使其维护起来更简单、方便。还可以提高应用性能，并且有助于防止内存泄漏以及避免发生 Null 指针异常。

## 使用

gradle配置

```
android {
        ...
        dataBinding {
            enabled = true
        }
       // dataBinding.enabled = true
    }
    
```

## 布局和绑定表达式

数据绑定布局文件略有不同，以根标记 `layout` 开头，后跟 `data` 元素和 `view` 根元素。此视图元素是非绑定布局文件中的根。以下代码展示了示例布局文件：

```
<?xml version="1.0" encoding="utf-8"?>
    <layout xmlns:android="http://schemas.android.com/apk/res/android">
       <data>
           <variable name="user" type="com.example.User"/>
       </data>
       <LinearLayout
           android:orientation="vertical"
           android:layout_width="match_parent"
           android:layout_height="match_parent">
           <TextView android:layout_width="wrap_content"
               android:layout_height="wrap_content"
               android:text="@{user.firstName}"/>
           <TextView android:layout_width="wrap_content"
               android:layout_height="wrap_content"
               android:text="@{user.lastName}"/>
       </LinearLayout>
    </layout>
```

布局中的表达式使用“`@{}`”语法写入特性属性中

## 数据对象

```
data class User(val firstName: String, val lastName: String)
```

MVC中是Model

MVP中是Presenter

MVVM中是ViewModel

## 绑定数据

> 系统会为每个布局文件生成一个绑定类。默认情况下，类名称基于布局文件的名称，它会转换为 Pascal 大小写形式并在末尾添加 Binding 后缀。以上布局文件名为 activity_main.xml，因此生成的对应类为 ActivityMainBinding。此类包含从布局属性（例如，user 变量）到布局视图的所有绑定，并且知道如何为绑定表达式指定值。建议的绑定创建方法是在扩充布局时创建，如以下示例所示：

```
override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(
                this, R.layout.activity_main)

        binding.user = User("Test", "User")
    }
```

*Pascal* *大小写形式*-所有单词第一个字母大写,其他字母小写

Camel *大小写形式*-除了第一个单词,所有单词第一个字母大写,其他字母小写

## 表达式语言

### 常见功能

m'n

- 算术运算符 `+ - / * %`
- 字符串连接运算符 `+`
- 逻辑运算符 `&& ||`
- 二元运算符 `& | ^`
- 一元运算符 `+ - ! ~`
- 移位运算符 `>> >>> <<`
- 比较运算符 `== > < >= <=`（请注意，`<` 需要转义为 `&lt;`）
- `instanceof`
- 分组运算符 `()`
- 字面量运算符 - 字符、字符串、数字、`null`
- 类型转换
- 方法调用
- 字段访问
- 数组访问 `[]`
- 三元运算符 `?:`

```
android:text="@{String.valueOf(index + 1)}"
    android:visibility="@{age > 13 ? View.GONE : View.VISIBLE}"
    android:transitionName='@{"image_" + id}'  
```

### 缺少的运算

您可以在托管代码中使用的表达式语法中缺少以下运算：

- `this`
- `super`
- `new`
- 显式泛型调用

### Null 合并运算符

如果左边运算数不是 `null`，则 Null 合并运算符 (`??`) 选择左边运算数，如果左边运算数为 ，则选择右边运算数。

```
android:text="@{user.displayName ?? user.lastName}"

//等价
android:text="@{user.displayName != null ? user.displayName : user.lastName}"
```

### 属性引用

表达式可以使用以下格式在类中引用属性，这对于字段、getter 和 [`ObservableField`](https://developer.android.google.cn/reference/androidx/databinding/ObservableField.html?hl=zh_cn) 对象都一样：

```
android:text="@{user.lastName}"    
```

### 避免出现 Null 指针异常

生成的数据绑定代码会自动检查有没有 `null` 值并避免出现 Null 指针异常。例如，在表达式 `@{user.name}` 中，如果 `user` 为 Null，则为 `user.name` 分配默认值 `null`。如果您引用 `user.age`，其中 age 的类型为 `int`，则数据绑定使用默认值 `0`。

### 集合

为方便起见，可使用 `[]` 运算符访问常见集合，例如数组、列表、稀疏列表和映射。

```xml
<data>
        <import type="android.util.SparseArray"/>
        <import type="java.util.Map"/>
        <import type="java.util.List"/>
        <variable name="list" type="List&lt;String>"/>
        <variable name="sparse" type="SparseArray&lt;String>"/>
        <variable name="map" type="Map&lt;String, String>"/>
        <variable name="index" type="int"/>
        <variable name="key" type="String"/>
    </data>
    …
    android:text="@{list[index]}"
    …
    android:text="@{sparse[index]}"
    …
    android:text="@{map[key]}"
```



**注意**：要使 XML 不含语法错误，您必须转义 `<` 字符。例如：不要写成 `List<String>` 形式，而是必须写成 `List&lt;String>`。

可以使用 `object.key` 表示法在映射中引用值。例如，以上示例中的 `@{map[key]}` 可替换为 `@{map.key}`。



### 字符串字面量

您可以使用单引号括住特性值，这样就可以在表达式中使用双引号，如以下示例所示：

```xml
android:text='@{map["firstName"]}'    
```

也可以使用双引号括住特性值。如果这样做，则还应使用反单引号 ``` 将字符串字面量括起来：

```xml
android:text="@{map[`firstName`]}"    
```

### 资源

您可以使用以下语法访问表达式中的资源：

```xml
android:padding="@{large? @dimen/largePadding : @dimen/smallPadding}"   
```

格式字符串和复数形式可通过提供参数进行求值：

```
android:text="@{@string/nameFormat(firstName, lastName)}"
    android:text="@{@plurals/banana(bananaCount)}"
```

某些资源需要显式类型求值，如下表所示：

| 类型              | 常规引用  | 表达式引用         |
| :---------------- | :-------- | :----------------- |
| String[]          | @array    | @stringArray       |
| int[]             | @array    | @intArray          |
| TypedArray        | @array    | @typedArray        |
| Animator          | @animator | @animator          |
| StateListAnimator | @animator | @stateListAnimator |
| color int         | @color    | @color             |
| ColorStateList    | @color    | @colorStateList    |

## 事件处理  

通过数据绑定，您可以编写从视图分派的表达式处理事件（例如，`onClick()` 方法）。

也可以使用以下机制处理事件

- [方法引用](https://developer.android.google.cn/topic/libraries/data-binding/expressions?hl=zh_cn#method_references)：在表达式中，您可以引用符合监听器方法签名的方法。当表达式求值结果为方法引用时，数据绑定会将方法引用和所有者对象封装到监听器中，并在目标视图上设置该监听器。如果表达式的求值结果为 `null`，则数据绑定不会创建监听器，而是设置 `null` 监听器。
- [监听器绑定](https://developer.android.google.cn/topic/libraries/data-binding/expressions?hl=zh_cn#listener_bindings)：这些是在事件发生时进行求值的 lambda 表达式。数据绑定始终会创建一个要在视图上设置的监听器。事件被分派后，监听器会对 lambda 表达式进行求值。

### 方法引用

```
<TextView android:layout_width="wrap_content"
               android:layout_height="wrap_content"
               android:text="@{user.firstName}"
               android:onClick="@{handlers::onClickFriend}"/>
```

**注意**：表达式中的方法签名必须与监听器对象中的方法签名完全一致。

### 监听器绑定

在方法引用中，**方法的参数必须与事件监听器的参数匹配**。在监听器绑定中，只有您的返回值必须与监听器的预期**返回值相匹配**（预期返回值无效除外）。

```
 <Button android:layout_width="wrap_content" android:layout_height="wrap_content"
            android:onClick="@{() -> presenter.onSaveClick(task)}" />
```

```
<CheckBox android:layout_width="wrap_content" android:layout_height="wrap_content"
          android:onCheckedChanged="@{(cb, isChecked) -> presenter.completeChanged(task, isChecked)}" />
    
```

如果您监听的事件返回类型不是 `void` 的值，则您的表达式也必须返回相同类型的值。例如，如果要监听长按事件，表达式应返回一个布尔值。

```
 class Presenter {
        fun onLongClick(view: View, task: Task): Boolean { }
    }
```

```
android:onLongClick="@{(theView) -> presenter.onLongClick(theView, task)}"
```

如果您需要将表达式与谓词（例如，三元运算符）结合使用，则可以使用 `void` 作为符号。

```
android:onClick="@{(v) -> v.isVisible() ? doSomething() : void}"
```

## 导入、变量和包含

数据绑定库提供了诸如导入、变量和包含等功能。通过导入功能，您可以轻松地在布局文件中引用类。通过变量功能，您可以描述可在绑定表达式中使用的属性。通过包含功能，您可以在整个应用中重复使用复杂的布局。

```
<import type="android.view.View"/>
<import type="com.example.real.estate.View"
            alias="Vista"/>
            
<import type="com.example.User"/>
<import type="java.util.List"/>
<variable name="user" type="User"/>
<variable name="userList" type="List&lt;User>"/>
```



```
<?xml version="1.0" encoding="utf-8"?>
    <layout xmlns:android="http://schemas.android.com/apk/res/android"
            xmlns:bind="http://schemas.android.com/apk/res-auto">
       <data>
           <variable name="user" type="com.example.User"/>
       </data>
       <LinearLayout
           android:orientation="vertical"
           android:layout_width="match_parent"
           android:layout_height="match_parent">
           <include layout="@layout/name"
               bind:user="@{user}"/>
           <include layout="@layout/contact"
               bind:user="@{user}"/>
       </LinearLayout>
    </layout>
```

数据绑定不支持 include 作为 merge 元素的直接子元素。例如，以下布局不受支持：

```
<?xml version="1.0" encoding="utf-8"?>
    <layout xmlns:android="http://schemas.android.com/apk/res/android"
            xmlns:bind="http://schemas.android.com/apk/res-auto">
       <data>
           <variable name="user" type="com.example.User"/>
       </data>
       <merge><!-- Doesn't work -->
           <include layout="@layout/name"
               bind:user="@{user}"/>
           <include layout="@layout/contact"
               bind:user="@{user}"/>
       </merge>
    </layout>
```



## 绑定适配器

某些特性需要自定义绑定逻辑。例如，`android:paddingLeft` 特性没有关联的 setter，而是提供了 `setPadding(left, top, right, bottom)` 方法。使用 [`BindingAdapter`](https://developer.android.google.cn/reference/androidx/databinding/BindingAdapter.html?hl=zh_cn) 注释的静态绑定适配器方法支持自定义特性 setter 的调用方式。

静态方法, 第一个参数为对应View

```
@BindingAdapter("android:paddingLeft")
    fun setPaddingLeft(view: View, padding: Int) {
        view.setPadding(padding,
                    view.getPaddingTop(),
                    view.getPaddingRight(),
                    view.getPaddingBottom())
    }

```

参数类型非常重要。第一个参数用于确定与特性关联的视图类型，第二个参数用于确定在给定特性的绑定表达式中接受的类型。

```
@BindingAdapter("imageUrl", "error")
    fun loadImage(view: ImageView, url: String, error: Drawable) {
        Picasso.get().load(url).error(error).into(view)
    }

```

```
<ImageView app:imageUrl="@{venue.imageUrl}" app:error="@{@drawable/venueError}" />
```

因为更改一个监听器也会影响另一个监听器，所以需要适用于其中一个特性或同时适用于这两个特性的适配器。您可以在注释中将 [`requireAll`](https://developer.android.google.cn/reference/androidx/databinding/BindingAdapter.html?hl=zh_cn#requireAll()) 设置为 `false`，以指定并非必须为每个特性都分配绑定表达式，如以下示例所示：

```
    @BindingAdapter(
            "android:onViewDetachedFromWindow",
            "android:onViewAttachedToWindow",
            requireAll = false
    )
    fun setListener(view: View, detach: OnViewDetachedFromWindow?, attach: OnViewAttachedToWindow?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            val newListener: View.OnAttachStateChangeListener?
            newListener = if (detach == null && attach == null) {
                null
            } else {
                object : View.OnAttachStateChangeListener {
                    override fun onViewAttachedToWindow(v: View) {
                        attach.onViewAttachedToWindow(v)
                    }

                    override fun onViewDetachedFromWindow(v: View) {
                        detach.onViewDetachedFromWindow(v)
                    }
                }
            }

            val oldListener: View.OnAttachStateChangeListener? =
                    ListenerUtil.trackListener(view, newListener, R.id.onAttachStateChangeListener)
            if (oldListener != null) {
                view.removeOnAttachStateChangeListener(oldListener)
            }
            if (newListener != null) {
                view.addOnAttachStateChangeListener(newListener)
            }
        }
    }
    
```

在某些情况下，需要在特定类型之间进行自定义转换。

```kotlin
 @BindingConversion
fun convertColorToDrawable(color: Int) = ColorDrawable(color)    
```