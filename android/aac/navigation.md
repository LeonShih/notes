# Navigation

## 简介

### 三大组成部分

1. Navigation graph      xml 资源文件
2. NavHost                     空容器,显示Nav graph  的destination
3. NavController          管理NavHost内 Navigation的一个对象

### 优点

> - Handling fragment transactions
> - Handling Up and Back actions correctly by default
> - Providing standardized resources for animations and transitions
> - Implementing and handling deep linking
> - Including Navigation UI patterns, such as navigation drawers and bottom navigation, with minimal additional work
> - [Safe Args](https://developer.android.google.cn/guide/navigation/navigation-pass-data#Safe-args) - a Gradle plugin that provides type safety when navigating and passing data between destinations



## 使用

### 环境搭建

dependencies

```

//AndroidX
dependencies {
    def nav_version = "2.1.0-alpha01"

    implementation "androidx.navigation:navigation-fragment:$nav_version" // For Kotlin use navigation-fragment-ktx
    implementation "androidx.navigation:navigation-ui:$nav_version" // For Kotlin use navigation-ui-ktx
}


//  Pre-AndroidX
dependencies {
    def nav_version = "1.0.0"

    implementation "android.arch.navigation:navigation-fragment:$nav_version" // For Kotlin use navigation-fragment-ktx
    implementation "android.arch.navigation:navigation-ui:$nav_version" // For Kotlin use navigation-ui-ktx
}
```

safeArgs

```
buildscript {
    repositories {
        google()
    }
    dependencies {
    //AndroidX
   classpath "androidx.navigation:navigation-safe-args-gradle-plugin:2.1.0-alpha01"
   // Pre-AndroidX
   classpath "android.arch.navigation:navigation-safe-args-gradle-plugin:1.0.0"
    }
}

apply plugin: "androidx.navigation.safeargs"  //java module
apply plugin: "androidx.navigation.safeargs.kotlin"  // kotlin module
```

### 创建Navi Gragh