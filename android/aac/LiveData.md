# LiveData

[`LiveData`](https://developer.android.google.cn/reference/androidx/lifecycle/LiveData.html) 是一种可观察的数据存储器类。与常规的可观察类不同，LiveData 具有生命周期感知能力，意指它遵循其他应用组件（如 Activity、Fragment 或 Service）的生命周期。这种感知能力可确保 LiveData 仅更新处于活跃生命周期状态的应用组件观察者。

## **优势**

- **确保界面符合数据状态**
- **不会发生内存泄露**
- **不会因 Activity 停止而导致崩溃**
- **不再需要手动处理生命周期**
- **数据始终保持最新状态**
- **适当的配置更改**
- **共享资源**

## 使用 LiveData 对象

1. 创建 `LiveData` 实例以存储某种类型的数据。这通常在 [`ViewModel`](https://developer.android.google.cn/reference/androidx/lifecycle/ViewModel.html) 类中完成
2. 创建可定义 [`onChanged()`](https://developer.android.google.cn/reference/androidx/lifecycle/Observer.html#onChanged(T)) 方法的 [`Observer`](https://developer.android.google.cn/reference/androidx/lifecycle/Observer.html) 对象，该方法可以控制当 `LiveData` 对象存储的数据更改时会发生什么。通常情况下，您可以在界面控制器（如 Activity 或 Fragment）中创建 `Observer` 对象。
3. 使用 [`observe()`](https://developer.android.google.cn/reference/androidx/lifecycle/LiveData.html#observe(android.arch.lifecycle.LifecycleOwner,
   android.arch.lifecycle.Observer)) 方法将 `Observer` 对象附加到 `LiveData` 对象。`observe()` 方法会采用 [`LifecycleOwner`](https://developer.android.google.cn/reference/androidx/lifecycle/LifecycleOwner.html) 对象。这样会使 `Observer` 对象订阅 `LiveData` 对象，以使其收到有关更改的通知。通常情况下，您可以在界面控制器（如 Activity 或 Fragment）中附加 `Observer` 对象。

### 创建 LiveData 对象

```
class NameViewModel : ViewModel() {

        // Create a LiveData with a String
        val currentName: MutableLiveData<String> by lazy {
            MutableLiveData<String>()
        }

        // Rest of the ViewModel...
    }
```

**注意**：请确保将用于更新界面的 `LiveData` 对象存储在 `ViewModel` 对象中，而不是将其存储在 Activity 或 Fragment 中

### 观察 LiveData 对象

```
class NameActivity : AppCompatActivity() {

        private lateinit var model: NameViewModel

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            // Other code to setup the activity...

            // Get the ViewModel.
            model = ViewModelProviders.of(this).get(NameViewModel::class.java)

            // Create the observer which updates the UI.
            val nameObserver = Observer<String> { newName ->
                // Update the UI, in this case, a TextView.
                nameTextView.text = newName
            }

            // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
            model.currentName.observe(this, nameObserver)
        }
    }
    
```

### 更新 LiveData 对象

```
button.setOnClickListener {
        val anotherName = "John Doe"
        model.currentName.setValue(anotherName)
    }
```

**注意**：您必须调用 [`setValue(T)`](https://developer.android.google.cn/reference/androidx/lifecycle/MutableLiveData.html#setValue(T)) 方法以从主线程更新 `LiveData` 对象。如果在 worker 线程中执行代码，则您可以改用 [`postValue(T)`](https://developer.android.google.cn/reference/androidx/lifecycle/MutableLiveData.html#postValue(T)) 方法来更新 `LiveData` 对象。

## 转换 LiveData

[`Lifecycle`](https://developer.android.google.cn/reference/android/arch/lifecycle/package-summary.html) 软件包会提供 [`Transformations`](https://developer.android.google.cn/reference/androidx/lifecycle/Transformations.html) 类，该类包括可应对这些情况的辅助程序方法。

```
 val userLiveData: LiveData<User> = UserLiveData()
    val userName: LiveData<String> = Transformations.map(userLiveData) {
        user -> "${user.name} ${user.lastName}"
    }
    
```

## 合并多个 LiveData 源

[`MediatorLiveData`](https://developer.android.google.cn/reference/androidx/lifecycle/MediatorLiveData.html) 是 [`LiveData`](https://developer.android.google.cn/reference/androidx/lifecycle/LiveData.html) 的子类，允许您合并多个 LiveData 源。只要任何原始的 LiveData 源对象发生更改，就会触发 `MediatorLiveData` 对象的观察者。

例如，如果界面中有可以从本地数据库或网络更新的 `LiveData` 对象，则可以向 `MediatorLiveData` 对象添加以下源：

- 与存储在数据库中的数据关联的 `LiveData` 对象。
- 与从网络访问的数据关联的 `LiveData` 对象。

您的 Activity 只需观察 `MediatorLiveData` 对象即可从这两个源接收更新。