### 引言

2018开始Andorid 已经宣布不再维护support, 最终定版在28.0.0, 而现在最新AS(Android Studio )项目新建默认都是AndroidX,甚至都不支持Android(特殊手段可以修改),这对新项目基本没有影响,但是对新建module会带来很大问题, 所以AndroidX的迁移势在必行.

### Android Support 与 AndroidX

AndroidX 对原始 Android Support库进行了重大改进，后者现在已不再维护。AndroidX 软件包完全取代了支持库，不仅提供同等的功能，而且提供了新的库。

**1.1 什么是AndroidX**

Android系统在刚刚面世的时候，可能连它的设计者也没有想到它会如此成功。随着Android系统版本不断地迭代更新，每个版本中都会加入很多新的API进去，但是新增的API在老版系统中并不存在，因此这就出现了一个向下兼容的问题。



于是Android团队推出了的Android Support Library，用于提供向下兼容的功能。比如我们熟知的support-v4库，appcompat-v7库都是属于Android Support Library的。4在这里指的是Android API版本号，对应的系统版本是1.6。support-v4的意思就是这个库中提供的API会向下兼容到Android 1.6系统。类似地，appcompat-v7指的是将库中提供的API向下兼容至API 7，也就是Android 2.1系统。



随着时间的推移，Android1.6、2.1系统早已被淘汰了，现在Android官方支持的最低系统版本已经是4.0.1，对应的API版本号是15。support-v4、appcompat-v7库也不再支持那么久远的系统了，但是它们的名字却一直保留了下来，虽然它们现在的实际作用已经对不上当初命名的原因了。



Android团队也意识到这种命名已经非常不合适了，于是对这些API的架构进行了一次重新的划分，推出了AndroidX。因此，AndroidX本质上其实就是对Android Support Library进行的一次升级。

**1.2 为什么要升级AndroidX**

- 版本 28.0.0 是Android Support 库的最后一个版本。官方将不再发布 android.support 库版本。所有新功能都将在 AndroidX命名空间中开发。
- 长远来看。AndroidX重新设计了包结构，旨在鼓励库的小型化，支持库和架构组件包的名字进行了简化。而且这也是减轻Android生态系统碎片化的有效方式。
- 与Android Support库不同，AndroidX软件包是单独维护和更新的。这些AndroidX包使用严格的语义版本控制，从版本1.0.0开始，您可以单独更新项目中的AndroidX库。

### 如何迁移

庆幸的是AS (3.2之后) 重构功能简化迁移流程

菜单栏 Refactor-- Migrate to AndroidX...

注意做好备份工作

操作后,AS会自动修改当前项目的 gradle.properties

```
android.useAndroidX=true  #启用 AndroidX
android.enableJetifier=true  #第三方依赖包AndroidX
```

### 迁移后APK差异

迁移前

![迁移前](https://gitee.com/LeonShih/Image/raw/master///android.jpg)

迁移后

![迁移后](https://gitee.com/LeonShih/Image/raw/master///androidx.jpg)



明显看到, dex包大小减少了0.3MB,类的数量减少400+,  方法数 减少5K+, 引用数减少7K+

### 参考

[官方迁移指南](https://developer.android.com/jetpack/androidx/migrate#migrate)

[干货 | 携程Android 10适配踩坑指南](https://mp.weixin.qq.com/s?__biz=MjM5MDI3MjA5MQ==&mid=2697269503&idx=2&sn=f5505724dcee64ebd9904ee16a2bfedb&scene=21#wechat_redirect)