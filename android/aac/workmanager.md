# [WorkManger](https://developer.android.com/topic/libraries/architecture/workmanager/)

### 简介

> 适合用于持久性工作的推荐解决方案。如果工作始终要通过应用重启和系统重新启动来调度，便是永久性的工作。由于大多数后台处理操作都是通过持久性工作完成的，因此 WorkManager 是适用于后台处理操作的主要推荐 API。

## 持久性工作的类型

WorkManager 可处理三种类型的永久性工作：

- **立即执行**：必须立即开始且很快就完成的任务，可以加急。
- **长时间运行**：运行时间可能较长（有可能超过 10 分钟）的任务。
- **可延期执行**：延期开始并且可以定期运行的预定任务。

大致表明了不同类型的持久性工作彼此之间的关系。

![持久性工作可以是立即执行、长时间运行或可延期执行的工作](https://developer.android.google.cn/images/guide/background/workmanager_main.svg)

同样，下表大致列出了各种工作类型。

| **类型** | **周期**     | **使用方式**                                                 |
| :------- | :----------- | :----------------------------------------------------------- |
| 立即     | 一次性       | `OneTimeWorkRequest` 和 `Worker`。如需处理加急工作，请对 OneTimeWorkRequest 调用 `setExpedited()`。 |
| 长期运行 | 一次性或定期 | 任意 `WorkRequest` 或 `Worker`。在工作器中调用 `setForeground()` 来处理通知。 |
| 可延期   | 一次性或定期 | `PeriodicWorkRequest` 和 `Worker`。                          |

如需详细了解如何设置 WorkManager，请参阅[定义您的 WorkRequest](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/define-work#work-constraints) 指南。

## 功能

除了具备更为简单且一致的 API 之外，WorkManager 还具备许多其他关键优势：

### 工作约束

使用[工作约束](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/define-work#work-constraints)明确定义工作运行的最佳条件。例如，仅在设备采用不按流量计费的网络连接时、当设备处于空闲状态或者有足够的电量时运行。

### 强大的调度

WorkManager 允许您使用灵活的调度窗口[调度工作](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/define-work)，以运行[一次性](https://developer.android.com/reference/androidx/work/OneTimeWorkRequest)或[重复](https://developer.android.com/reference/androidx/work/PeriodicWorkRequest)工作。您还可以对工作进行标记或命名，以便调度唯一的、可替换的工作以及监控或取消工作组。

已调度的工作存储在内部托管的 SQLite 数据库中，由 WorkManager 负责确保该工作持续进行，并在设备重新启动后重新调度。

此外，WorkManager 遵循[低电耗模式](https://developer.android.com/training/monitoring-device-state/doze-standby)等省电功能和最佳做法，因此您在这方面无需担心。

### 加急工作

您可以使用 WorkManager 调度需在后台立即执行的工作。您应该使用[加急工作](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/define-work#expedited)来处理对用户来说很重要且会在几分钟内完成的任务。

### 灵活的重试政策

有时工作会失败。WorkManager 提供了[灵活的重试政策](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/define-work#retries_backoff)，包括可配置的[指数退避政策](https://developer.android.com/reference/androidx/work/BackoffPolicy)。

### 工作链

对于复杂的相关工作，您可以使用直观的接口[将各个工作任务串联起来](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/chain-work)，这样您便可以控制哪些部分依序运行，哪些部分并行运行。

[Kotlin](https://developer.android.com/topic/libraries/architecture/workmanager/#kotlin)[Java](https://developer.android.com/topic/libraries/architecture/workmanager/#java)

```kotlin
val continuation = WorkManager.getInstance(context)
    .beginUniqueWork(
        Constants.IMAGE_MANIPULATION_WORK_NAME,
        ExistingWorkPolicy.REPLACE,
        OneTimeWorkRequest.from(CleanupWorker::class.java)
    ).then(OneTimeWorkRequest.from(WaterColorFilterWorker::class.java))
    .then(OneTimeWorkRequest.from(GrayScaleFilterWorker::class.java))
    .then(OneTimeWorkRequest.from(BlurEffectFilterWorker::class.java))
    .then(
        if (save) {
            workRequest<SaveImageToGalleryWorker>(tag = Constants.TAG_OUTPUT)
        } else /* upload */ {
            workRequest<UploadWorker>(tag = Constants.TAG_OUTPUT)
        }
    )
```

对于每项工作任务，您可以[定义工作的输入和输出数据](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/define-work#input_output)。将工作串联在一起时，WorkManager 会自动将输出数据从一个工作任务传递给下一个工作任务。

### 内置线程互操作性

WorkManager [无缝集成](https://developer.android.com/topic/libraries/architecture/workmanager/advanced/threading) [Coroutines](https://developer.android.com/topic/libraries/architecture/workmanager/advanced/coroutineworker) 和 [RxJava](https://developer.android.com/topic/libraries/architecture/workmanager/advanced/rxworker)，让您可以[插入自己的异步 API](https://developer.android.com/topic/libraries/architecture/workmanager/advanced/listenableworker)，非常灵活。

**注意**：尽管 Coroutines 和 WorkManager 分别是针对于不同用例推荐的解决方案，但它们二者并不是互斥的关系。您可以在通过 WorkManager 调度的工作中使用协程。

## 使用 WorkManager 保证工作可靠性

WorkManager 适用于需要**可靠运行**的工作，即使用户导航离开屏幕、退出应用或重启设备也不影响工作的执行。例如：

- 向后端服务发送日志或分析数据。
- 定期将应用数据与服务器同步。

WorkManager 不适用于那些可在应用进程结束时安全终止的进程内后台工作。它也并非对所有需要立即执行的工作都适用的通用解决方案。请查看[后台处理指南](https://developer.android.com/guide/background)，了解哪种解决方案符合您的需求。

## 与其他 API 的关系

虽然协程是适合某些用例的推荐解决方案，但您不应将其用于持久性工作。请务必注意，协程是一个并发框架，而 WorkManager 是一个持久性工作库。同样，AlarmManager 仅适合用于时钟或日历。

| **API**          | **推荐使用场景**           | **与 WorkManager 的关系**                                    |
| :--------------- | :------------------------- | :----------------------------------------------------------- |
| **Coroutines**   | 所有不需要持久的异步工作。 | 协程是在 Kotlin 中退出主线程的标准方式。不过，它们在应用关闭后会释放内存。对于持久性工作，请使用 WorkManager。 |
| **AlarmManager** | 仅限闹钟。                 | 与 WorkManager 不同，AlarmManager 会使设备从低电耗模式中唤醒。因此，它在电源和资源管理方面来讲并不高效。AlarmManager 仅适合用于精确闹钟或通知（例如日历活动）场景，而不适用于后台工作。 |





## 使用

  build.gradle 添加dependencies  

```gradle
dependencies {
    def work_version = "2.7.1"

    // (Java only)
    implementation "androidx.work:work-runtime:$work_version"

    // Kotlin + coroutines
    implementation "androidx.work:work-runtime-ktx:$work_version"

    // optional - RxJava2 support
    implementation "androidx.work:work-rxjava2:$work_version"

    // optional - GCMNetworkManager support
    implementation "androidx.work:work-gcm:$work_version"

    // optional - Test helpers
    androidTestImplementation "androidx.work:work-testing:$work_version"

    // optional - Multiprocess support
    implementation "androidx.work:work-multiprocess:$work_version"
}
```



#### code中使用

创建worker

`doWork()` 方法在 WorkManager 提供的后台线程上异步运行。

```kotlin
class UploadWorker(appContext: Context, workerParams: WorkerParameters):
       Worker(appContext, workerParams) {
   override fun doWork(): Result {

       // Do the work here--in this case, upload the images.
       uploadImages()

       // Indicate whether the work finished successfully with the Result
       return Result.success()
   }
}
```

配置执行worker并提交给系统

```kotlin
val uploadWorkRequest = OneTimeWorkRequestBuilder<UploadWorker>()
        .build()
WorkManager.getInstance().enqueue(uploadWorkRequest)
```



### 定义Workrequests

一次性任务

```kotlin
val myWorkRequest = OneTimeWorkRequest.from(MyWork::class.java)

//复杂任务使用builder构建
val uploadWorkRequest: WorkRequest =
   OneTimeWorkRequestBuilder<MyWork>()
       // Additional configuration
       .build()
```

调度加急工作

> WorkManager 2.7.0 引入了加急工作的概念。这使 WorkManager 能够执行重要工作，同时使系统能够更好地控制对资源的访问权限。

加急工作具有以下特征：

- **重要性**：加急工作适用于对用户很重要或由用户启动的任务。
- **速度**：加急工作最适合那些立即启动并在几分钟内完成的简短任务。
- **配额**：限制前台执行时间的系统级配额决定了加急作业是否可以启动。
- **电源管理**：[电源管理限制](https://developer.android.com/topic/performance/power/power-details)（如省电模式和低电耗模式）不太可能影响加急工作。
- **延迟时间**：系统立即执行加急工作，前提是系统的当前工作负载允许执行此操作。这意味着这些工作对延迟时间较为敏感，不能安排到以后执行。

**注意**：当您的应用在前台运行时，配额不会限制加急工作的执行。仅在应用在后台运行时或当应用移至后台时，执行时间配额才适用。因此，您应在后台加快要继续的工作。当应用在前台运行时，您可以继续使用 `setForeground()`。

执行加急(expedite)任务

```kotlin
val request = OneTimeWorkRequestBuilder()
    .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
    .build()

WorkManager.getInstance(context)
    .enqueue(request)
```

### 向后兼容性和前台服务

为了保持加急作业的向后兼容性，WorkManager 可能会在 Android 12 之前版本的平台上运行前台服务。前台服务可以向用户显示通知。

在 Android 12 之前，工作器中的 `getForegroundInfoAsync()` 和 `getForegroundInfo()` 方法可让 WorkManager 在您调用 `setExpedited()` 时显示通知。

如果您想要请求任务作为加急作业运行，则所有的 ListenableWorker 都必须实现 `getForegroundInfo` 方法。

**注意**：如果未能实现对应的 `getForegroundInfo` 方法，那么在旧版平台上调用 `setExpedited` 时，可能会导致运行时崩溃。

以 Android 12 或更高版本为目标平台时，前台服务仍然可通过对应的 `setForeground` 方法使用。

**注意**：`setForeground()` 可能会在 Android 12 上抛出运行时异常，并且在[启动受到限制](https://developer.android.com/guide/components/foreground-services#background-start-restrictions)时可能会抛出异常。



### 重复任务--最小时间间隔15min( [JobScheduler](https://developer.android.google.cn/reference/android/app/job/JobScheduler))

```
val constraints = Constraints.Builder()
        .setRequiresCharging(true)
        .build()

val saveRequest =
PeriodicWorkRequestBuilder<SaveImageToFileWorker>(1, TimeUnit.HOURS)
    .setConstraints(constraints)
    .build()

WorkManager.getInstance()
    .enqueue(saveRequest)
```

### 灵活的运行间隔

```kotlin
val myUploadWork = PeriodicWorkRequestBuilder<SaveImageToFileWorker>(
       1, TimeUnit.HOURS, // repeatInterval (the period cycle)
       15, TimeUnit.MINUTES) // flexInterval
    .build()
```

## 工作约束

[约束](https://developer.android.com/reference/androidx/work/Constraints)可确保将工作延迟到满足最佳条件时运行。以下约束适用于 WorkManager。

| **NetworkType**      | 约束运行工作所需的[网络类型](https://developer.android.com/reference/androidx/work/NetworkType)。例如 Wi-Fi (`UNMETERED`)。 |
| -------------------- | ------------------------------------------------------------ |
| **BatteryNotLow**    | 如果设置为 true，那么当设备处于“电量不足模式”时，工作不会运行。 |
| **RequiresCharging** | 如果设置为 true，那么工作只能在设备充电时运行。              |
| **DeviceIdle**       | 如果设置为 true，则要求用户的设备必须处于空闲状态，才能运行工作。在运行批量操作时，此约束会非常有用；若是不用此约束，批量操作可能会降低用户设备上正在积极运行的其他应用的性能。 |
| **StorageNotLow**    | 如果设置为 true，那么当用户设备上的存储空间不足时，工作不会运行。 |

```kotlin
val constraints = Constraints.Builder()
   .setRequiredNetworkType(NetworkType.UNMETERED)
   .setRequiresCharging(true)
   .build()

val myWorkRequest: WorkRequest =
   OneTimeWorkRequestBuilder<MyWork>()
       .setConstraints(constraints)
       .build()

```

如果指定了多个约束，工作将仅在满足所有约束时才会运行。

```

```

如果在工作运行时不再满足某个约束，WorkManager 将停止工作器。系统将在满足所有约束后重试工作。

## 延迟工作

```kotlin
val myWorkRequest = OneTimeWorkRequestBuilder<MyWork>()
   .setInitialDelay(10, TimeUnit.MINUTES)
   .build()
```

## 重试和退避政策

如果您需要让 WorkManager 重试工作，可以从工作器返回 `Result.retry()`。然后，系统将根据[退避延迟时间](https://developer.android.com/reference/androidx/work/WorkRequest#DEFAULT_BACKOFF_DELAY_MILLIS)和[退避政策](https://developer.android.com/reference/androidx/work/BackoffPolicy)重新调度工作。

- 退避延迟时间指定了首次尝试后重试工作前的最短等待时间。此值不能超过 10 秒（或 [MIN_BACKOFF_MILLIS](https://developer.android.com/reference/androidx/work/WorkRequest#MIN_BACKOFF_MILLIS)）。
- 退避政策定义了在后续重试过程中，退避延迟时间随时间以怎样的方式增长。WorkManager 支持 2 个退避政策，即 `LINEAR` 和 `EXPONENTIAL`。

每个工作请求都有退避政策和退避延迟时间。默认政策是 `EXPONENTIAL`，延迟时间为 10 秒，但您可以在工作请求配置中替换此设置。

以下是自定义退避延迟时间和政策的示例。

```kotlin
val myWorkRequest = OneTimeWorkRequestBuilder<MyWork>()
   .setBackoffCriteria(
       BackoffPolicy.LINEAR,
       OneTimeWorkRequest.MIN_BACKOFF_MILLIS,
       TimeUnit.MILLISECONDS)
   .build()
```



## 标记工作

```kotlin
val myWorkRequest = OneTimeWorkRequestBuilder<MyWork>()
   .addTag("cleanup")
   .build()

//获取
WorkManager.getWorkInfosByTag(String)
//取消
WorkManager.cancelAllWorkByTag(String)
```

## 分配输入数据

```kotlin
// Define the Worker requiring input
class UploadWork(appContext: Context, workerParams: WorkerParameters)
   : Worker(appContext, workerParams) {

   override fun doWork(): Result {
       val imageUriInput =
           inputData.getString("IMAGE_URI") ?: return Result.failure()

       uploadFile(imageUriInput)
       return Result.success()
   }
   ...
}

// Create a WorkRequest for your Worker and sending it input
val myUploadWork = OneTimeWorkRequestBuilder<UploadWork>()
   .setInputData(workDataOf(
       "IMAGE_URI" to "http://..."
   ))
   .build()

```

## 唯一工作

唯一工作是一个很实用的概念，可确保同一时刻只有一个具有特定名称的工作实例。

- [`WorkManager.enqueueUniqueWork()`](https://developer.android.com/reference/androidx/work/WorkManager#enqueueUniqueWork(java.lang.String, androidx.work.ExistingWorkPolicy, androidx.work.OneTimeWorkRequest))（用于一次性工作）
- [`WorkManager.enqueueUniquePeriodicWork()`](https://developer.android.com/reference/androidx/work/WorkManager#enqueueUniquePeriodicWork(java.lang.String, androidx.work.ExistingPeriodicWorkPolicy, androidx.work.PeriodicWorkRequest))（用于定期工作）

```kotlin
val sendLogsWorkRequest =
       PeriodicWorkRequestBuilder<SendLogsWorker>(24, TimeUnit.HOURS)
           .setConstraints(Constraints.Builder()
               .setRequiresCharging(true)
               .build()
            )
           .build()
WorkManager.getInstance(this).enqueueUniquePeriodicWork(
           "sendLogs",
           ExistingPeriodicWorkPolicy.KEEP,
           sendLogsWorkRequest
)

```

现在，如果上述代码在 sendLogs 作业已处于队列中的情况下运行，系统会保留现有的作业，并且不会添加新的作业。

### 冲突解决政策

调度唯一工作时，您必须告知 WorkManager 在发生冲突时要执行的操作。您可以通过在将工作加入队列时传递一个枚举来实现此目的。

对于一次性工作，您需要提供一个 [`ExistingWorkPolicy`](https://developer.android.com/reference/androidx/work/ExistingWorkPolicy)，它支持用于处理冲突的 4 个选项。

- [`REPLACE`](https://developer.android.com/reference/androidx/work/ExistingWorkPolicy#REPLACE)：用新工作替换现有工作。此选项将取消现有工作。
- [`KEEP`](https://developer.android.com/reference/androidx/work/ExistingWorkPolicy#KEEP)：保留现有工作，并忽略新工作。
- [`APPEND`](https://developer.android.com/reference/androidx/work/ExistingWorkPolicy#APPEND)：将新工作附加到现有工作的末尾。此政策将导致您的新工作[链接](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/chain-work)到现有工作，在现有工作完成后运行。

## 观察您的工作

在将工作加入队列后，您可以随时按其 `name`、`id` 或与其关联的 `tag` 在 WorkManager 中进行查询，以检查其状态。

[Kotlin](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/managing-work#kotlin)[Java](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/managing-work#java)

```kotlin
// by id
workManager.getWorkInfoById(syncWorker.id) // ListenableFuture<WorkInfo>

// by name
workManager.getWorkInfosForUniqueWork("sync") // ListenableFuture<List<WorkInfo>>

// by tag
workManager.getWorkInfosByTag("syncTag") // ListenableFuture<List<WorkInfo>>
```

该查询会返回 [`WorkInfo`](https://developer.android.com/reference/androidx/work/WorkInfo) 对象的 [`ListenableFuture`](https://guava.dev/releases/23.1-android/api/docs/com/google/common/util/concurrent/ListenableFuture.html)，该值包含工作的 [`id`](https://developer.android.com/reference/androidx/work/WorkInfo#getId())、其标记、其当前的 [`State`](https://developer.android.com/reference/androidx/work/WorkInfo.State) 以及通过 [`Result.success(outputData)`](https://developer.android.com/reference/androidx/work/ListenableWorker.Result#success(androidx.work.Data)) 设置的任何输出数据。

利用每个方法的 [`LiveData`](https://developer.android.com/topic/libraries/architecture/livedata) 变种，您可以通过注册监听器来观察 `WorkInfo` 的变化。例如，如果您想要在某项工作成功完成后向用户显示消息，您可以进行如下设置：

[Kotlin](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/managing-work#kotlin)[Java](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/managing-work#java)

```kotlin
workManager.getWorkInfoByIdLiveData(syncWorker.id)
               .observe(viewLifecycleOwner) { workInfo ->
   if(workInfo?.state == WorkInfo.State.SUCCEEDED) {
       Snackbar.make(requireView(),
      R.string.work_completed, Snackbar.LENGTH_SHORT)
           .show()
   }
}
```

### 复杂的工作查询

WorkManager 2.4.0 及更高版本支持使用 [`WorkQuery`](https://developer.android.com/reference/androidx/work/WorkQuery) 对象对已加入队列的作业进行复杂查询。WorkQuery 支持按工作的标记、状态和唯一工作名称的组合进行查询。

以下示例说明了如何查找带有“syncTag”标记、处于 `FAILED` 或 `CANCELLED` 状态，且唯一工作名称为“preProcess”或“sync”的所有工作。

```kotlin
val workQuery = WorkQuery.Builder
       .fromTags(listOf("syncTag"))
       .addStates(listOf(WorkInfo.State.FAILED, WorkInfo.State.CANCELLED))
       .addUniqueWorkNames(listOf("preProcess", "sync")
    )
   .build()

val workInfos: ListenableFuture<List<WorkInfo>> = workManager.getWorkInfos(workQuery)
```

`WorkQuery` 也适用于等效的 LiveData 方法 [`getWorkInfosLiveData()`](https://developer.android.com/reference/androidx/work/WorkManager#getWorkInfosLiveData(androidx.work.WorkQuery))。

## 取消和停止工作

如果您不再需要运行先前加入队列的工作，则可以要求将其取消。您可以按工作的 `name`、`id` 或与其关联的 `tag` 取消工作。

```kotlin
// by id
workManager.cancelWorkById(syncWorker.id)

// by name
workManager.cancelUniqueWork("sync")

// by tag
workManager.cancelAllWorkByTag("syncTag")
```

WorkManager 会在后台检查工作的 [`State`](https://developer.android.com/reference/androidx/work/WorkInfo.State)。如果工作已经[完成](https://developer.android.com/reference/androidx/work/WorkInfo.State#isFinished())，系统不会执行任何操作。否则，工作的状态会更改为 [`CANCELLED`](https://developer.android.com/reference/androidx/work/WorkInfo.State#CANCELLED)，之后就不会运行这个工作。任何[依赖于此工作](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/chain-work)的 [`WorkRequest`](https://developer.android.com/reference/androidx/work/WorkRequest) 作业也将变为 `CANCELLED`。

目前，[`RUNNING`](https://developer.android.com/reference/androidx/work/WorkInfo.State#RUNNING) 可收到对 [`ListenableWorker.onStopped()`](https://developer.android.com/reference/androidx/work/ListenableWorker#onStopped()) 的调用。如需执行任何清理操作，请替换此方法。如需了解详情，请参阅[停止正在运行的工作器](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/managing-work#stop-worker)。

**注意**：[`cancelAllWorkByTag(String)`](https://developer.android.com/reference/androidx/work/WorkManager#cancelAllWorkByTag(java.lang.String)) 会取消具有给定标记的所有工作。



## 停止正在运行的工作器

正在运行的 `Worker` 可能会由于以下几种原因而停止运行：

- 您明确要求取消它（例如，通过调用 `WorkManager.cancelWorkById(UUID)` 取消）。
- 如果是[唯一工作](https://developer.android.com/topic/libraries/architecture/workmanager/how-to/managing-work#unique-work)，您明确地将 [`ExistingWorkPolicy`](https://developer.android.com/reference/androidx/work/ExistingWorkPolicy) 为 [`REPLACE`](https://developer.android.com/reference/androidx/work/ExistingWorkPolicy#REPLACE) 的新 `WorkRequest` 加入到了队列中。旧的 `WorkRequest` 会立即被视为已取消。
- 您的工作约束条件已不再满足。
- 系统出于某种原因指示您的应用停止工作。如果超过 10 分钟的执行期限，可能会发生这种情况。该工作会调度为在稍后重试。

在这些情况下，您的工作器会停止。

您应该合作地取消正在进行的任何工作，并释放您的工作器保留的所有资源。例如，此时应该关闭所打开的数据库和文件句柄。有两种机制可让您了解工作器何时停止。

### onStopped() 回调

在您的工作器停止后，WorkManager 会立即调用 [`ListenableWorker.onStopped()`](https://developer.android.com/reference/androidx/work/ListenableWorker#onStopped())。替换此方法可关闭您可能保留的所有资源。

#### isStopped() 属性

您可以调用 [`ListenableWorker.isStopped()`](https://developer.android.com/reference/androidx/work/ListenableWorker#isStopped()) 方法以检查工作器是否已停止。如果您在工作器执行长时间运行的操作或重复操作，您应经常检查此属性，并尽快将其用作停止工作的信号。

**注意**：WorkManager 会忽略已收到 onStop 信号的工作器所设置的 [`Result`](https://developer.android.com/reference/androidx/work/ListenableWorker.Result)，因为工作器已被视为停止。