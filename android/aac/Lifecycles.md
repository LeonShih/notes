# Lifecycles

生命周期感知型组件可执行操作来响应另一个组件（如 Activity 和 Fragment）的生命周期状态的变化。这些组件有助于您写出更有条理且往往更精简的代码，这样的代码更易于维护。

![lifecycles](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/lifecycles.png)

appcompat包含基本的[lifecycles](https://developer.android.google.cn/topic/libraries/architecture/lifecycle)依赖

*Support Library 26.1.0 及更高版本中的包含*

```
class StateListener :LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun  onResume(){
        Log.d("StateListener","resume")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun  onPause(){
        Log.d("StateListener","pause")
    }
}
```

Activity中增加监听

```
override fun onCreate(savedInstanceState: Bundle?) {
   ...
   lifecycle.addObserver(StateListener())
}
```

