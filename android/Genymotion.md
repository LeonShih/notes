## 安装

[Genymotion官网](https://www.genymotion.com/ ) 下载genymotion ,同时需要注册号帐号,建议使用带VirtulBox的安装包, 也可不带,目前3.1.1兼容VirtulBox 6.1.x

![image-20200908205543077](https://gitee.com/LeonShih/Image/raw/master/image-20200908205543077.png)

下载后,直接默认,一路下一步即可

安装完成后, 登录帐号,选择个人使用

首先得选择安装虚拟手机的镜像

![image-20200908205920858](https://gitee.com/LeonShih/Image/raw/master/image-20200908205920858.png)



![image-20200908210037618](https://gitee.com/LeonShih/Image/raw/master/image-20200908210037618.png)

选择后会自动下载镜像, 需要加速,否则下载很慢

![image-20200908210242904](https://gitee.com/LeonShih/Image/raw/master/image-20200908210242904.png)

手动下载参考 [genymotion 镜像快速下载](https://blog.csdn.net/liang_duo_yu/article/details/70800107



安装完成后点击启动即可

## 配置

### 安装只有arm架构的so库,的包,会提示错误

![image-20200908212231316](https://gitee.com/LeonShih/Image/raw/master/image-20200908212231316.png)

需要手动安装 指定版本的 [ARM-translation](https://leon.lanzous.com/b0d8dk1zg)

![image-20200908212657242](https://gitee.com/LeonShih/Image/raw/master/image-20200908212657242.png)



安装完成后,重启,即可安装只有arm so库的apk.



### xposed框架 (支持到 sdk27, Oreo 8.1),按需要安装

安装XposedInstaller_3.1.5.apk , 可通过APP下载 xposed框架

![image-20200908213042790](https://gitee.com/LeonShih/Image/raw/master/image-20200908213042790.png)

下载很慢,建议[手动下载](https://dl-xda.xposed.info/framework/sdk26/)后拖动安装, 安装后,手动重启模拟器

然后就可以安装自己喜欢的xposed插件了



