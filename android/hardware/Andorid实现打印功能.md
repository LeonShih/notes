# Android 实现打印机功能

### 方案一 HP 官方插件 HP Print Service 插件

[下载]: https://fir.im/6z38

安装后, 通过调用以下方法即可打印

```
 /**
     * 惠普打印插件 打印
     *
     * @param context
     * @param filePath
     */
    public void printFileHp(Context context, String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            Intent intent = new Intent("com.hp.android.printservice.TESTPRINT");
            context.startActivity(intent);
            return;
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        Uri data = Uri.fromFile(new File(filePath));
        ComponentName comp = null;
        if (filePath.endsWith("pdf")) {
            intent.setDataAndType(data, "application/pdf");
        } else if (filePath.endsWith("jpg") || filePath.endsWith("jpeg")) {
            intent.setDataAndType(data, "image/jpeg");
        } else if (filePath.endsWith("png")) {
            intent.setDataAndType(data, "image/png");
        }
        comp = new ComponentName("com.hp.android.printservice", "com.hp.android.printservice.common.TermsActivity");
        intent.setComponent(comp);

        context.startActivity(intent);
    }
```

**优点:**

- 实现简单
- 打印完成后自动退出打印

**缺点:** 

- ​       只支持pdf 和图片格式
- ​       不支持USB连接

### 方案二 PrinterShare 插件

[下载]: https://fir.im/rjk6

安装后, 通过调用以下方法即可打印

```
/**
     * printershare 软件打印
     *
     * @param context
     * @param filePath
     */
    public void printFile(Context context, String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);

        Uri data;
        if (filePath.startsWith("http")) {
            data = Uri.parse(filePath);
        } else {
            data = Uri.fromFile(new File(filePath));
        }

        Leon.d(data.toString());
        ComponentName comp = null;
        if (filePath.endsWith("pdf")) {
            intent.setDataAndType(data, "application/pdf");
            comp = new ComponentName("com.dynamixsoftware.printershare", "com.dynamixsoftware.printershare.ActivityPrintPDF");
        } else if (filePath.endsWith("doc") || filePath.endsWith("docx") || filePath.endsWith("txt")) {
            intent.setDataAndType(data, "application/doc");
            comp = new ComponentName("com.dynamixsoftware.printershare", "com.dynamixsoftware.printershare.ActivityPrintDocuments");
        } else if (filePath.endsWith("jpg") || filePath.endsWith("jpeg") || filePath.endsWith("gif") || filePath.endsWith("png")) {
            intent.setDataAndType(data, "image/jpeg");
            comp = new ComponentName("com.dynamixsoftware.printershare", "com.dynamixsoftware.printershare.ActivityPrintPictures");
        } else if (filePath.endsWith("html") || filePath.endsWith("htm") || filePath.startsWith("http")) {
            intent.setDataAndType(data, "text/html");
            comp = new ComponentName("com.dynamixsoftware.printershare", "com.dynamixsoftware.printershare.ActivityWeb");
        }

        intent.setComponent(comp);

        context.startActivity(intent);
    }
```

**优点**:

- 可打印多种格式,网页,dox, ppt, xls 
- 支持USB , Wifi,蓝牙等连接
- 支持多种打印机

**缺点**:

- 打印完成后,软件无法自动退出



### 方案三  静默打印

类似Linux系统, 连接打印接后,会生成/dev/bus/lp0 节点, 要打印的文件copy到该目录下即可实现打印

```bash
copy  file.pdf /dev/bus/lp0
#或者
echo "hello" >/dev/bus/lp0
```

 

**优点:**

- 实现简单
- 无预览页面,用户无感

**缺点:** 

- 需要系统级支持,固件厂商修改
- 文件只支持PDF格式
- 需要给PDF文档嵌入支持中文的字体

参考

https://blog.csdn.net/wyl_tyrael/article/details/85702310
https://blog.csdn.net/qq_35241234/article/details/86685742



### 总结

目前采用**第二方案**, **PrinterShare 插件**进行打印

需要额外处理以下问题:

- 打印后无法自动返回
- 返回首页按钮 遮挡打印按钮



以上两个问题,测试demo已解决.