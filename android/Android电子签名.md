

### 电子签名

> 电子签名是指[数据电文](https://baike.baidu.com/item/数据电文)中以电子形式所含、所附用于识别签名人身份并表明签名人认可其中内容的数据。通俗点说，电子签名就是通过**密码技术**对[电子文档](https://baike.baidu.com/item/电子文档/3045210)的电子形式的签名，**并非是书面签名的数字图像化**，它类似于手写[签名](https://baike.baidu.com/item/签名/2890277)或印章，也可以说它就是电子印章。

### 电子签名法

根据《电子签名法》的相关规定，可靠的电子签名具有与手写签名或盖章同等的法律效力。
又有《电子签名法》第十三条：电子签名同时符合下列条件的，视为**可靠的电子签名**：

（一）电子签名制作数据用于电子签名时，属于电子签名人专有；
（二）**签署时电子签名制作数据仅由电子签名人控制**；
（三）**签署后对电子签名的任何改动能够被发现**；
（四）**签署后对数据电文内容和形式的任何改动能够被发现**。



:warning:word文档可以修改,无法满足上述条件, 只有**pdf文档**满足



### 技术实现细节

- 手写签名实现及保存  (已完成)

- pdf文档预览                (已完成)              

- 手写签名移动到pdf文档位置, 计算签名位置  (已完成)

- pdf 图章签名                (已完成)

- bks签名文件生成         (已完成)

- 指纹读取 , 需要硬件支持    (已完成)

- 签名与指纹合并             (已完成)

  

### 最终实现效果



![sig2](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/sig22.gif)



### 签名pdf打开效果

**整体显示效果**

![sig_full_01](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/sig_full_1.jpg)

**放大显示效果**

![sig_zoom_01](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/sig_zoom_01.jpg)



**查看文件签名(未被修改)**

![sig_validate](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/sig_validate.jpg)



**压缩pdf文档后,签名无效**

![image-20200716164754585](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/image-20200716164754585.png)



### 参考

[AndroidPdfViewer](https://github.com/barteksc/AndroidPdfViewer)

[Google 官方 KeyStore 签名支持](https://developer.android.google.cn/reference/java/security/KeyStore)

[生成android使用的BKS证书](https://www.xuebuyuan.com/2223210.html)

