获取系统异常日志,针对未root设备

```
adb bugreport > report.zip 
```



本地开启VPN后，GIt也需要设置代理，才能正常略过GFW，访问goole code等网站

设置如下（可复制）：

```
git config --global https.proxy http://127.0.0.1:1080

 git config --global https.proxy https://127.0.0.1:1080

git config --global http.proxy 'socks5://127.0.0.1:1080' 

git config --global https.proxy 'socks5://127.0.0.1:1080'
```

 

取消

```
git config --global --unset http.proxy

git config --global --unset https.proxy
```

