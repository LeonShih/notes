# Cmake

## 特点

- 每个源码目录下都有一个CmakeLists.txt
- 语句不区分大小写  一句一行,无行结束 注释#
- Cmake根据CmakeLists.txt自动生成Makefile

## 语法

语法规则

- ${} 变量取值, 但IF语句是直接使用变量

- 指令 (参数1 参数2 ... )    参数之间用空格或分号隔开

- 内部和外部构建 (哪里执行cmake,哪里构建)

  -  in-source   源码和编译过程文件同一目录
  - out-of-source 源码和编译过程文件不同目录

- 常用变量及指令

  变量

  | 语句                                                         | 注释                                                         |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | PROJECT_BINARY_DIR  PROJECT_SOURCE_DIR<br/>CMAKE_BINARY_DIR  CMAKE_SOURCE_DIR | 工程目标文件目录<br/>工程源文件目录                          |
  | CMAKE_CURRENT_BINARY_DIR <br/>CMAKE_CURRENT_SOURCE_DIR       | CmakeLists.txt 路径                                          |
  | CMAKE_CURRENT_LIST_FILE <br/>CMAKE_CURRENT_LIST_LINE         | 调用这个变量的CmakeLists.txt 路径及行号                      |
  | <project name>_BINARY_DIR <br/><project name>_SOURCE_DIR     | project name目标文件目录<br/>project name源文件目录          |
  | EXECUTABLE_OUTPUT_PATH                                       | 可执行文件输出路径                                           |
  | LIBRARRY_OUT_PATH                                            | lib输出路径                                                  |
  | CMAKE_INSTALL_PREFIX                                         | 安装目录                                                     |
  | CMAKE_MODULE_PATH                                            | Cmake 模块所在的目录                                         |
  | PROJECT_NAME                                                 | 通过PROJECT定义的值                                          |
  | CMAKE_INCLUDE_CURRENT_DIR                                    | 自动添加<br/>CMAKE_CURRENT_BINARY_DIR <br/>CMAKE_CURRENT_SOURCE_DIR |
  | CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE                     | 头文件目录始终置于系统文件目录前                             |
  | CMAKE_MAJOR_VERSION <br/>CMAKE_MINOR_VERSION <br/>CMAKE_PATCH_VERSION <br/> | 主版本,次版本 补丁                                           |
  | CMAKE_SYSTEM<br/>CMAKE_SYSTEM_NAME <br/>CMAKE_SYSTEM_VERSION <br/>CMAKE_SYSTEM_PROCESSOR | 系统名称 如Linux-2.6.26<br>Linux<br>2.6.26<br>I386           |
  | UNIX<br>WIN32                                                | 所有类Unix平台值为TRUE<br>所有WIN32平台值为TRUE              |
  | CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS                            | 开关选项,控制 if else 书写方式                               |
  | BUILD_SHARED_LIBS                                            | 开关,默认静态库                                              |
  | CMAKE_C_FLAGS<br>CMAKE_CXX_FLAGS                             | 设置C编译选项<br>设置C++编译选项                             |
  
  CMAKE 指令

  | 语句                                                         | 注释                                                         |
  | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | PROJECT (project name[CXX]\[C\][Java])                       | 定义工程名称,包含了<br><project name>_BINARY_DIR <br/><project name>_SOURCE_DIR |
  | SET(var [value] [cache type docstring[force]])               | 自定义变量指令<br>set(SRC_LIST main.cpp hello.cpp)<br/>set(SRC_LIST "main.cpp" "hello.cpp") |
  | MESSAGE ([SEND_ERROR\|STATUS\|FATAL_ERROR])<br> "message to display" ...) | SEND_ERROR:产生错误,跳过生成<br>STATUS:输出前缀为---的信息<br>FATAL_ERROR:终止所有Cmake过程 |
  | ADD_EXECUTABLE (target source_file ...)                      | 增加可执行目标文件,target由source_file生成                   |
  | ADD_SUBDIRECTORY(source_dir [binary_dir]<br> [EXCULED FROM ALL]) | 增加子目录                                                   |
  | SUBDIRS(dir1 dir2 ...)                                       | 一次添加多个目录,及时外部编译                                |
  | INSTALL (TARGETS targets[<br>[ARCHIEVE\|LIBRARY\|RUNTIME]<br>[DESTINATION <dir>]<br>[PERMISSIONS permissions...]<br>[CONFIGGURAIONS [DEBUG\|RELEASE\|...]]<br>[COMPONENT <component>]<br>[OPTIONAL]<br>\][...]) | 安装目标文件<br>ARCHIEVE   静态库文件<br>LIBRARY  动态库文件<br>RUNTIME 可执行文件<br>DESTINATION  定义安装路径  绝对路径腐败CMAKE_INSTALL_PREFIX |
  | INSTALL (FILES files[<br>[DESTINATION <dir>]<br/>[PERMISSIONS permissions...]<br/>[CONFIGGURAIONS [DEBUG\|RELEASE\|...]]<br>[COMPONENT <component>]<br/>[OPTIONAL]<br/>\][...]) | 安装普通文件<br>可指定权限,默认644                           |
  | INSTALL (DIRECTORY dirs[<br/>[DESTINATION <dir>]<br/>[FILE_PERMISSIONS permissions...]<br/>[DIRECOTRY_PERMISSIONS permissions...]<br/>[USE_SOURCE_PERMISSIONS permissions...]<br/>[COMPONENT <component>]<br/>[OPTIONAL]<br/>\][...]) | 安装目录,可指定权限,默认644                                  |
  | ADD_LIBRARY(libname [SHARED\|STATIC\|MODULE] [EXCLUDE_FROM_ALL] src1 src2 ... srcN) | 添加依赖库                                                   |
  | SET_TARGET_PROPERTIES( target1 target2 ..  PROPERTIES prp1 value prop2 value2) | 设置目前输出的名字及属性                                     |
  | GET_TARGET_PROPERTIES(var target property)                   | 获取目标属性                                                 |
  | $ENV{NAME}                                                   | 调用系统环境变量                                             |
  | SET(ENV{变量名} 值)                                          | 设置环境变量值                                               |
  | ADD_DEFINITIONS<br>ADD_DEFINITIONS(-DENABLE_DEBUG)           | 向编译器添加-D定义                                           |
  | ADD_DEPENDENCIES (target_name depend_target1 denpend_target) | 定义target依赖其他target                                     |
  | ADD_TEST(testname program arg1 arg2)                         | 在打开ENABLE_TESTING后有效                                   |
  | ENABLE_TESTING                                               | 控制makefile是否构建test目标,一般在project 主CmakeLists.txt中 |
  | AUX_SOURCE_DIRECTORY(dir Variable)<br>AUX_SOURCE_DIRECTORY(. SRC_LIST) | 自动构建源文件列表                                           |
  | CMAKE_MINIMUM_REQUIRED(VERSION verson_num[fatal_error])      | 检查Cmake版本,不满足产生错误或退出                           |
  | EXEX_PROGRAM(program [ARGS args]<br>[OUTPUT_VARIABLE var]<br>[RETURN_VALUE value ]) | 执行程序                                                     |
  | FILE<br>FILE(WRITE  filename "msg" ..)<br>FILE(APPEND filename "msg" ..)<br>FILE(READ filename)<br>FILE(REMOVE\|REMOVE_RECURSE dir..)<br>FILE(MAKE_DIRECTORY dir..)<br/> | 文件操作                                                     |
  | INCLUDE(file [OPTIONAL])<br>INCLUDE(module [OPTIONAL])       | 引入CmakeLists.txt或者Cmake模块                              |
  | FIND_FILE(<VAR> name1 path1 path2 ...)<br>FIND_LIBRARY(<VAR> name1 path1 path2 ...)<br/>FIND_PATH(<VAR> name1 path1 path2 ...)<br/>FIND_PROGRAM(<VAR> name1 path1 path2 ...)<br/>FIND_PACKAGE(<name> [major.minor] [QUITE]<br>\[NO_MODULE]\[\[REQUIRED\|COMPONENTS][components ...] ])<br/> | name1 文件全路径                                             |
  
  ### 条件语句
  
  > IF(expression)
  >
  > \# THEN section
  >
  > command1(args ...)
  >
  > command2(args ...)
  >
  > ...
  >
  > ELSE(expression)
  >
  > \# else section
  >
  > command1(args ...)
  >
  > command2(args ...)
  >
  > ...
  >
  > ENDIF(expression)
  
  ```
  IF(var)   # 非 空 null , N  NO ,OFF FALSE  NOTFOUND或 <var>_NOTFOUND
  IF(var1 AND var2)
  IF(var1 OR var2)
  IF(COMMAND cmd)  #cmd 确实是命令 且可调用
  IF(EXISTS dir|file)  文件或目录存在
  IF(file1  IS_NEWER_THAN  file2)
  IF(IS_DIRECTORY dirname)
  IF(variable MATCHES regex)
  IF(string MATCHES regex)
  IF(string|variable LESS|GREATER|EQUAL|STREQUAL  number)
  IF(DEFINED variable)
  ```
  
  ```
  IF(WIN32)
  MESSAGE(STATUS "windows")
  ELSE(WIN32)
  MESSAGE(STATUS " not windows")
  ENDIF(WIN32)
  
  #简写
  SET(CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS ON )
  IF(WIN32)
  MESSAGE(STATUS "windows")
  ELSE()
  MESSAGE(STATUS " not windows")
  ENDIF()
  ```
  
  
  
  ### 循环语句

> WHILE(condition)
>
> COMMAND1(ARGS...)
>
> COMMAND2(ARGS...)
>
> ...
>
> ENDWHILE(condition)

> 列表
>
> FOREACH(loop_var arg1 arg2 ..)
>
> COMMAND1(ARGS...)
>
> COMMAND2(ARGS...)
>
> ENDFOREACH(loop_var)
>
> 
>
> 范围
>
> FOREACH(loop_var RANGE total) #FOREACH(loop_var RANGE start stop [step])
>
> COMMAND1(ARGS...)
>
> COMMAND2(ARGS...)
>
> ENDFOREACH(loop_var)





```
AUX_SOURCE_DIRECTORY(. SRC_LIST)
FOREACH(F ${SRC_LIST})
MESSAGE(${F})
ENDFOREACH(F)
```

```
FOREACH(VAR RANGE 10)  #  FOREACH(VAR RANGE 5 15 3)
MESSAGE(${VAR})
ENDFOREACH(VAR)
#  0  1 2 3 4 5 6 7 8 9 10    # 5 8 11 14 
```

