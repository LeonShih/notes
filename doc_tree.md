文件夹 PATH 列表
E:\GITREPO\NOTES
│  doc_tree.md
│  LICENSE
│  pull.bat
│  pull.sh
│  up.bat
│  up.sh
│  
├─android
│  │  activity_lifecycle.png
│  │  Android 打开小程序.md
│  │  Android.md
│  │  Android电子签名.md
│  │  AspectJ in Andoird.md
│  │  fragmeng_lifecycle.png
│  │  Genymotion.md
│  │  progurad.md
│  │  RxJava use case.md
│  │  其他.md
│  │  后台保活白色.md
│  │  签名证书keystore jks pem x509.pem .md
│  │  网络检测.md
│  │  
│  ├─aac
│  │      AndroidX迁移.md
│  │      DataBinding.md
│  │      JetPack.md
│  │      Lifecycles.md
│  │      LiveData.md
│  │      navigation.md
│  │      room.md
│  │      viewmodel.md
│  │      workmanager.md
│  │      
│  ├─hardware
│  │      Andorid实现打印功能.md
│  │      
│  ├─interview
│  │      Android开源框架面试题集(含答案).md
│  │      Android网络编程面试题集(含答案).md
│  │      Android面试题集(含答案).md
│  │      Java面试题集(含答案).md
│  │      《Android面试题精编解析大全》.pdf
│  │      数据结构与算法面试题集(含答案).md
│  │      
│  ├─reverse
│  │      android 逆向.md
│  │      
│  ├─rom
│  │      开机动画修改.md
│  │      
│  └─source
│          init进程启动.md
│          源码阅读.md
│          
├─c c++
│      cmake.md
│      
├─English
│      1.vinc=vict 胜,征服.xmind
│      2 .form 形式，格式，形成.xmind
│      构词法.md
│      
├─flutter
│      flutter.md
│      
├─h5
│      css_1_知识点.doc
│      css_2_知识点.doc
│      css_3_知识点.doc
│      css_4_知识点.doc
│      css_5_知识点.doc
│      CSS基础.md
│      HTML 基础.md
│      html_1_知识点.doc
│      html_2_知识点.doc
│      JavaScript基础.md
│      「查漏补缺」HTML  与  CSS  进阶.md
│      
├─java
│  │  ConflictingBeanDefinitionException.md
│  │  IDEA.md
│  │  SpringBoot.md
│  │  Windows 10 子系统.md
│  │  服务器搭建.md
│  │  
│  ├─itheima
│  │  │  基础加强Junit_Reflect_Annotation.md
│  │  │  
│  │  ├─html+css+javascript+ xml
│  │  │      BootStrap笔记.md
│  │  │      DOM树.bmp
│  │  │      HTML&CSS课堂笔记.md
│  │  │      JavaScript基础笔记.md
│  │  │      JavaScript高级笔记.md
│  │  │      w3cschool菜鸟教程.CHM
│  │  │      xml笔记.md
│  │  │      特殊字符表.png
│  │  │      
│  │  ├─mysql
│  │  │      JDBC笔记.pdf
│  │  │      JDBC连接池&JDBCTemplate课堂笔记.md
│  │  │      MySQL基础.pdf
│  │  │      MySQL多表查询与事务的操作.pdf
│  │  │      MySQL约束与设计.pdf
│  │  │      数据库连接池.bmp
│  │  │      练习.sql
│  │  │      
│  │  └─servlet
│  │          Cookie_Session笔记.md
│  │          HttpServlet.bmp
│  │          HTTP协议.bmp
│  │          Referer请求头.bmp
│  │          request&response对象原理.bmp
│  │          Request请求转发&域对象.bmp
│  │          Response笔记.md
│  │          Servlet&HTTP&Request笔记.md
│  │          Servlet.bmp
│  │          Servlet执行原理.bmp
│  │          Tomcat&Servlet笔记.md
│  │          tomcat目录结构.png
│  │          登录案例分析.bmp
│  │          资源分类.bmp
│  │          
│  └─linux
│          Linux系统编程.pdf
│          
├─other
│  │  18位社会信用代码.md
│  │  18位身份证说明.md
│  │  Android Nexus 私服搭建.md
│  │  git commit规范.md
│  │  github加速.md
│  │  gitlab删除大文件.md
│  │  git使用规范_2.0.md
│  │  git问题汇总.md
│  │  Kotlin Scope function.md
│  │  Maven Reposity.md
│  │  Open Source License.md
│  │  Sublime.md
│  │  Typora + PicGo + Github + jsdelivr个人笔记系统搭建.md
│  │  Windows Scoop安装.md
│  │  正则表达式.txt
│  │  编码与解码.md
│  │  裁判文书App 破解.md
│  │  黑马git教案-v2.0.docx
│  │  
│  ├─OpenGL 知识点
│  │      OpenGL知识点总结.md
│  │      
│  └─RxMarbles
│          RxMarbles_combining.md
│          RxMarbles_conditional.md
│          RxMarbles_filter.md
│          RxMarbles_math_error.md
│          RxMarbles_transform.md
│          
├─shell
│      bash 进阶.md
│      bash.md
│      Windows Batch.md
│      
└─读书笔记及专栏
    ├─Android 工程师进阶 34 讲
    │      第01讲：程序运行时，内存到底是如何进行分配的？.md
    │      第02讲：GC 回收机制与分代回收策略.md
    │      第03讲：字节码层面分析 class 类文件结构.md
    │      第04讲：编译插桩操纵字节码，实现不可能完成的任务.md
    │      第05讲：深入理解 ClassLoader 的加载机制.md
    │      第06讲：Class 对象在执行引擎中的初始化过程.md
    │      第07讲：Java 内存模型与线程.md
    │      第08讲：既生 Synchronized，何生 ReentrantLock.md
    │      第09讲：Java 线程优化 偏向锁，轻量级锁、重量级锁.md
    │      第10讲：深入理解 AQS 和 CAS 原理.md
    │      第11讲：线程池之刨根问底.md
    │      第12讲：DVM 以及 ART 是如何对 JVM 进行优化的？.md
    │      第13讲：Android 是如何通过 Activity 进行交互的？.md.md
    │      第14讲：彻底掌握 Android touch 事件分发时序.md
    │      第15讲：Android 如何自定义 View？.md
    │      第16讲：为什么 RecyclerView 可以完美替代 Listview？.md
    │      第17讲：Android OkHttp 全面详解.md
    │      第18讲：Android Bitmap 全面详解.md
    │      第19讲：startActivity 启动过程分析.md
    │      第20讲：底层剖析 Window 、Activity、 View 三者关系.md
    │      第21讲：Android 如何通过 View 进行渲染？.md
    │      第22讲：Android App 的安装过程.md
    │      第23讲：15?分钟彻底掌握?Handler.md
    │      第24讲：APK 如何做到包体积优化？.md
    │      第25讲：Android 崩溃的那些事儿.md
    │      第26讲：面对内存泄漏，如何进行优化？.md
    │      第27讲：面对 UI 卡顿，如何入手分析解决问题？.md
    │      第28讲：Android Gradle 构建问题解析.md
    │      第29讲：MVP 中 presenter 生命周期的管理.md
    │      第30讲：如何设计一个比较合理的 LogUtil 类？.md
    │      第31讲：Android 屏幕适配的处理技巧都有哪些？.md
    │      第32讲：解析动态权限适配遇到的问题.md
    │      第33讲：对于网络编程，你做过哪些优化？.md
    │      第34讲：混合开发真的适合你吗？.md
    │      
    └─Android工程化最佳实践
            ch01 探寻高效易用的反射API.md
            ch02 打造高扩展性的Log系统.md
            ch03 万变不离其宗的Intent.md
            ch04 SharedPreferences再封装.md
            ch05 寻找Fragment的继任者.md
            ch06 AlertDialog为我所用.md
            ch07 gradle使用技巧.md
            ch08 缩减apk 编译时间.md
            ch09 app 终极瘦身实践.md
            ch10 编写针对性Test Case.md
            ch11 Android Studio 使用经验.md
            ch12 抓包工具whistle 实践.md
            
