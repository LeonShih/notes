

# 安装



快捷键

<kbd>Win</kbd> +<kbd>X</kbd>      <kbd>A</kbd>

如果不是 powershell, 输入powershell 进入ps命令

安装目录配置 (可不配置)

文件默认安装到 `C:\Users\<user>\scoop`  --global 会安装在`C:\ProgramData\scoop`

```
$env:SCOOP='D:\Scoop'
[Environment]::SetEnvironmentVariable('SCOOP', $env:SCOOP, 'User')
# run the installer
```

输入以下命令后回车

```
iwr -useb get.scoop.sh | iex
```



输入scoop会提示怎么使用, 常用的已星号标记

```
Usage: scoop <command> [<args>]

Some useful commands are:

alias       Manage scoop aliases
bucket ⭐️     Manage Scoop buckets
cache       Show or clear the download cache
checkup     Check for potential problems
cleanup ⭐️ Cleanup apps by removing old versions
config ⭐️      Get or set configuration values
create      Create a custom app manifest
depends     List dependencies for an app
export      Exports (an importable) list of installed apps
help        Show help for a command
hold        Hold an app to disable updates
home        Opens the app homepage
info        Display information about an app
install ⭐️    Install apps
list   ⭐️     List installed apps
prefix      Returns the path to the specified app
reset  ⭐️      Reset an app to resolve conflicts
search ⭐️     Search available apps
status ⭐️        Show status and check for new app versions
unhold      Unhold an app to enable updates
uninstall ⭐️   Uninstall an app
update   ⭐️    Update apps, or Scoop itself
virustotal  Look for app's hash on virustotal.com
which       Locate a shim/executable (similar to 'which' on Linux)


Type 'scoop help <command>' to get help for a specific command.
```

## scoop配置

>  大部分源都是从github下载,请配置 本地hosts

代理配置
  ```
  scoop config proxy 127.0.0.1:7890
  # 删除
  scoop config rm proxy
  ```


- aria2 (比较费时间)

  ```
  scoop install aria2
  ```

  ```
  //配置aria2
  scoop config aria2-max-connection-per-server 16 &&scoop config aria2-split 16&&scoop config aria2-min-split-size 1M
  ```



​      某些网址不支持多线程下载,可暂时关闭 下载, 不影响已经开启的

```
scoop config aria2-enabled false
```



- bucket

  添加内置源   更多参考 [官方及推荐bucket](https://rasa.github.io/scoop-directory/by-stars)

  ```
  scoop bucket add extras
  ```

  下载图床软件

  ```
  scoop bucket add helbing https://github.com/helbing/scoop-bucket & scoop install picgo
  ```



## 安装

```
scoop install git grep sed gawk
scoop install python node
```

## 卸载

```
scoop uninstall python node
```



## 更新

```
scoop update   //更新scoop bucket, 重新git pull下

scoop status  //查看可更新 软件
scoop update softName // 更新可更新软件
```



## 清除缓存

```
# 清除指定app緩存
scoop cache rm <app>
# 清除所有緩存
scoop cache rm *
```



## 重设版本

```
scoop reset java@17
```



## 参考

 [scoop——强大的Windows命令行包管理工具](https://www.jianshu.com/p/50993df76b1c)

[官网](https://scoop.sh/) 

 [github](https://github.com/lukesampson/scoop)