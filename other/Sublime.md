

下载 http://www.sublimetext.cn/3



[插件配置](http://packagecontrol.cn/installation)



插件推荐

ChineseLocalizations 汉化

**[Emmet ](https://sublime.wbond.net/packages/Emmet)**  前端必备 编码快捷键

**[JSFormat](https://sublime.wbond.net/packages/JsFormat)**  Javascript的代码格式化插件

**[LESS](https://sublime.wbond.net/packages/LESS)**  LESS高亮插件

**Sublimerge**  文本对比

**[Bracket Highlighter](https://github.com/facelessuser/BracketHighlighter)**  代码匹配

#### **[AutoFileName](https://sublime.wbond.net/packages/AutoFileName)** 快捷输入文件名