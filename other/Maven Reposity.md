### 平时用到的库

| 仓库名         | 地址                                                         | 备注                                                     |
| -------------- | ------------------------------------------------------------ | -------------------------------------------------------- |
| mavenCentral   | https://repo1.maven.org/maven2/<br/>https://repo.maven.apache.org/maven2/ | 全区最大的maven库,第二个为apache的镜像库, gradle默认地址 |
| jcenter        | https://jcenter.bintray.com/anverus/tools/                   | bintray私有库 兼容mavenCentral中心仓库，且性能更优       |
| google         | https://dl.google.com/dl/android/maven2/                     | google私有库                                             |
| jitpack        | https://www.jitpack.io                                       | 自动构建库github,及其他git 项目,自带cdn加速              |
| mavenLocal     | ~/.m2/repository                                             | 本地仓库                                                 |
| Spring         | https://repo.spring.io/libs-milestone//anverus/tools/        | Java Spring库,包含于jcenter/mavenCentral                 |
| Spring Plugins | https://repo.spring.io/plugins-release/                      | Java Spring 插件库,包含于jcenter/mavenCentral            |



## 国内阿里云镜像库

| 仓库名称      | 代理源地址                             | 使用地址                                          |
| :------------ | :------------------------------------- | :------------------------------------------------ |
| central       | https://repo1.maven.org/maven2/        | https://maven.aliyun.com/repository/central       |
| jcenter       | http://jcenter.bintray.com/            | https://maven.aliyun.com/repository/jcenter       |
| public        | central/jcenter 聚合仓                 | https://maven.aliyun.com/repository/public        |
| google        | https://maven.google.com/              | https://maven.aliyun.com/repository/google        |
| gradle-plugin | https://plugins.gradle.org/m2/         | https://maven.aliyun.com/repository/gradle-plugin |
| spring        | http://repo.spring.io/libs-milestone/  | https://maven.aliyun.com/repository/spring        |
| spring-plugin | http://repo.spring.io/plugins-release/ | https://maven.aliyun.com/repository/spring-plugin |

### 加速配置

将central/jcenter/google 换成阿里云地址即可

#### maven配置

打开maven的配置文件(windows机器一般在maven安装目录的conf/settings.xml)，在`<mirrors></mirrors>`标签添加mirror子节点:

```
<mirror>
    <id>aliyunmaven</id>
    <mirrorOf>*</mirrorOf>
    <name>阿里云公共仓库</name>
    <url>https://maven.aliyun.com/repository/public</url>
</mirror>
```

如果想使用其它代理仓库,可在`<repositories></repositories>`节点中加入对应的仓库使用地址。以使用google代理仓为例：

```
<repository>
    <id>spring</id>
    <url>https://maven.aliyun.com/repository/google</url>
    <releases>
        <enabled>true</enabled>
    </releases>
    <snapshots>
        <enabled>true</enabled>
    </snapshots>
</repository>
```

#### gradle配置

在build.gradle文件中加入以下代码:

```
allprojects {
    repositories {
        maven { url 'https://maven.aliyun.com/repository/public/' }
        maven { url 'https://maven.aliyun.com/repository/google/' }
        mavenLocal()
    }
}
```



### 参考

[maven repo排名](https://mvnrepository.com/repos)

[阿里云效文档](https://help.aliyun.com/document_detail/102512.html?spm=a2c40.aliyun_maven_repo.0.0.36183054XM40LO#h2-u914Du7F6Eu6307u53572)

[阿里云repositories 一览](https://maven.aliyun.com/mvn/view)