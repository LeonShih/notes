## 准备

[Typora官网  (Markdown 编辑查看软件)](https://typora.io/) 下载安装

[PicGo  (图床工具) github](https://github.com/Molunerfinn/PicGo/releases) 下载安装

[nodejs  (PicGo 插件下载需要)](https://nodejs.org/en/) 下载安装

以上windows系统均可以通过 [scoop 命令安装](https://gitee.com/LeonShih/notes/blob/master/other/Windows%20Scoop%E5%AE%89%E8%A3%85.md) 

## 配置

### nodejs安装后配置 

设置npm镜像,国内加速

```
npm config set registry https://registry.npm.taobao.org
```

### PicGo配置

插件设置 下载 搜索 github-plus  (支持gitee 和github)     版本是1.2.0

- repo      github仓库名称

- branch  项目分支 默认 master即可

- token    github access token

- path   github 仓库的目录  不填为主目录,    

- customUrl   主要用于github cdn加速

  如https://github.com/Leon406/jsdelivr  

  customUrl地址 https://cdn.jsdelivr.net/gh/Leon406/jsdelivr    cdn.jsdelivr.net/gh替换github.com即可

- origin 选择github ,可选gitee

下载完配置如下

![image-20200701151950539](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/image-20200701151950539.png)



#### 新建github仓库  [github加速](https://gitee.com/LeonShih/notes/blob/master/other/github%E5%8A%A0%E9%80%9F.md)

如下图的 repo 为 Leon406/Img

![image-20200701150837688](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/image-20200701150837688.png)

#### 获取token  设置--开发者设置 -- 个人 access token

![image-20200701152711132](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/image-20200701152711132.png)



配置好后,点击上传区, 拖动文件到PicGo进行上传

### Typora 配置

  <kbd>Ctrl</kbd>+<kbd>,</kbd>  进入偏好设置

![image-20200701153356749](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/image-20200701153356749.png)

上传配置

- 插入图片时 上传图片
- 上传服务设置PicGo app
- 再配置下picgo路径
- 最后验证图片上传选项

![image-20200701153550052](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/image-20200701153550052.png)

## 使用

直接拖动图片到typora,或者截图后直接复制到typora会自动进行图片上传

自动上传脚本windows

git项目根目录新建  upload.bat,写入以下内容

```
@echo off
tree /f .|findstr /V "卷" >doc_tree.md
git add .
git commit -m "commit from auto upload shell"
git push
pause
```

git项目根目录新建 pull.bat,写入以下内容

```
git pull
```

以后写好笔记后,双击 upload.bat即可

## 参考

[GithubPlus+PicGo + Typora 一键式图床](http://www.mamicode.com/info-detail-2995230.html)

[个人笔记项目](https://gitee.com/LeonShih/notes.git)