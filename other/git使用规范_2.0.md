

#git 工作流使用规范



### 0.初始化 (空项目) 一般只执行一次

```
git init
git remote add origin <repo site>
```

####   0.1更换服务器ip 重新连接

```
git remote remove origin
git remote add origin <new repo site>
```

#### 0.2 已有项目仓库

```
git clone <repo site>
```



### 1.切换分支dev 开发  (  从 步骤一到步骤五)

```
git checkout -b dev      //dev 自己定义本地分支
git add .
git commit -m "your comments"
```



### 2.开发完成切回master  merge

```
git checkout master
    //git merge dev
git rebase dev
```

​    不建议使用merge会自动交叉合并

```
Merging 状态 记得重新 add  commit
```

### 3.将本地master内容推送到服务器 develop分支  (不要省略  :develop )

​		本地commit 3到5次 或者写了100~200行代码时,再执行

​		代码运行没有错误再提交

```
git pull -rebase origin develop
git merge -no-ff develop   //禁用fast-forward, 不会自动合并

git push  origin master:develop   //develop 服务器分支
```



#### 			3.1无法获取时  从服务器分支获取最新数据	

```
git pull origin develop
```

#### 	 	3.2有冲突,解决后提交,根据提示CONFLICT 找到对应文件并且修改	

####      	        3.3重新提交

```
git add .
git commit -m "your comments"
git push  origin master:develop   //develop 服务器分支

git branch -av    // 提交后确认本地与服务器是否为同一版本
```

### 4.提交成功后,删除dev分支

```
git branch -D dev

```



### 5.切换分支继续开发

```
git checkout -b dev
```






其他:

```
或者 将资源从master copy到 新分支
git checkout -b develop



git push origin develop
```
重置远端服务器地址

```
1.修改命令 
git remote set-url origin [url]
例如：

Git remote set-url origin https://cy@127.0.0.1:8443/r/TanTan.git


或者 
2.先删后加
git remote rm origin
git remote add origin [url]


```





### 配置

全局配置参数 用户目录下 .gitconfig文件

windows路径   %USERPROFILE%/.gitconfig

```
//最大缓存 50M
git config --global http.postBuffer  524288000
//禁用ssl验证
git config --global http.sslVerify "false"
```



参考

[git全局配置参数](https://blog.csdn.net/naruto227/article/details/92090260)

[git学习导航](https://gitee.com/all-about-git)

