# Sonatype  Nexus 3.x Maven 私服搭建 for Android

## 0.准备

[Nexus]: https://www.sonatype.com/download-oss-sonatype

### 1.解压nexus文件

​    将下载的nexus-3.14.0-04-win64.zip解压到自定义目录即可。

### 2.配置nexus的端口和上下文路径

　打开zip解压文件下的 ../nexus-3.14.0-04-win64/nexus-3.14.0-04/etc/nexus-default.properties。
　　如下属性可以自定义修改。

- application-host : Nexus服务监听的主机
- application-port: Nexus服务监听的端口，
- nexus-context-path : Nexus服务的上下文路径

　　通常可以不做任何修改，但个人习惯于修改 application-host 为127.0.0.1。

### 3.nexus  启动

在.../nexus-3.14.0-04-win64/nexus-3.14.0-04/bin 目录下，以管理员身份运行cmd 
  　　1. nexus.exe /run 命令可以启动nexus服务（参考[官方文档](https://help.sonatype.com/repomanager3/installation/installation-methods)) 
  　　2. nexus.exe /install  命令来启动(推荐使用这种方式，参考[官方文档](https://help.sonatype.com/repomanager3/installation/run-as-a-service)),。

### 4.登录网页

启动成功后,输入配置的地址,如127.0.0.1:8081,登录用户名admin,密码 admin123

#### 参考

https://blog.csdn.net/u010015108/article/details/54945125

https://www.cnblogs.com/hujunzheng/p/9807646.html