# RxMarbles--Math And ErrorHandling

## 5.数学计算

### 5.1 count---- 计数

![count](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/count.png)

**测试代码**

```kotlin
private fun count() {
    println("---------count---------")
    val list = mutableListOf(1, 2, 3, 4)
    println(list)
    println("count =====================>")
    list.toFlowable().count().subscribe(Consumer { print("count : $it") })
}
```

**输出结果**

```
---------count---------
[1, 2, 3, 4]
count =====================>
count : 4
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 5.2 reduce---- 累计归一

**测试代码![reduce](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/reduce.png)**

```kotlin
private fun count() {
    println("---------count---------")
    val list = mutableListOf(1, 2, 3, 4)
    println(list)
    println("count =====================>")
    list.toFlowable().count().subscribe(Consumer { print("count : $it") })
}
```

**输出结果**

```
---------count---------
[1, 2, 3, 4]
count =====================>
count : 4
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 5.3 onErrorResumeNext---- 错误复原第二个对象

![onErrorResumeNext](E:%5Cgithub%5Cnotes%5Cother%5CRxMarbles%5Cimg%5CmathAndError%5ConErrorResumeNext.png)

**测试代码**

```kotlin
private fun onErrorResumeNext() {
    println("---------onErrorResumeNext---------")
    val list = mutableListOf(1, 2, 0, 4)
    val list2 = mutableListOf(1, 2, 3, 4)
    println("$list \n$list2")
    println("onErrorResumeNext =====================>")
    list.toFlowable().doAfterNext {
        4 / it
    }.onErrorResumeNext(list2.toFlowable())
            .subscribe { println(it) }
}
```

**输出结果**

```
---------onErrorResumeNext---------
[1, 2, 0, 4] 
[1, 2, 3, 4]
onErrorResumeNext =====================>
1
2
0
1
2
3
4
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 5.4 onErrorReturn---- 错误返回

![onErrorReturn](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/onErrorReturn.png)

**测试代码**

```kotlin
private fun onErrorReturn(a: Int) {
    println("---------onErrorReturn---------")
    val list = mutableListOf(1, 2, 0, 4)
    println("$list")
    println("onErrorReturn =====================>")
    list.toFlowable().doAfterNext {
        4 / it
    }.onErrorReturn { a }
            .subscribe { println(it) }
}
```

**输出结果**

```
onErrorReturn =====================>
1
2
0
88
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'