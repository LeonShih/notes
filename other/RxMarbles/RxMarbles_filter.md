# RxMarbles--filter

## 3.过滤操作

### 3.1 debounce---- 防抖(取某个时间段的最后一个元素)

![debounce](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/debounce.png)

**测试代码**

```kotlin
private fun debounce(a: Long) {
    println("---------debounce---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("debounce $a =====================>")
    list.toFlowable()
            .debounce(a, TimeUnit.SECONDS)
            .subscribe {
                println(it)
            }
}
```

**输出结果**

```
---------debounce---------
[1, 2, 5, 4]
debounce 1 =====================>
4
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'



### 3.2 distinct---- 去重

![distinct](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/distinct.png)

**测试代码**

```kotlin
private fun distinct() {
    println("---------distinct---------")
    val list = mutableListOf(1, 2, 2, 4)
    println(list)
    println("distinct =====================>")
    list.toFlowable()
            .distinct()
            .subscribe { println(it) }
}
```

**输出结果**

```
---------distinct---------
[1, 2, 2, 4]
distinct =====================>
1
2
4
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 3.3 distinctUntilChanged---- 相邻相同元素去重

![distinctUntilChanged](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/distinctUntilChanged.png)

**测试代码**

```kotlin
private fun distinctUntilChanged() {
    println("---------distinctUntilChanged---------")
    val list = mutableListOf(1, 2, 4, 4, 1, 2, 2)
    println(list)
    println("distinctUntilChanged =====================>")
    list.toFlowable()
            .distinctUntilChanged()
            .subscribe { println(it) }
}
```

**输出结果**

```
---------distinctUntilChanged---------
[1, 2, 4, 4, 1, 2, 2]
distinctUntilChanged =====================>
1
2
4
1
2
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 3.4 elementAt---- 取索引元素

![elementAt](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/elementAt.png)

**测试代码**

```kotlin
private fun elementAt(i: Long) {
    println("---------elementAt---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("elementAt $i =====================>")
    list.toFlowable()
            .elementAt(i)
            .subscribe { println(it) }
}
```

**输出结果**

```
---------elementAt---------
[1, 2, 5, 4]
elementAt 3 =====================>
4
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 3.5 filter---- 过滤满足条件

![filter](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/filter.png)

**测试代码**

```kotlin
private fun filter() {
    println("---------filter---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("filter =====================>")
    list.toFlowable()
            .filter { it > 3 }
            .subscribe {
                println(it)
            }
}
```

**输出结果**

```
---------filter---------
[1, 2, 5, 4]
filter  x > 3=====================>
5
4
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 3.6 first---- 取首个

![first](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/first.png)

**测试代码**

```kotlin
private fun first() {
    println("---------first---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("first =====================>")
    list.toFlowable()
            .firstElement()
            .subscribe {
                println(it)
            }
}

```

**输出结果**

```
---------first---------
[1, 2, 5, 4]
first =====================>
1
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 3.7 last---- 取最后

![last](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/last.png)

**测试代码**

```kotlin
private fun last() {
    println("---------last---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("last =====================>")
    list.toFlowable()
            .lastElement()
            .subscribe {
                println(it)
            }
}

```

**输出结果**

```
---------last---------
[1, 2, 5, 4]
last =====================>
4
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 3.8 skip---- 跳过前几个

![skip](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/skip.png)



**测试代码**

```kotlin
private fun skip(a: Long) {
    println("---------skip---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("skip $a=====================>")
    list.toFlowable()
            .skip(a)
            .subscribe {
                println(it)
            }
}
```

**输出结果**

```
---------skip---------
[1, 2, 5, 4]
skip 2=====================>
5
4
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 3.9 skipLast---- 跳过最后一个

![skipLast](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/skipLast.png)

**测试代码**

```kotlin
private fun skipLast(a: Int) {
    println("---------skipLast---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("skipLast $a =====================>")
    list.toFlowable()
            .skipLast(a)
            .subscribe {
                println(it)
            }
}
```

**输出结果**

```
---------skipLast---------
[1, 2, 5, 4]
skipLast 1 =====================>
1
2
5
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 3.10 take---- 取前几个

![take](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/take.png)

**测试代码**

```kotlin
private fun take(a: Long) {
    println("---------take---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("take $a=====================>")
    list.toFlowable()
            .take(a)
            .subscribe {
                println(it)
            }
}
```

**输出结果**

```
---------take---------
[1, 2, 5, 4]
take 2=====================>
1
2
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 3.11 takeLast---- 取最后几个

**测试代![takeLast](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/takeLast.png)码**

```kotlin
private fun takeLast(a: Int) {
    println("---------takeLast---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("takeLast $a=====================>")
    list.toFlowable()
            .takeLast(a)
            .subscribe {
                println(it)
            }
}
```

**输出结果**

```
---------takeLast---------
[1, 2, 5, 4]
takeLast 2=====================>
5
4
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 3.12 ignoreElements---- 不发送元素

![ignoreElements](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/ignoreElements.png)

**测试代码**

```kotlin
private fun ignoreElements() {
    println("---------ignoreElements---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("ignoreElements =====================>")
    list.toFlowable()
            .ignoreElements()
            .subscribe {
                Consumer<Int> {
                    println(it)
                }
            }
}
```

**输出结果**

```
---------ignoreElements---------
[1, 2, 5, 4]
ignoreElements =====================>
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'