# RxMarbles

## 1.合并操作Combining

### 1.1 merge---- 依发射顺序合并

![](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/merge.png)



**测试代码**

```kotlin
fun merge() {
    println(" before merge")
    val list1 = mutableListOf("H", "E", "L", "L", "O")
    val list2 = mutableListOf(" ", "R", "X")

    println("list1 : $list1 \nlist2 : $list2")
    println("merge =====================>")
    Flowable.merge(Flowable.fromIterable(list1), Flowable.fromIterable(list2))
            .subscribe {
                print(it)
            }
    println()
}
```

**输出结果**

```
before merge
list1 : [H, E, L, L, O] 
list2 : [ , R, X]
merge =====================>
HELLO RX
```

结果跟concat一样,不知道什么原因,难道是CPU太快了?

### 1.2 concat----后面拼接

![](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/concat.png)

**测试代码**

```kotlin
fun concat() {
    println("before concat")
    val list1 = mutableListOf(1, 2, 3, 4, 5)
    val list2 = mutableListOf("A", "B", "C")

    println("list1 : $list1 \nlist2 : $list2")
    println("concat =====================>")
    Flowable.merge(Flowable.fromIterable(list1),
            Flowable.fromIterable(list2))
            .subscribe {
                print(it)
            }
    println()
}
```

**输出结果**

```
before merge
list1 : [H, E, L, L, O] 
list2 : [ , R, X]
merge =====================>
HELLO RX
```



### 1.3 startWith----首个插入

![](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/startWith.png)

**测试代码**

```kotlin
private fun startWith() {
println(" before startWith")
    val list1 = mutableListOf(1, 2, 3)

    println("list1 : $list1")
    println("startWith =====================>")

    Flowable.fromIterable(list1)
            .startWith(0)
            .subscribe {
                print(it)
            }
    println()
}
```

**输出结果**

```
 before startWith
list1 : [1, 2, 3]
startWith =====================>
0123
```



### 1.4 amb----取首发Observable/Flowable

![](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/amb.png)

**测试代码**

```kotlin
fun amb() {
    println("before amb")
    val list1 = mutableListOf(1, 2, 3,4,5)
    val list2 = mutableListOf("A", "B", "C","D")

    println("list1 : $list1 \nlist2 : $list2")
    println("amb =====================>")

    Flowable.ambArray(Flowable.fromIterable(list1).delay(111200,TimeUnit.NANOSECONDS),  Flowable.fromIterable(list2))
            .subscribe {
                print(it)
            }
    println()
}
```

**输出结果**

```
before amb
list1 : [1, 2, 3, 4, 5] 
list2 : [A, B, C, D]
amb =====================>
12345
```



### 1.5 combineLatest----合并多个对象最新

![](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/combineLatest.png)

**测试代码**

```kotlin
fun combineLatest() {
    println("before combineLatest")
    val list1 = mutableListOf(1, 2, 3, 4, 5)
    val list2 = mutableListOf("A", "B", "C", "D")

    println("list1 : $list1 \nlist2 : $list2")
    println("combineLatest =====================>")

    Flowable.combineLatest(Flowable.fromIterable(list1),Flowable.fromIterable(list2),
            BiFunction<Int, String, String> { t1, t2 ->
                t1.toString() + t2
            })
            .subscribe {
                println(it)
            } 
    println()
}
```

**输出结果**

```
before combineLatest
list1 : [1, 2, 3, 4, 5] 
list2 : [A, B, C, D]
combineLatest =====================>
5A
5B
5C
5D
```



### 1.6 sequenceEqual----序列判等

![](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/sequenceEqual.png)

**测试代码**

```kotlin
fun sequenceEqual() {
    println("before sequenceEqual")
    val list1 = mutableListOf(1, 2, 3, 4, 5)
    val list2 = mutableListOf(1, 2, 3, 4, 5)

    println("list1 : $list1 \nlist2 : $list2")
    println("sequenceEqual =====================>")

    Flowable.sequenceEqual(Flowable.fromIterable(list1),Flowable.fromIterable(list2))
            .subscribe { t1, _ -> println(t1) }
    println()
}
```

**输出结果**

```
before sequenceEqual
list1 : [1, 2, 3, 4, 5] 
list2 : [1, 2, 3, 4, 5]
sequenceEqual =====================>
true
```

### 1.7 zip----打包

![](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/zip.png)

**测试代码**

```kotlin
fun zip() {
    println("before zip")
    val list1 = mutableListOf(1, 2, 3)
    val list2 = mutableListOf("A", "B", "C", "D")

    println("list1 : $list1 \nlist2 : $list2")
    println("zip =====================>")

    Flowable.zip(Flowable.fromIterable(list1), Flowable.fromIterable(list2),
            BiFunction<Int, String, String> { t1, t2 -> t1.toString() + t2 }
    )
            .subscribe {
                print(it)
            }
    println()
}
```

**输出结果**

```
before zip
list1 : [1, 2, 3] 
list2 : [A, B, C, D]
zip =====================>
1A2B3Cxxxxxxxxxx 

```



