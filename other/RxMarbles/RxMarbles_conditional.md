# RxMarbles--Conditonal

## 2.条件操作

### 2.1 all---- 所有满足条件

![all](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/all.png)



**测试代码**

```kotlin
 private fun all() {
    println("======all=======")
    var list = mutableListOf(2, 4, 7)
    println(list)
    println("all(x => isEven(x) =====================>")
    list.toFlowable().all {
        it % 2 == 0
    }.subscribe { t1, _ ->
        println(t1)
    }
}

```

**输出结果**

```
======all=======
[2, 4, 7]
all(x => isEven(x) =====================>
false
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 2.2 contains---- 包含

![contains](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/contains.png)

**测试代码**

```kotlin
private fun contains(any: Any) {
    println("======contains=======")
    var list = mutableListOf(2, 4, 7)
    println(list)
    println("contains $any =====================>")
    list.toFlowable()
            .contains(any)
            .subscribe(Consumer { println(it) })
}

```

**输出结果**

```
======contains=======
[2, 4, 7]
contains 4 =====================>
true
```

### 2.3 skipWhile---- 跳过满足条件 (直到条件为false后不再跳过)

![skipWhile](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/skipWhile.png)

**测试代码**

```kotlin
 private fun skipWhile() {
    println("======skipWhile=======")
    var list = mutableListOf(1,2, 4, 7,8)
    println(list)
    println("skipWhile  isEven =====================>")
    list.toFlowable()
            .skipWhile(::isEven)
            .subscribe { println(it) }
}
```

**输出结果**

```
//第一次为false后不再跳过
======skipWhile=======
[1, 2, 4, 7, 8]
skipWhile  isEven =====================>
1
2
4
7
8
//直到false后不再跳过
======skipWhile=======
[2, 4, 7, 8]
skipWhile  isEven =====================>
7
8
```

### 2.4 skipUntil---- 直到下一个对象发送事件才停止

![skipUntil](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/skipUntil.png)

**测试代码**

```kotlin
private fun skipUntil() {
    println("======skipUntil=======")
    var list = mutableListOf(3, 5, 8, 7)
    var list2 = mutableListOf(1, 2, 1, 4)
    println(list)
    println("skipUntil  list2 emits =====================>")
    list.toFlowable()
            .skipUntil(list2.toFlowable())
            .subscribe { println(it) }
}
```

**输出结果**

```
======skipUntil=======
[3, 5, 8, 7]
skipUntil  list2 emits =====================>
3
5
8
7
```

CPU运行速度较快没等第二个对象发送事件就已经结束了

### 2.5 takeWhile---- 条件满足是取,直到不满足

![takeWhile](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/takeWhile.png)

**测试代码**

```kotlin
private fun takeWhile() {
    println("======takeWhile=======")
    var list = mutableListOf(2, 4, 7)
    println(list)
    println("takeWhile  isEven =====================>")
    list.toFlowable()
            .takeWhile(::isEven)
            .subscribe { println(it) }
}
```

**输出结果**

```
======takeWhile=======
[2, 4, 7]
takeWhile  isEven =====================>
2
4

//第一次不满足条件就停止
======takeWhile=======
[1, 2, 4, 7]
takeWhile  isEven =====================>
```

### 2.6 takeUntil----直到第二个对象发射事件才停止取

![takeUntil](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/takeUntil.png)

**测试代码**

```kotlin
private fun takeUntil() {
    println("======takeUntil=======")
    var list = mutableListOf(3, 5, 8, 7)
    var list2 = mutableListOf(1, 2, 1, 4)
    println(list)
    println("takeUntil  list2 emits =====================>")
    list.toFlowable()
            .takeUntil(list2.toFlowable())
            .subscribe { println(it) }
}

```

**输出结果**

```
 ======takeUntil=======
[3, 5, 8, 7]
takeUntil  list2 emits =====================>

```

CPU运行速度较快,没等第二个对象发送事件就已经结束了