# RxMarbles--Transform

## 4.变换操作

### 4.1 buffer---- 缓存

![buffer](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/buffer.png)



**测试代码**

```kotlin
private fun buffer(a: Int) {
    println("---------buffer---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("buffer $a =====================>")
    list.toFlowable()
            .buffer(a)
            .subscribe {
                println(it)
            }
}
```

**输出结果**

```
---------buffer---------
[1, 2, 5, 4]
buffer 3 =====================>
[1, 2, 5]
[4]
```

说明:简化代码 引入RxKotlin  ----  implementation 'io.reactivex.rxjava2:rxkotlin:2.2.0'

### 4.2 map---- 变换

![map](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/map.png)

**测试代码**

```kotlin
private fun map() {
    println("---------map---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("map =====================>")
    list.toFlowable()
            .map { it * it }
            .subscribe {
                println(it)
            }
}
```

**输出结果**

```
---------map---------
[1, 2, 5, 4]
map =====================>
1
4
25
16
```

### 4.3 flatMap---- 拆分多个后变换

![flatMap](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/flatMap.png)



**测试代码**

```kotlin
private fun flatMap() {
    println("---------flatMap---------")
    val list = mutableListOf(mutableListOf(1, 2, 3), mutableListOf(4, 5, 6), mutableListOf(7, 8, 9))
    println(list)
    println("flatMap =====================>")
    list.toFlowable()
            .flatMap { it.toFlowable() }
            .subscribe {
                println(it)
            }
}
```

**输出结果**

```
---------flatMap---------
[[1, 2, 3], [4, 5, 6], [7, 8, 9]]
flatMap =====================>
1
2
3
4
5
6
7
8
9
//由3个变成了9个Flowable元素
```

### 4.4 groupBy---- 分组

![groupBy](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/groupBy.png)

**测试代码**

```kotlin
private fun groupBy() {
    println("---------groupBy---------")
    val list = mutableListOf(2, 2, 5, 4, 8, 2)
    println(list)
    println("groupBy =====================>")
    list.toFlowable()
            .groupBy {
                it > 4
            }
            .subscribe {
                it.toList().subscribe { t, _ -> println(t) }
            }
}
```

**输出结果**

```
---------groupBy---------
[2, 2, 5, 4, 8, 2]
groupBy =====================>
[2, 2, 4, 2]
[5, 8]
```

### 4.5 scan---- 累计

![scan](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/scan.png)

**测试代码**

```kotlin
private fun scan() {
    println("---------scan---------")
    val list = mutableListOf(1, 2, 5, 4)
    println(list)
    println("scan =====================>")
    list.toFlowable()
            .scan { t1, t2 -> t1 + t2 }
            .subscribe {
                println(it)
            }
}
```

**输出结果**

```
---------scan---------
[1, 2, 5, 4]
scan =====================>
1
3
8
12
```