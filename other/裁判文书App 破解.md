# Fiddle 抓包获取接口

模拟器 安装官网的Android 端的app, 配置模拟器网络和Fiddle 进行抓包

发现 调用的都是同一个接口, 但是请求的参数不一样

```
POST http://wenshuapp.court.gov.cn/appinterface/rest.q4w HTTP/1.1
Content-Type: application/x-www-form-urlencoded
User-Agent: Dalvik/2.1.0 (Linux; U; Android 7.1.2; DUK-AL20 Build/N2G48H)
Host: wenshuapp.court.gov.cn
Connection: Keep-Alive
Accept-Encoding: gzip
Content-Length: 188

request=eyJpZCI6IjIwMjAwMzEyMTM1NzIyIiwiY29tbWFuZCI6IndzQ291bnRTZWFyY2giLCJwYXJhbXMiOnsiYXBwIjoiY3B3cyIsImRldnR5cGUiOiIxIiwiZGV2aWQiOiIyM2E5Yzk4MjhkYTQ0M2FiYmNmYThhYjQ1MjIwMWZhYiJ9fQ%3D%3D
```

```
POST http://wenshuapp.court.gov.cn/appinterface/rest.q4w HTTP/1.1
Content-Type: application/x-www-form-urlencoded
User-Agent: Dalvik/2.1.0 (Linux; U; Android 7.1.2; DUK-AL20 Build/N2G48H)
Host: wenshuapp.court.gov.cn
Connection: Keep-Alive
Accept-Encoding: gzip
Content-Length: 166

request=eyJpZCI6IjIwMjAwMzEyMTM1NzIyIiwiY29tbWFuZCI6Im1wVXBkYXRlRGF0YSIsInBhcmFtcyI6eyJkZXZ0eXBlIjoiMSIsImRldmlkIjoiMjNhOWM5ODI4ZGE0NDNhYmJjZmE4YWI0NTIyMDFmYWIifX0%3D
```

很容易看出来POST 里的body进行base64 处理了 解密后 (忽略%3D)

```
{
    "id": "20200312135722",
    "command": "wsCountSearch",
    "params": {
        "app": "cpws",
        "devtype": "1",
        "devid": "23a9c9828da443abbcfa8ab452201fab"
    }
}

// 接口返回数据如下

{
    "ret": {
        "msg": "",
        "code": 1
    },
    "data": {
        "WsCountVo": {
            "WZ_Z_zfwl": "41786948145",  //访问总量
            "WS_T_txzaj": "909", //行政 今日新增 
            "WS_T_tqtaj": "91",  //其他 今日
            "WS_T_tmsaj": "12860", //民事 总共
            "WS_C_pcaj": "97197",  //赔偿 总共
            "WS_T_tpcaj": "65", //赔偿 今日新增
            "WS_C_xzaj": "2507857",// 行政 总数
            "WS_C_aj": "88880681", //文书总量
            "WS_T_tzxaj": "7003",  // 执行 今日
            "curDate": "2020-03-12",
            "WS_C_xsaj": "8445604", / 刑事 中国
            "WS_T_taj": "22559",  // 今日 总共案件
            "WS_C_msaj": "55394192", // 民事 总共
            "WS_C_qtaj": "523493", //其他 总共
            "WS_T_txsaj": "1631",  // 刑事 今日
            "WS_C_zxaj": "21912338",// 执行 总数
            "nextDate": "2020-03-12"
        }
    }
}
```

```
{
    "id": "20200312135722",
    "command": "mpUpdateData",
    "params": {
        "devtype": "1",
        "devid": "23a9c9828da443abbcfa8ab452201fab"
    }
}
//app 升级信息,接口返回数据如下
{
    "ret": {
        "msg": "",
        "code": 1
    },
    "data": {
        "JaUpdateVO": {
            "sversion": "2.0.1106",
            "fileurl": "http://wenshu.court.gov.cn/MobilePage/download/wenshuapp.apk",
            "filesize": "2.5MB",
            "updtype": "2",
            "updinfo": "2019-11-06版本"
        }
    }
}
```

通过修改command 及params 来调用不同的接口



查看民事案件 接口参数

```
request=eyJpZCI6IjIwMjAwMzEyMTQxOTUyIiwiY29tbWFuZCI6InF1ZXJ5RG9jIiwicGFyYW1zIjp7InBhZ2VOdW0iOiIxIiwic29ydEZpZWxkcyI6InM1MDpkZXNjIiwiY2lwaGVydGV4dCI6IjEwMDAxMDAgMTAwMDEwMSAxMDExMDEwIDEwMDAxMDAgMTAxMDAwMSAxMDEwMTAwIDEwMTAxMTAgMTEwMDAxMSAxMTAwMTAgMTEwMTExIDExMDAxMDEgMTEwMDExMSAxMTAxMDAwIDExMDAxMCAxMTEwMDAgMTEwMDEwMCAxMDExMDAxIDExMTAxMTEgMTAwMDAxMSAxMDAwMDEwIDEwMTEwMDEgMTEwMDAwIDExMTEwMDEgMTExMDEwMCAxMTAwMTAgMTEwMDAwIDExMDAxMCAxMTAwMDAgMTEwMDAwIDExMDAxMSAxMTAwMDEgMTEwMDEwIDEwMTAxMTEgMTExMDExMSAxMDAwMTExIDExMDExMDEgMTAwMTAxMSAxMDEwMDEwIDEwMDEwMTEgMTAwMTExMSAxMTAxMTAgMTEwMTAwMSAxMDAxMTAxIDEwMDAxMTAgMTExMTAxMCAxMTAxMDAwIDExMDExMTAgMTAxMTAwMCAxMTEwMDAwIDEwMTAxMSAxMDAxMDAwIDEwMDAwMDEgMTEwMDAxMSAxMDEwMDAxIDExMTEwMSAxMTExMDEiLCJkZXZpZCI6IjIzYTljOTgyOGRhNDQzYWJiY2ZhOGFiNDUyMjAxZmFiIiwiZGV2dHlwZSI6IjEiLCJwYWdlU2l6ZSI6IjIwIiwicXVlcnlDb25kaXRpb24iOlt7ImtleSI6InM4IiwidmFsdWUiOiIwMyJ9XX19
```

base64解码后

```
{"id":"20200312141952","command":"queryDoc",
  "params":{"pageNum":"1","sortFields":"s50:desc","ciphertext":"1000100 1000101 1011010 1000100 1010001 1010100 1010110 1100011 110010 110111 1100101 1100111 1101000 110010 111000 1100100 1011001 1110111 1000011 1000010 1011001 110000 1111001 1110100 110010 110000 110010 110000 110000 110011 110001 110010 1010111 1110111 1000111 1101101 1001011 1010010 1001011 1001111 110110 1101001 1001101 1000110 1111010 1101000 1101110 1011000 1110000 101011 1001000 1000001 1100011 1010001 111101 111101","devid":"23a9c9828da443abbcfa8ab452201fab","devtype":"1","pageSize":"20","queryCondition":[{"key":"s8","value":"03"}]}}
```

详情

```
request=eyJpZCI6IjIwMjAwMzEyMTcyNTI1IiwiY29tbWFuZCI6ImRvY0luZm9TZWFyY2giLCJwYXJhbXMiOnsiY2lwaGVydGV4dCI6IjEwMTAxMDAgMTEwMDAxMCAxMDAwMDAxIDExMDExMDAgMTAwMDAxMCAxMTAwMDExIDEwMTEwMDAgMTAwMDAxMSAxMTEwMTExIDEwMTAxMTAgMTEwMDAxMSAxMTEwMTAxIDEwMTAxMTAgMTEwMTAwIDExMDAxMTEgMTEwMDAxMCAxMTAwMDEgMTExMTAxMCAxMDAxMDEwIDExMDAxMTAgMTAwMTAxMSAxMTAwMDAxIDEwMTAwMTEgMTAxMDAwMSAxMTAwMTAgMTEwMDAwIDExMDAxMCAxMTAwMDAgMTEwMDAwIDExMDAxMSAxMTAwMDEgMTEwMDEwIDExMTAwMCAxMDEwMTExIDEwMTAwMTEgMTAxMDAxMSAxMTAwMDEwIDEwMDAxMTAgMTExMDAxIDExMDAxMTAgMTExMDAwIDEwMDEwMTEgMTEwMDExMSAxMTAxMTAxIDExMTAxMTEgMTExMDEwMSAxMTAxMDAgMTEwMTAwIDEwMTAxMTAgMTEwMDAxMSAxMDEwMTEwIDEwMDAxMDEgMTExMTAwMSAxMDAwMDAxIDExMTEwMSAxMTExMDEiLCJkb2NJZCI6IjQwZDA2NTUxYjdmNDQ2YTdiOTU2YWI3NjAwYzMwZGQyIiwiZGV2dHlwZSI6IjEiLCJkZXZpZCI6IjIzYTljOTgyOGRhNDQzYWJiY2ZhOGFiNDUyMjAxZmFiIn19"
```

base64解码后

```
{"id":"20200312172525","command":"docInfoSearch","params":{"ciphertext":"1010100 1100010 1000001 1101100 1000010 1100011 1011000 1000011 1110111 1010110 1100011 1110101 1010110 110100 1100111 1100010 110001 1111010 1001010 1100110 1001011 1100001 1010011 1010001 110010 110000 110010 110000 110000 110011 110001 110010 111000 1010111 1010011 1010011 1100010 1000110 111001 1100110 111000 1001011 1100111 1101101 1110111 1110101 110100 110100 1010110 1100011 1010110 1000101 1111001 1000001 111101 111101","docId":"40d06551b7f446a7b956ab7600c30dd2","devtype":"1","devid":"23a9c9828da443abbcfa8ab452201fab"}}
```



根据两个很容易找到规律

- id   时间戳格式化
- command  命令
- params
  - ciphertext    加密文本 (也可以写死)
  - devtype          设备类型 写死
  - devid  设备id  32位 (可以写死)

关键就是构造ciphertext    

## 反编译Apk

jadx 反编译成java代码, 全局搜索 **ciphertext**, 发现只有两次用到

一次是是获取列表的时候,一次是查询详情的时候用到

```
  public void a(List list, int i, int i2, a.C0046a aVar) {
        g.a("WenShuService", "condition:" + list);
        this.a = "queryDoc";
        net.lawyee.mobilelib.a.a a = net.lawyee.mobilelib.a.a.a(a());
        a.a("ciphertext", d.a());
        a.a("sortFields", "s50:desc");
        a.a("queryCondition", list);
        a.a("pageNum", Integer.valueOf(i));
        a.a("pageSize", Integer.valueOf(i2));
        a(aVar);
        a(a.b(this.a), (String) null);
    }

```

    public void a(String docId, a.C0046a aVar) {
        g.a("WenShuService", "GetAllFileInfoByIDNew fileId:" + str);
        this.a = "docInfoSearch";
        net.lawyee.mobilelib.a.a a = net.lawyee.mobilelib.a.a.a(a());
        a.a("docId", docId);
        a.a("ciphertext", d.a());
        a(aVar);
        a(a.b(this.a), (String) null);
    }


jadx 不易进行代码进行编辑及查找, 可导出为gradle 项目, 将src 里文件 copy到新建的IDEA项目 src 下

可以删除android 自带的包  android androidx  org, 以及其他第三方包 com.bigkoo, com.google , com.handmark 包

![image-20200312143753432](C:\Users\Leon\AppData\Roaming\Typora\typora-user-images\image-20200312143753432.png)



接下里就是从上述java代码, 开始寻找 请求参数构造是如何构造的

最终反混淆 修改的代码参考附件

```
com.lawyee.wenshuapp.config.ApplicationSet  //入口
com.lawyee.wenshuapp.net.WenShuService   // 网络请求
com.lawyee.wenshuapp.ui.WenShuDetailActivity  // 详情页面
                       .ListActivity          // 列表页面
com.lawyee.wenshuapp.util.Encrypt       //3DES加解密
                         .HttpTask      // 网络请求

com.lawyee.wenshuapp.vo.WenShuListVO   // 列表bean,可以找到数字对应的键名称
                       .EncryptVO      //加密后bean 里面进行解密操作
```



## 返回结果解密 (3DES加密)

从WenShuDetailActivity --> EncryptVO#setContent  可以找到加密的类, 比较简单, 3DES 加解密

1. secretKey (24位) 服务器返回
2.  iv (8位)为 当前时间 yyyyMMdd 格式 

有这两个参数就可以进行des解密了

