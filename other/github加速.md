## Github加速

### 限速原因

所有域名都需要dns 解析获取到实际地址才能跳转到指定页面

国内的dns存在一定的污染,将解析地址指向错误的地址,或者较差的服务器

**说白就是被墙了**

解决方法很简单, 不走dns解析, 走本地hosts 直接映射实际地址

### 确定ip地址  (获取的ip地址可能会变)

**github主要有以下4类地址**

源码下载 （release资源）

```
github-production-release-asset-2e65be.s3.amazonaws.com 
github-production-user-asset-6210df.s3.amazonaws.com
github-cloud.s3.amazonaws.com 
github-com.s3.amazonaws.com
github-production-repository-file-5c1aeb.s3.amazonaws.com
```

raw及用户头像

```
raw.github.com
raw.githubusercontent.com
avatars.githubusercontent.com 
avatars0.githubusercontent.com 
avatars1.githubusercontent.com
avatars2.githubusercontent.com 
avatars3.githubusercontent.com 
avatars4.githubusercontent.com
avatars5.githubusercontent.com
avatars6.githubusercontent.com
avatars7.githubusercontent.com
avatars8.githubusercontent.com
camo.githubusercontent.com
user-images.githubusercontent.com 
```

网站及 git clone

```
github.com
www.github.com
github.global.ssl.fastly.net
assets-cdn.github.com
```

api及gist

```
api.github.com 
gist.github.com
```



**可以通过以下两个网站获取**

- https://www.ipaddress.com/   

   域名后面加 .ipaddress.com 自动解析

  ```
  https://baidu.com.ipaddress.com
  ```

- http://tool.chinaz.com/dns

  选择ttl最小的

[也可以通过自动写入hosts工具](https://leon.lanzous.com/id56pji)

**获取到ip如下**

```
52.216.106.140 github-production-release-asset-2e65be.s3.amazonaws.com 
52.216.106.140 github-production-user-asset-6210df.s3.amazonaws.com
52.216.106.140 github-cloud.s3.amazonaws.com 
52.216.106.140 github-com.s3.amazonaws.com
52.216.106.140 github-production-repository-file-5c1aeb.s3.amazonaws.com
199.232.68.133 raw.github.com
199.232.68.133 raw.githubusercontent.com
199.232.68.133 avatars0.githubusercontent.com 
199.232.68.133 avatars1.githubusercontent.com
199.232.68.133 avatars2.githubusercontent.com 
199.232.68.133 avatars3.githubusercontent.com 
199.232.68.133 avatars4.githubusercontent.com 
199.232.68.133 avatars5.githubusercontent.com 
199.232.68.133 avatars6.githubusercontent.com 
199.232.68.133 avatars7.githubusercontent.com 
199.232.68.133 avatars8.githubusercontent.com 
199.232.68.133 user-images.githubusercontent.com 
199.232.68.133 camo.githubusercontent.com
140.82.113.3 github.com
199.232.69.194 github.global.ssl.fastly.net
185.199.110.153 assets-cdn.github.com
140.82.114.5 api.github.com 
140.82.113.3 gist.github.com

```

### 修改hosts文件

将上一步获取的ip地址复制到hosts文件中

```
//windows 路径
C:\Windows\System32\drivers\etc
```

```
//linux
vi  /etc/hosts
```

### 刷新DNS,使hosts生效

windows 进入cmd 输入以下

```
ipconfig /flushdns
```

linux 直接输入

```
sudo /etc/init.d/networking restart
```

关闭浏览器重新访问



## 其他方式

### [jsdelivr 文件加速（单文件大小限制20M）](https://github.com/Leon406/jsdelivr)

### [油猴脚本 服务器镜像](https://github.com/jadezi/github-accelerator/) 

https://github.com/ 镜像服务器地址  方便clone

-   [https://hub.fastgit.org](https://hub.fastgit.org/) | 中国香港
-   [https://gitclone.com](https://gitclone.com/) | 中国浙江杭州
-   [https://github.com.cnpmjs.org](https://github.com.cnpmjs.org/) | 新加坡

release资源下载服务器地址

- ​	https://github.wuyanzheshui.workers.dev
- ​    https://download.fastgit.org

#### Raw 文件加速：

- [https://cdn.jsdelivr.net](https://cdn.jsdelivr.net/) | 全球镜像
- [https://raw.fastgit.org](https://raw.fastgit.org/) | 中国香港
- [https://git.yumenaka.net](https://git.yumenaka.net/) | 美国洛杉矶

#### Release、Code(ZIP) 文件加速：

- [https://gh.con.sh](https://gh.con.sh/) | 美国 01
- [https://gh.api.99988866.xyz](https://gh.api.99988866.xyz/) | 美国 02
- [https://download.fastgit.org](https://download.fastgit.org/) | 日本东京
- [https://g.ioiox.com](https://g.ioiox.com/) | 中国香港 *（估计 10M 小水管，但稳定，不会动不动下载中断，算是备用*
- [https://git.yumenaka.net](https://git.yumenaka.net/) | 美国洛杉矶*（晚上时比前面两个美国的更快*



### SwitchHosts+ googlehosts

第三方hosts同步管理工具+ [googlehosts](https://github.com/googlehosts/hosts/blob/master/hosts-files/hosts)  

googlehosts  加速地址

https://cdn.jsdelivr.net/gh/googlehosts/hosts/hosts-files/hosts

个人github订阅

https://raw.githubusercontent.com/Leon406/pyutil/master/github/hosts?token=AGHZG34S4OB7IKP3I7DBVQC7LHYTC

### 码云镜像

代理下载(有条件)

```
 alias gg='http_proxy=127.0.0.1:1080 https_proxy=127.0.0.1:1080'
 gg git clone https://github.com/repo.git
```

```
//设置全局代理
git config --global http.proxy http://127.0.0.1:1080
git config --global https.proxy http://127.0.0.1:1080
//取消全局代理
git config --global --unset http.proxy
git config --global --unset https.proxy
```



## 后记

之前一直用码云镜像加速,没有dns问题,速度也快,但是同步有点慢

热门的github库可以用码云镜像,但是偶尔自己下建议还是配hosts



## 参考

[如何解决github下载速度太慢的问题](https://blog.csdn.net/jachinFang/article/details/95064543)

[GitHub 轻松提速教程(下载)](https://www.cnblogs.com/LyShark/p/10574755.html)

[googlehost](https://github.com/googlehosts/hosts/blob/master/hosts-files/hosts)

[switchHosts](https://github.com/oldj/SwitchHosts)