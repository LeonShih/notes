



### git clone --depth=1后 怎么获取完整提交的记录?

修改`.git`文件夹内`config`文件的`[remote "origin"]`节的内容

```
[remote "origin"]
	url = https://xxx.com/abc/xxx.git
--	fetch = +refs/heads/master:refs/remotes/origin/master
++	fetch = +refs/heads/*:refs/remotes/origin/*
```

或者使用命令

```
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
```

最后获取

```
git fetch -pv
```



### Git-永久删除某个重要文件或文件夹 (包括历史记录) 强制

提交错误的文件后,又将文件删除后重新提交,虽然文件不在了,**但是历史记录还保存着文件for revert** ,

如果文件过大可能会导致git clone失败

```
git rev-list --objects --all | grep "$(git verify-pack -v .git/objects/pack/*.idx | sort -k 3 -n | tail -50 | awk '{print$1}')"
```



#### 从你的资料库中清除文件

删除多余的文件或者文件夹 file2remove (都不能以 '/' 开头, 文件夹需要加 -r参数 ,可以使用 * 通配符)

```
git filter-branch --force --index-filter 'git rm --cached --ignore-unmatch file2giremove' --prune-empty --tag-name-filter cat -- --all

git filter-branch --force --index-filter 'git rm -r --cached --ignore-unmatch */proguardMapping.txt' --prune-empty --tag-name-filter cat -- --all
git filter-branch --force --index-filter 'git rm -r --cached --ignore-unmatch doc' --prune-empty --tag-name-filter cat -- --all
```

![git删除多余的大文件](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/git删除多余的大文件.jpg)

#### 删除后推送到远程服务器,每个分支都需要提交

[gitlab](https://docs.gitlab.com/ee/user/project/repository/reducing_the_repo_size_using_git.html#reducing-the-repository-size-using-git)

```
git push origin --force 'refs/heads/*'
```

![推送git](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/推送git.jpg)





#### **清理和回收空间**

```
git for-each-ref --format='delete %(refname)' refs/original | git update-ref --stdin && git reflog expire --expire=now --all|git gc --prune=now
```

![资源回收](https://cdn.jsdelivr.net/gh/Leon406/jsdelivr/img/回收资源.jpg)

执行后,发现.git目录文件大小显著减少 130M -->16.8M  (webKernal文件100M+)

### git clone、git pull和git fetch的用法及区别

- git clone 

   将其他仓库克隆到本地，**`包括被clone仓库的版本变化`**  clone操作是一个从无到有的**克隆**操作，不需要`git init`初始化。

- git fetch

   理解 fetch 的关键, 是理解 FETCH_HEAD，FETCH_HEAD指的是: 某个branch在服务器上的最新状态’。这个列表保存在 .git/FETCH_HEAD 文件中, 其中每一行对应于远程服务器的一个分支。

- git pull

  拉取远程分支更新到本地仓库的操作.git pull是相当于从远程仓库获取最新版本，然后再与本地分支merge（合并）

  即：`git pull = git fetch + git merge` 

  

`git clone下来的项目可以直接推送到远程，git pull 和 git fetch 需要先执行 git remote add 添加远程仓库后才能 push。`



### fatal: refusing to merge unrelated histories

因为远程仓库已经存在代码记录了，并且那部分代码没有和本地仓库进行关联，我们可以使用如下操作允许pull未关联的远程仓库旧代码

```
git pull origin master --allow-unrelated-histories
```

注意可能需要解决下冲突



### 多个仓库配置

```
git remote add origin 第一个仓库地址

git remote set-url --add origin 第二个仓库地址

git push -u origin master //推送命令
```



### 统计

```
# 统计当前作者今天（从凌晨 1 点开始）提交次数
$ git log --author="$(git config --get user.name)" --no-merges --since=1am --stat

# 按提交作者统计，按提交次数排序
$ git shortlog -sn
$ git shortlog --numbered --summary

# 只看某作者提交的 commit 数
$ git log --author="faker" --oneline --shortstat

# 按提交作者统计，提交数量排名前 5（看全部，去掉 head 管道即可）
$ git log --pretty='% aN' | sort | uniq -c | sort -k1 -n -r | head -n 5

# 按提交者邮箱统计，提交数量排名前 5
$ git log --pretty=format:% ae | gawk -- '{ ++c [$0]; } END { for (cc in c) printf "%5d % s\n",c [cc],cc; }' | sort -u -n -r | head -n 5

# 统计贡献者数量
$ git log --pretty='% aN' | sort -u | wc -l

# 统计提交数量
$ git log --oneline | wc -l
```

### [OpenSSL SSL_read: Connection was reset, errno 10054](https://www.cnblogs.com/jfen625/p/12995408.html)

这是服务器的SSL证书没有经过第三方机构的签署，所以报错。

解决办法：

```
git config --global http.sslVerify "false"
```

### [解决 Failed to connect to github.com port 443:connection timed out](https://blog.csdn.net/Hodors/article/details/103226958)

gfw拦截了,需要走代理或者配置下[hosts](https://raw.fastgit.org/Leon406/pyutil/master/github/hosts)

```
git config --global http.proxy http://127.0.0.1:1080
 
git config --global https.proxy http://127.0.0.1:1080
```

取消代理

```
git config --global --unset http.proxy
git config --global --unset https.proxy
```



### error: invalid path

```
git config core.protectNTFS false
```



### 强制本地代码

```
git fetch --all && git reset --hard origin/master && git pull
```



### SSH登录

- git bash 

  输入

  ```
  ssh-keygen  -t rsa -C "自己的邮箱"
  ```

  三次回车,结果如下


![screenshot-20220507-162047](https://gitee.com/LeonShih/Image/raw/master/screenshot-20220507-162047.png)



复制公钥  /c/Users/Leon/.ssh/id_rsa.pub内容 到github/gitlab ssh配置页面,即可

## no matching host key type found

- 找到git安装目录

- 进入 etc目录，找到ssh目录ssh_config文件

- 新增

  ```
  Host *.对应的域名.com
  	HostkeyAlgorithms +ssh-rsa
  	PubkeyAcceptedAlgorithms +ssh-rsa
  ```

## File name too long

```
git config --system core.longpaths true
```

## 文件暂存

```
git stash
git stash list
git stash pop [index]
git stash apply [index]
git stash clear
```



## 参考

[git clone --depth=1 后获取其他分支](https://www.cnblogs.com/zhangyiqiu/p/12295572.html)

[Git-永久删除某个重要文件或文件夹 (包括历史记录) 强制](https://blog.51cto.com/phpervip/2497305?source=dra)

[git clone、git pull和git fetch的用法及区别](https://www.cnblogs.com/lalalagq/p/9968949.html)

[Git同时推送多个远程仓库](https://www.jianshu.com/p/a74c5ab4fdfc)

[Git统计操作](https://yelog.org/2017/05/16/Git%E7%BB%9F%E8%AE%A1%E6%93%8D%E4%BD%9C/)

[git error：invalid path问题解决（win下）](https://www.cnblogs.com/GyForever1004/p/13702643.html)

[no matching host key type found](https://www.jianshu.com/p/fdd8c6a1c2a5)

