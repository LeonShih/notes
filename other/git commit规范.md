



## 如何写好git commit

为什么

日常的开发工作中，通常使用git来管理项目代码，当对代码进行修改后，可以通过git commit命令来提交代码。

git 规定提交时必须要写提交信息，作为改动说明，保存在 commit 历史中，方便回溯。规范的 log 不仅有助于他人 review, 还可以有效的输出 CHANGELOG，甚至对于项目的研发质量都有很大的提升。

但是在日常工作中，大多数同学对于 log 信息都是简单写写，没有很好的重视，这对于项目的管理和维护来说，无疑是不友好的。本篇文章主要是结合我自己的使用经验来和大家分享一下 git commit 的一些规范，让你的 log 不仅“好看”还“实用”。



Git每次提交代码都需要写commit message，否则就不允许提交。一般来说，commit message应该清晰明了，说明本次提交的目的，具体做了什么操作……但是在日常开发中，大家的commit message千奇百怪，中英文混合使用、fix bug等各种笼统的message司空见怪，这就导致后续代码维护成本特别大，有时自己都不知道自己的fix bug修改的是什么问题。基于以上这些问题，我们希望通过某种方式来监控用户的git commit message，让规范更好的服务于质量，提高大家的研发效率。



好处

- 便于程序员对提交历史进行追溯，了解发生了什么情况。
- 一旦约束了commit message，意味着我们将慎重的进行每一次提交，不能再一股脑的把各种各样的改动都放在一个git commit里面，这样一来整个代码改动的历史也将更加清晰。
- 格式化的commit message才可以用于自动化输出Change log。



### 格式

Each commit message consists of a **header**, a **body**, and a **footer**.

```
<header>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

```
 fix(forms): properly handle the change to the FormGroup shape (#40829)

 Currently the code in the `FormGroupDirective` assumes that the shape of the underlying `FormGroup` never
 changes and `FormControl`s are not replaced with other types. In practice this is possible and Forms code
 should be able to process such changes in FormGroup shape.

 This commit adds extra check to the `FormGroupDirective` class to avoid applying FormControl-specific to
 other types.

 Fixes #13788.

 PR Close #40829
```



#### Commit Message Header

```
<type>(<scope>): <short summary>
  │       │             │
  │       │             └─⫸ Summary in present tense. Not capitalized. No period at the end.
  │       │
  │       └─⫸ Commit Scope
  │
  └─⫸ Commit Type: build|ci|docs|feat|fix|perf|refactor|test
```

The `<type>` and `<summary>` fields are mandatory, the `(<scope>)` field is optional. 

##### Type

Must be one of the following:

- **build** (origin chore): Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- **ci**: Changes to our CI configuration files and scripts (example scopes: Circle, BrowserStack, SauceLabs)
- **docs**: Documentation only changes
- **feat**: A new feature
- **fix**: A bug fix
- **perf**: A code change that improves performance
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **test**: Adding missing tests or correcting existing tests
- style：code format change
- revert: revert to last version
- release: publish to product

##### Scope

The scope should be the name of the package affected (as perceived by the person reading the changelog generated from commit messages).

##### Summary

Use the summary field to provide a succinct description of the change:

- use the imperative, present tense: "change" not "changed" nor "changes"
- don't capitalize the first letter
- no dot (.) at the end

#### Commit Message Body

Just as in the summary, use the imperative, present tense: "fix" not "fixed" nor "fixes".

```

More detailed explanatory text, if necessary.  Wrap it to 
about 72 characters or so. 

Further paragraphs come after blank lines.

- Bullet points are okay, too
- Use a hanging indent
```



#### Commit Message Footer

The footer can contain information about breaking changes and is also the place to reference GitHub issues, Jira tickets, and other PRs that this commit closes or is related to.

```
BREAKING CHANGE: <breaking change summary>
<BLANK LINE>
<breaking change description + migration instructions>
<BLANK LINE>
<BLANK LINE>
Fixes #<issue number>
```

**（1）不兼容变动**

如果当前代码与上一个版本不兼容，则 Footer 部分以`BREAKING CHANGE`开头，后面是对变动的描述、以及变动理由和迁移方法。

```
BREAKING CHANGE: isolate scope bindings definition has changed.

    To migrate the code follow the example below:
    Before:
    
    scope: {
      myAttr: 'attribute',
    }

    After:

    scope: {
      myAttr: '@',
    }

    The removed `inject` wasn't generaly useful for directives so there should be no code using it.
```

**（2）关闭 Issue**

如果当前 commit 针对某个issue，那么可以在 Footer 部分关闭这个 issue 。

```
Closes #123

Closes #123, #245, #992
```



### Revert commits

If the commit reverts a previous commit, it should begin with `revert: `, followed by the header of the reverted commit.

The content of the commit message body should contain:

- information about the SHA of the commit being reverted in the following format: `This reverts commit <SHA>`,
- a clear description of the reason for reverting the commit message.

```
revert: feat(pencil): add 'graphiteWidth' option

This reverts commit 667ecc1654a317a13331b17617d973392f415f02.
```

### git 提交信息模板

如果你的团队对提交信息有格式要求，可以在系统上创建一个文件，并配置 git 把它作为默认的模板，这样可以更加容易地使提交信息遵循格式。

通过以下命令来配置提交信息模板:

```
git config commit.template   [模板文件名]    //这个命令只能设置当前分支的提交模板
git config  — —global commit.template   [模板文件名]    //这个命令能设置全局的提交模板，注意global前面是两杠
```

新建 .gitmessage.txt(模板文件) 内容可以如下:

```
# headr: <type>(<scope>): <subject>
# - type: feat, fix, docs, style, refactor, test, chore
# - scope: can be empty
# - subject: start with verb (such as 'change'), 50-character line
#
# body: 72-character wrapped. This should answer:
# * Why was this change necessary?
# * How does it address the problem?
# * Are there any side effects?
#
# footer:
# - Include a link to the issue.
# - BREAKING CHANGE
#
```



### changelog生成

发版记需要 git tag v1.0,方便版本比对

```
 git log 11.2.4..11.2.5 --pretty="- %s" |grep -E "fix\(|fix:|feat\(|feat:"  > changelog.md
```

发版记需要 git tag v1.0,方便版本比对

```
echo "## feature: " > changelog.md
git log 11.2.4..11.2.5 --pretty="- %s" |grep -E "feat\(|feat:"  >> changelog.md
echo "## bug fix: " >>changelog.md
git log 11.2.4..11.2.5 --pretty="- %s" |grep -E "fix\(|fix:"  >> changelog.md
```



### 参考文档

[Contributing to Angular](https://github.com/angular/angular/blob/master/CONTRIBUTING.md)

[angular github](https://github.com/angular/angular)

[AngularJS Git Commit Message Conventions](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit#heading=h.8sw072iehlhg)

[前端技巧：git commit提交规范](https://blog.51cto.com/15128443/2661111)

[如何规范你的Git commit？](https://developer.aliyun.com/article/770277)

https://www.ruanyifeng.com/blog/2016/01/commit_message_change_log.html

