## 异常日志

> org.springframework.context.annotation.ConflictingBeanDefinitionException: Annotation-specified bean name 'xxxxx' for bean class [com.xxx.service.xxxxx] conflicts with existing, non-compatible bean definition of same name and class [com.xxx.service.impl.xxxxx]
>     at org.springframework.context.annotation.ClassPathBeanDefinitionScanner.checkCandidate(ClassPathBeanDefinitionScanner.java:320)
>     at org.mybatis.spring.mapper.ClassPathMapperScanner.checkCandidate(ClassPathMapperScanner.java:236)
>     at org.springframework.context.annotation.ClassPathBeanDefinitionScanner.doScan(ClassPathBeanDefinitionScanner.java:259)
>     at org.mybatis.spring.mapper.ClassPathMapperScanner.doScan(ClassPathMapperScanner.java:163)
>     at org.springframework.context.annotation.ClassPathBeanDefinitionScanner.scan(ClassPathBeanDefinitionScanner.java:226)
>     at org.mybatis.spring.mapper.MapperScannerConfigurer.postProcessBeanDefinitionRegistry(MapperScannerConfigurer.java:317)
>     at org.springframework.context.support.PostProcessorRegistrationDelegate.invokeBeanFactoryPostProcessors(PostProcessorRegistrationDelegate.java:123)
>     at org.springframework.context.support.AbstractApplicationContext.invokeBeanFactoryPostProcessors(AbstractApplicationContext.java:682)
>     at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:523)
>     at org.springframework.context.support.ClassPathXmlApplicationContext.<init>(ClassPathXmlApplicationContext.java:139)
>     at org.springframework.context.support.ClassPathXmlApplicationContext.<init>(ClassPathXmlApplicationContext.java:83)
>     at com.rbsn.schedule.gateway.dubbo.DubboProvider.main(DubboProvider.java:17)
>     at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
>     at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
>     at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
>     at java.lang.reflect.Method.invoke(Method.java:498)
>     at com.intellij.rt.execution.application.AppMain.main(AppMain.java:147)

## 原因

- 项目中类名重名了
- IDEA 有热加载替换机制, class包名变更后,之前旧的class不会删除

## 解决

1. 确认项目中是否存在相同的两个类名
2. 没有的话,查看target目录 classes目录是否又重名
3. 如果没有用Spring Boot的tomcat,看看部署目录下是否有重名
4. 手动删除多余的class



   修改包名路径后,建议先进行clean后构建



### 参考

[Tomcat SEVERE: Error listenerStart ... ConflictingBeanDefinitionException](https://www.cnblogs.com/angryorange/p/4812570.html)

[Spring Boot ConflictingBeanDefinitionException: Annotation-specified bean name for @Controller class](http://www.hackerav.com/?post=36817)

