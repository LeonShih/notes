### 项目属性配置

- application.properties
- application.yml (推荐,可读性更好)



项目中引用配置属性

@Value("${key}")

配置中可引用属性  "key: ${key}"



配置中对象解析 ,通过@Autowired 注入对象

对象设置

@ConfigurationProperties

@Component



环境切换配置

application-prod.yml

application-dev.yml



application.yml中配置 使用配置文件

```
spring
	profiles
		active : dev
```

或者命令参数

java -jar xxx.jar --spring.profiles.dev



### Controller的使用

| 注解                     | 描述                                                       |
| ------------------------ | ---------------------------------------------------------- |
| @Controller              | 处理http请求,默认配合模版使用 ,返回的为html的名字          |
| @RestController          | Spring4后, 组合注解,返回 Json @Controller +  @ResponseBody |
| @RequestMapping          | 配置url映射                                                |
| @PathVariable            | 获取url中的数据  大括号占位                                |
| @RequestParam            | 获取请求参数                                               |
| @GetMapping/@PostMapping | 组合注解                                                   |



## 数据库操作