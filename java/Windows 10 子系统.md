Windows 10子系统搭建

进入Microsoft store 搜索下载 ubuntu,点击安装即可

控制面板-->程序-->启用和关闭windows功能-->滑到底部选择适用于Linux的Windows 子系统

电脑重启后启动ubuntu

设置个人密码

设置root密码 sudo passwd

```
sudo passwd root
```

修改apt 源

sudo vi /etc/apt/sources.list



http:*//mirrors.aliyun.com/ubuntu/*

https:*//mirrors.tuna.tsinghua.edu.cn/ubuntu*



保存文件之后执行指令: sudo apt-get update



```
apt dist-upgrade  #批量升级  --simulate
apt-get install --only-upgrade <packagename>
```

第三方包管理器 aptitude



### 升级d到wsl2 [Ubuntu 20.04 LTS](https://www.microsoft.com/en-us/p/ubuntu-2004-lts/9n6svws3rx71?activetab=pivot:overviewtab)

有关 Ubuntu for WSL 的其他版本以及其他安装 WSL 的方法，[查看 Ubuntu Wiki 上的 WSL 页面](https://wiki.ubuntu.com/WSL#Ubuntu_on_WSL)。

若要在 [Windows](https://microsoft.pvxt.net/x9Vg1) 10 May 2020 update (build 19041 或更高版本) 启用 WSL 2，需要在 PowerShell 中以管理员身份运行以下命令：s

```
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
```

将 Ubuntu for WSL 1 升级为 WSL 2

```
wsl.exe --set-version Ubuntu 2
```

将 WSL 2 设置为默认版本

```
wsl.exe --set-default-version 2
```

注意需要重新启动系统以完成 WSL 2 的激活。